FROM public.ecr.aws/docker/library/rust:1.75.0-bookworm AS builder

RUN apt update && apt install -y pkg-config libssl-dev

RUN mkdir /build /inst

COPY ./ /build/

RUN cd /build && cargo install --path distributed --root /inst

FROM public.ecr.aws/docker/library/debian:bookworm-20240110

RUN apt update && apt install -y libssl3 libcurl4 curl git git-lfs iputils-ping && rm -rf /var/cache/apt

RUN mkdir -p /usr/local/bin

COPY --from=builder /inst/bin/* /usr/local/bin/

RUN useradd -m -c 'Lorry' lorry

# Locations for volumes
RUN mkdir -p /config /db /confgit /work-area

COPY lorry-entrypoint /usr/local/bin/

RUN chmod +x /usr/local/bin/lorry-entrypoint

CMD ["lorry-entrypoint"]
