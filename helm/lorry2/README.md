# Lorry 2 Helm Chart

```mermaid
flowchart LR
    subgraph "Existing Services"
        http(["HTTP Port"])
        nginx-ingress["Nginx Ingress"]
    end

    subgraph "Lorry Namespace"
        subgraph "Lorry Controller Pod"
            lorry-controller["Lorry Controller"]
            controller-config-dir["Config Dir"]
            controller-db-dir["Database Dir"]
        end

        controller-svc["Controller Service"]
        controller-config["Controller ConfigMap"]

        controller-config --> controller-config-dir

        subgraph "Worker Pod"
            lorry-worker-1["Lorry Worker 1"]
            worker-config-dir-1["Config Dir"]
            worker-work-dir-1["Work Dir"]
        end

        subgraph "Worker Pod"
            lorry-worker-2["Lorry Worker 2"]
            worker-config-dir-2["Config Dir"]
            worker-work-dir-2["Work Dir"]
        end
        
        subgraph "Worker Pod"
            lorry-worker-3["Lorry Worker 3"]
            worker-config-dir-3["Config Dir"]
            worker-work-dir-3["Work Dir"]
        end
        
        subgraph "Worker Pod"
            lorry-worker-4["Lorry Worker 4"]
            worker-config-dir-4["Config Dir"]
            worker-work-dir-4["Work Dir"]
        end
        
        subgraph "Worker Pod"
            lorry-worker-5["Lorry Worker 5"]
            worker-config-dir-5["Config Dir"]
            worker-work-dir-5["Work Dir"]
        end

        worker-config["Worker ConfigMap"]
        worker-config --> worker-config-dir-1
        worker-config --> worker-config-dir-2
        worker-config --> worker-config-dir-3
        worker-config --> worker-config-dir-4
        worker-config --> worker-config-dir-5

    end
    http --> nginx-ingress
    nginx-ingress --> controller-svc
    controller-svc --> lorry-controller
    lorry-worker-1 --> controller-svc
    lorry-worker-2 --> controller-svc
    lorry-worker-3 --> controller-svc
    lorry-worker-4 --> controller-svc
    lorry-worker-5 --> controller-svc

```