## 1.0.1 (https://gitlab.com/CodethinkLabs/lorry/lorry2/compare/v1.0.0...v1.0.1) (2024-03-19)

### Bug Fixes

* strip leading `v` from docker image tags ([1351cd6](https://gitlab.com/CodethinkLabs/lorry/lorry2/commit/1351cd669b3c58f7853cc6eab3bca3244803563a))
* improve helm chart naming ([4a25511](https://gitlab.com/CodethinkLabs/lorry/lorry2/commit/4a25511ec5fc3ecefd767f28fe334de0605551dc))

# 1.0.0 (2024-03-18)


### Summary

* This is the first release of Lorry after the rewrite to Rust
* Some Lorry features which had little to no uptake by users have been dropped:
    * Cascading configuration files
    * Bundling or tarball-ing of mirrors
    * Support for upstreams which are not git or raw files
* If you are moving from lorry 1, please see the migration guide
  at https://gitlab.com/CodethinkLabs/lorry/lorry2#migration-from-lorry-1

