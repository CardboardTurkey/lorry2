use std::{
    fs::{self, File},
    io::{Read, Write},
    path::{Path, PathBuf},
    process::{Command, Stdio},
    time::{self},
};
//TODO in tests where we are assert errors, rewrite to assert the specific error case we are expecting

use tempfile::{Builder, TempDir};
use url::Url;
use workerlib::{extract_lorry_specs, mirror, Arguments, MirroringError};

//cribbed from git-testament
fn prep_temp_folder(name: &str) -> TempDir {
    Builder::new()
        .prefix(&format!("test-{name}-"))
        .tempdir_in(test_base_dir())
        .expect("Unable to create temporary directory for test")
}

fn test_base_dir() -> PathBuf {
    let mut base = PathBuf::from(env!("CARGO_TARGET_TMPDIR"));
    base.push("tests");
    base.push("lorry");
    std::fs::create_dir_all(&base).expect("Unable to create test base directory");
    base
}

fn get_all_relative_paths<P: AsRef<Path>>(p: P, top_level: bool) -> Vec<String> {
    let p = p.as_ref();
    let folder_name = p.file_name().unwrap().to_os_string().into_string().unwrap();

    let files = p
        .read_dir()
        .unwrap()
        .map(|r| r.unwrap().path())
        .filter(|f| f.is_file())
        .map(|n| n.file_name().unwrap().to_os_string().into_string().unwrap());
    //.collect::<Vec<_>>();

    let dirs = p
        .read_dir()
        .unwrap()
        .map(|r| r.unwrap().path())
        .filter(|f| f.is_dir());

    let f = dirs.flat_map(|d| get_all_relative_paths(d, false));
    //.collect::<Vec<_>>();

    if !top_level {
        f.chain(files)
            .map(|s| {
                //append the folder name to the filenames given

                format!("{}/{}", folder_name, s)
            })
            .collect()
    } else {
        f.chain(files).collect()
    }
}

fn write_to_file(f: &PathBuf, b: &[u8]) -> std::io::Result<()> {
    File::options()
        .write(true)
        .create(true)
        .truncate(true)
        .open(f)
        .unwrap()
        .write_all(b)
}

fn create_upstream(bare: bool) -> TempDir {
    let upstream_dir: TempDir = prep_temp_folder("upstream");
    if !upstream_dir.path().exists() {
        fs::create_dir(&upstream_dir).unwrap();
    }

    run_cmd(
        upstream_dir.path(),
        "git",
        if !bare {
            &["init", "-b", workerlib::DEFAULT_BRANCH_NAME]
        } else {
            &["init", "--bare", "-b", workerlib::DEFAULT_BRANCH_NAME]
        },
    );
    upstream_dir
}

fn commit_changes<P: AsRef<Path>>(repo_dir: P) {
    run_cmd(repo_dir.as_ref(), "git", &["add", "*"]);
    run_cmd(
        repo_dir.as_ref(),
        "git",
        &["commit", "-a", "-m", "Committed file."],
    );
}
#[macro_export]
macro_rules! assert_repos_equal {
    ( $upstream_repo:expr,$downstream_repo:expr) => {
        let mut upstream_file_list = get_all_relative_paths($upstream_repo, true);
        let mut downstream_file_list = get_all_relative_paths($downstream_repo, true);
        //remove files under the `.git` directory

        let is_git_directory = |name: &str| -> bool { name.starts_with(".git/") };

        //don't read the update count file, it will never be equal and is irrelevant to directory equality for our uses

        let is_update_count_file = |name: &str| -> bool { name == "lorry-update-count" };
        upstream_file_list = upstream_file_list
            .into_iter()
            .filter(|s| !is_git_directory(s))
            .filter(|s| !is_update_count_file(s))
            .collect();

        downstream_file_list = downstream_file_list
            .into_iter()
            .filter(|s| !is_git_directory(s))
            .filter(|s| !is_update_count_file(s))
            .collect();

        upstream_file_list.sort();
        downstream_file_list.sort();

        assert_eq!(upstream_file_list, downstream_file_list);

        //now to test every file's contents

        let file_pairs = upstream_file_list.iter().zip(downstream_file_list.iter());

        file_pairs
            .map(|(u, d)| ($upstream_repo.join(u), $downstream_repo.join(d)))
            .for_each(|(upstream_file, downstream_file)| {
                assert_eq!(
                    fs::read(&upstream_file).expect(&format!("couldn't read {}",&upstream_file.to_string_lossy()))
                        ,fs::read(&downstream_file).expect(&format!("couldn't read {}",&downstream_file.to_string_lossy()))
                )
            });
    };
}

macro_rules! assert_repos_not_equal {
    ( $upstream_repo:expr,$downstream_repo:expr) => {
        let mut upstream_file_list = get_all_relative_paths($upstream_repo, true);
        let mut downstream_file_list = get_all_relative_paths($downstream_repo, true);
        //remove files under the `.git` directory

        let is_git_directory = |name: &str| -> bool { name.starts_with(".git/") };

        //don't read the update count file, it will never be equal and is irrelevant to directory equality for our uses

        let is_update_count_file = |name: &str| -> bool { name == "lorry-update-count" };

        upstream_file_list = upstream_file_list
            .into_iter()
            .filter(|s| !is_git_directory(s))
            .filter(|s| !is_update_count_file(s))
            .collect();

        downstream_file_list = downstream_file_list
            .into_iter()
            .filter(|s| !is_git_directory(s))
            .filter(|s| !is_update_count_file(s))
            .collect();


        if upstream_file_list.len() == downstream_file_list.len() {



        upstream_file_list.sort();
        downstream_file_list.sort();

        let file_pairs = upstream_file_list.iter().zip(downstream_file_list.iter());

        assert!(
            file_pairs
                .map(|(u, d)| ($upstream_repo.join(u), $downstream_repo.join(d)))
                .filter(|(upstream_file, downstream_file)| {
                    fs::read(upstream_file).expect("couldn't read upstream")
                        != fs::read(downstream_file).expect("couldn't read downstream")})
                .collect::<Vec<_>>()
                .len()
                != 0
        );
    }
    };
}

fn run_cmd<P: AsRef<Path>>(local: P, cmd: &str, args: &[&str]) -> bool {
    let mut child_process = Command::new(cmd);

    child_process
        .args(args)
        .env("GIT_CEILING_DIRECTORIES", local.as_ref().parent().unwrap());

    println!("Running: {cmd} {:?}", args);
    let child_output = child_process
        .current_dir(local)
        .stdin(Stdio::null())
        .output()
        .expect("Unable to run subcommand");
    //if !child_output.status.success() {
    println!("Status was: {:?}", child_output.status.code());
    println!("Stdout was:\n{:?}", String::from_utf8(child_output.stdout));
    println!("Stderr was:\n{:?}", String::from_utf8(child_output.stderr));
    //}
    child_output.status.success()
}

#[tokio::test]
async fn mirroring_creates_downstream() {
    //create a git repo to mirror from
    let upstream_dir = create_upstream(false);
    let first_commit_msg = "First commit";

    write_to_file(
        &upstream_dir.path().join("file"),
        first_commit_msg.as_bytes(),
    )
    .unwrap();

    commit_changes(&upstream_dir);

    let lorry_dir: TempDir = prep_temp_folder("lorry");

    let repo_name = "test";
    let lorry = &extract_lorry_specs(format!(
        "test:
    type: git
    url: {}",
        upstream_dir.path().to_str().unwrap()
    ))
    .unwrap()[repo_name];
    /*println!(
        "{:?}",
        mirror_dir.path().read_dir().unwrap().collect::<Vec<_>>()
    );*/

    let mirror_dir: TempDir = prep_temp_folder("mirror");
    let mirror_repo = mirror_dir.path().join("test");
    let (tx, mut rx) = tokio::sync::mpsc::unbounded_channel::<(logging::LogLevel, String)>();
    let _logger_handle = tokio::task::spawn(async move {
        while let Some((_, msg)) = rx.recv().await {
            println!("{}", msg)
        } //drain the channel
    });

    let mirror_dir = Url::from_directory_path(mirror_dir.path()).unwrap();
    let args: Arguments = Arguments {
        working_area: lorry_dir.path().to_path_buf(),
        pull_only: false,
        verbose_logging: true,
        repack: false, //TODO test this flag at another point
        keep_multiple_backups: false,
        push_options: vec![],          //TODO test this flag at another point
        check_ssl_certificates: false, //TODO test this flag at another point
        transmitter: tx,
        maximum_redirects: 0,
        log_level: None,
    };

    mirror(repo_name, lorry, &mirror_dir, &args).await.unwrap();
    let clientside_repo = prep_temp_folder("single_mirror_client");

    let clientside_dir = clientside_repo.path().join(repo_name);
    run_cmd(
        &clientside_repo,
        "git",
        &["clone", mirror_repo.to_str().unwrap(), repo_name],
    );
    assert_repos_equal!(upstream_dir.path(), &clientside_dir);

    //make a change to the "upstream" and verify it is copied across
    write_to_file(
        &upstream_dir.path().join("file"),
        "Second commit".as_bytes(),
    )
    .unwrap();

    commit_changes(upstream_dir.path());
    mirror(repo_name, lorry, &mirror_dir, &args).await.unwrap();
    run_cmd(&clientside_dir, "git", &["pull"]);

    //test that the repo is updated
    assert_repos_equal!(upstream_dir.path(), &clientside_dir);
}

#[tokio::test]
async fn a_b_switching_creates_repos() {
    let lorry_dir: TempDir = prep_temp_folder("lorry_internal");
    let mirror_dir: TempDir = prep_temp_folder("mirror");

    let client_dir: TempDir = prep_temp_folder("client");
    let developer_dir: TempDir = prep_temp_folder("dev");
    let upstream_dir = create_upstream(true);

    let repo_name = "test";
    run_cmd(
        developer_dir.path(),
        "git",
        &["clone", upstream_dir.path().to_str().unwrap(), repo_name],
    );

    let developer_repo = developer_dir.path().join(repo_name);
    let client_repo = client_dir.path().join(repo_name);

    write_to_file(&developer_repo.join("file"), "First commit".as_bytes()).unwrap();
    commit_changes(&developer_repo);
    run_cmd(&developer_repo, "git", &["push"]);

    let lorry = &extract_lorry_specs(format!(
        "{repo_name}:
    type: git
    url: {}",
        upstream_dir.path().to_str().unwrap()
    ))
    .unwrap()[repo_name];
    let (tx, mut rx) = tokio::sync::mpsc::unbounded_channel::<(logging::LogLevel, String)>();
    let _logger_handle = tokio::task::spawn(async move {
        while let Some((_, _)) = rx.recv().await {} //drain the channel
    });
    let mirror_dir = Url::from_directory_path(mirror_dir.path()).unwrap();
    let args: Arguments = Arguments {
        working_area: lorry_dir.path().to_path_buf(),
        pull_only: false,
        verbose_logging: true,
        repack: false,
        keep_multiple_backups: false,
        push_options: vec![],
        check_ssl_certificates: false,
        transmitter: tx,
        maximum_redirects: 0,
        log_level: None,
    };

    mirror(repo_name, lorry, &mirror_dir, &args).await.unwrap();

    let mirror_repo_a = lorry_dir.path().join(repo_name).join("git-a");

    let client_a: TempDir = prep_temp_folder("client_a");
    run_cmd(
        client_a.path(),
        "git",
        &["clone", mirror_repo_a.to_str().unwrap(), repo_name],
    );

    assert_repos_equal!(&developer_repo, client_a.path().join(repo_name));
    run_cmd(
        client_dir.path(),
        "git",
        &[
            "clone",
            mirror_dir
                .to_file_path()
                .unwrap()
                .join(repo_name)
                .to_str()
                .unwrap(),
            repo_name,
        ],
    );
    assert_repos_equal!(&developer_repo, &client_repo);

    //make a change to the "upstream"
    write_to_file(&developer_repo.join("file"), "Second commit".as_bytes()).unwrap();
    commit_changes(&developer_repo);
    run_cmd(&developer_repo, "git", &["push"]);
    mirror(repo_name, lorry, &mirror_dir, &args).await.unwrap();
    //these changes should show up in `git-b` but not `git-a`
    run_cmd(client_a.path().join(repo_name), "git", &["pull"]);
    assert_repos_not_equal!(&developer_repo, client_a.path());
    //but they should be on the mirror!
    run_cmd(&client_repo, "git", &["pull"]);
    assert_repos_equal!(&developer_repo, &client_repo);

    mirror(repo_name, lorry, &mirror_dir, &args).await.unwrap();

    //The changes are now in git-a, so should show up in both git-a and the mirror
    run_cmd(client_a.path().join(repo_name), "git", &["pull"]);
    assert_repos_not_equal!(&developer_repo, client_a.path());
    run_cmd(&client_repo, "git", &["pull"]);
    assert_repos_equal!(&developer_repo, &client_repo);
}

#[tokio::test]
async fn a_b_switching_preserves_a_repo() {
    let lorry_dir: TempDir = prep_temp_folder("lorry_internal");
    let mirror_dir: TempDir = prep_temp_folder("mirror");

    let client_dir: TempDir = prep_temp_folder("client");
    let developer_dir: TempDir = prep_temp_folder("dev");
    let upstream_dir = create_upstream(true);

    let repo_name = "test";
    run_cmd(
        developer_dir.path(),
        "git",
        &["clone", upstream_dir.path().to_str().unwrap(), repo_name],
    );

    let developer_repo = developer_dir.path().join(repo_name);
    let client_repo = client_dir.path().join(repo_name);

    write_to_file(&developer_repo.join("file"), "First commit".as_bytes()).unwrap();
    commit_changes(&developer_repo);
    run_cmd(&developer_repo, "git", &["push"]);

    let lorry = &extract_lorry_specs(format!(
        "{repo_name}:
    type: git
    url: {}",
        upstream_dir.path().to_str().unwrap()
    ))
    .unwrap()[repo_name];
    let (tx, mut rx) = tokio::sync::mpsc::unbounded_channel::<(logging::LogLevel, String)>();
    let _logger_handle = tokio::task::spawn(async move {
        while let Some((_, _)) = rx.recv().await {} //drain the channel
    });
    let mirror_dir = Url::from_directory_path(mirror_dir.path()).unwrap();
    let args: Arguments = Arguments {
        working_area: lorry_dir.path().to_path_buf(),
        pull_only: false,
        verbose_logging: true,
        repack: false,
        keep_multiple_backups: false,
        push_options: vec![],
        check_ssl_certificates: false,
        transmitter: tx,
        maximum_redirects: 0,
        log_level: None,
    };

    mirror(repo_name, lorry, &mirror_dir, &args).await.unwrap(); //creates git-a
    run_cmd(
        client_dir.path(),
        "git",
        &[
            "clone",
            mirror_dir
                .to_file_path()
                .unwrap()
                .join(repo_name)
                .to_str()
                .unwrap(),
            repo_name,
        ],
    );
    assert_repos_equal!(&developer_repo, &client_repo);
    tokio::time::sleep(time::Duration::from_secs(1)).await;
    mirror(repo_name, lorry, &mirror_dir, &args).await.unwrap(); //creates git-b
    run_cmd(&client_repo, "git", &["pull"]);
    assert_repos_equal!(&developer_repo, &client_repo);
    tokio::time::sleep(time::Duration::from_secs(1)).await;
    mirror(repo_name, lorry, &mirror_dir, &args).await.unwrap(); //overwrites git-a
    run_cmd(&client_repo, "git", &["pull"]);
    assert_repos_equal!(&developer_repo, &client_repo);
    tokio::time::sleep(time::Duration::from_secs(1)).await;
    mirror(repo_name, lorry, &mirror_dir, &args).await.unwrap(); //overwrites git-b
    run_cmd(&client_repo, "git", &["pull"]);
    assert_repos_equal!(&developer_repo, &client_repo);

    let internal_repo_a = lorry_dir.path().join("test").join("git-a");
    let internal_repo_b = lorry_dir.path().join("test").join("git-b");

    let bkup_a: TempDir = prep_temp_folder("bkup_a");
    assert!(run_cmd(
        bkup_a.path(),
        "git",
        &["clone", internal_repo_a.to_str().unwrap(),],
    ));
    let bkup_b: TempDir = prep_temp_folder("bkup_b");
    assert!(run_cmd(
        bkup_b.path(),
        "git",
        &["clone", internal_repo_b.to_str().unwrap(),],
    ));

    let mirror_repo_backup = lorry_dir.path().join("test").join("git-post-fail");
    assert!(!mirror_repo_backup.exists());

    //delete the upstream. This will create errors anytime we try to mirror
    upstream_dir.close().unwrap();

    let bkup_a_repo = bkup_a.path().join("git-a");
    let bkup_b_repo = bkup_b.path().join("git-b");

    assert_repos_equal!(&bkup_a_repo, &bkup_b_repo);
    assert_repos_equal!(&bkup_a_repo, &developer_repo);
    assert_repos_equal!(&developer_repo, &bkup_b_repo);
    assert_repos_equal!(&internal_repo_a, &internal_repo_b);

    //this should fail to mirror to git-a.
    assert!(match mirror(repo_name, lorry, &mirror_dir, &args).await {
        Ok(_) => false,
        Err(e) => matches!(e, MirroringError::GitMirrorFailed(_)),
    });
    run_cmd(&client_repo, "git", &["pull"]);
    assert_repos_equal!(&developer_repo, &client_repo);

    assert!(mirror_repo_backup.exists());
    assert!(!internal_repo_a.exists());
    assert_repos_not_equal!(&mirror_repo_backup, &internal_repo_b);

    assert!(!run_cmd(&bkup_a_repo, "git", &["pull"]));
    assert!(run_cmd(&bkup_b_repo, "git", &["pull"]));
    // verify b is as it was before
    assert_repos_equal!(&developer_repo, &bkup_b_repo);

    //try to mirror again, and results should be the same: b is unchanged, a is nonexistent
    //git-a no longer exists, we can't compare to it. We just want to make sure git-b is preserved
    assert!(!internal_repo_a.exists());
    assert_repos_equal!(&developer_repo, &bkup_b_repo);
    assert!(match mirror(repo_name, lorry, &mirror_dir, &args).await {
        Ok(_) => false,
        Err(e) => matches!(e, MirroringError::GitMirrorFailed(_)),
    });
    run_cmd(&client_repo, "git", &["pull"]);

    assert!(!run_cmd(&bkup_a_repo, "git", &["pull"]));
    assert!(run_cmd(&bkup_b_repo, "git", &["pull"]));
    assert!(run_cmd(&client_repo, "git", &["pull"]));
    assert_repos_equal!(&developer_repo, &client_repo);

    // verify b is as it was before
    assert_repos_equal!(&developer_repo, &bkup_b_repo);
    assert!(internal_repo_b.exists());

    //Again, repo a is nonexistent (since the mirror cannot succeed) and b stays as the internal active copy.
    assert!(!internal_repo_a.exists());
    //assert_repos_not_equal!(&internal_repo_a, &internal_repo_b);
    assert_repos_equal!(&bkup_a_repo, &developer_repo);
    assert_repos_equal!(&bkup_a_repo, &bkup_b_repo);

    //and just make sure our two backup-up git-a repos are equal: because we ascertain the first is identical to pre-mirror failure a and b.
    assert_repos_equal!(&developer_repo, &bkup_b_repo);
}

#[tokio::test]
async fn keep_multiple_backups_on() {
    let upstream_dir = create_upstream(false);

    let first_commit_msg = "First commit";

    write_to_file(
        &upstream_dir.path().join("file"),
        first_commit_msg.as_bytes(),
    )
    .unwrap();

    commit_changes(upstream_dir.path());

    let mirror_dir: TempDir = prep_temp_folder("a_b_switching");

    let repo_name = "test";
    let lorry = &extract_lorry_specs(format!(
        "test:
    type: git
    url: {}",
        upstream_dir.path().to_str().unwrap()
    ))
    .unwrap()[repo_name];

    let working_area = mirror_dir.path().join("working_dir");
    fs::create_dir(&working_area).unwrap();

    let (tx, mut rx) = tokio::sync::mpsc::unbounded_channel::<(logging::LogLevel, String)>();
    let _logger_handle = tokio::task::spawn(async move {
        while let Some((_, _)) = rx.recv().await {} //drain the channel
    });

    let mirror_dir = Url::from_directory_path("/").unwrap();
    let args: Arguments = Arguments {
        working_area: working_area.clone(),
        pull_only: true,
        verbose_logging: true,
        repack: false,
        keep_multiple_backups: true, //testing this parameter
        push_options: vec![],
        check_ssl_certificates: false,
        transmitter: tx,
        maximum_redirects: 0,
        log_level: None,
    };

    //The sleep calls are required, because otherwise the backup folders will have the same timestamp, and this will cause a crash because lorry attempts to rername them to the sanme name
    mirror(repo_name, lorry, &mirror_dir, &args).await.unwrap();
    tokio::time::sleep(time::Duration::from_secs(1)).await;
    mirror(repo_name, lorry, &mirror_dir, &args).await.unwrap();
    tokio::time::sleep(time::Duration::from_secs(1)).await;
    mirror(repo_name, lorry, &mirror_dir, &args).await.unwrap();
    tokio::time::sleep(time::Duration::from_secs(1)).await;
    mirror(repo_name, lorry, &mirror_dir, &args).await.unwrap();
    tokio::time::sleep(time::Duration::from_secs(1)).await;
    mirror(repo_name, lorry, &mirror_dir, &args).await.unwrap();
    tokio::time::sleep(time::Duration::from_secs(1)).await;
    mirror(repo_name, lorry, &mirror_dir, &args).await.unwrap();
    tokio::time::sleep(time::Duration::from_secs(1)).await;
    mirror(repo_name, lorry, &mirror_dir, &args).await.unwrap();
    tokio::time::sleep(time::Duration::from_secs(1)).await;
    mirror(repo_name, lorry, &mirror_dir, &args).await.unwrap();
    //git-a and git-b directories should be set up

    let mirror_local_dirname = working_area.join(repo_name.replace('/', "_"));

    assert_eq!(
        8,
        mirror_local_dirname
            .read_dir()
            .unwrap()
            .collect::<Vec<_>>()
            .len(),
    ); // we should have one folder for each `mirror` call
}

#[tokio::test]
async fn keep_multiple_backups_off() {
    let upstream_dir = create_upstream(false);

    let first_commit_msg = "First commit";

    write_to_file(
        &upstream_dir.path().join("file"),
        first_commit_msg.as_bytes(),
    )
    .unwrap();

    commit_changes(upstream_dir.path());

    let mirror_dir: TempDir = prep_temp_folder("a_b_switching");

    let repo_name = "test";
    let lorry = &extract_lorry_specs(format!(
        "test:
    type: git
    url: {}",
        upstream_dir.path().to_str().unwrap()
    ))
    .unwrap()[repo_name];

    let working_area = mirror_dir.path().join("working_dir");
    fs::create_dir(&working_area).unwrap();

    let (tx, mut rx) = tokio::sync::mpsc::unbounded_channel::<(logging::LogLevel, String)>();
    let _logger_handle = tokio::task::spawn(async move {
        while let Some((_, _)) = rx.recv().await {} //drain the channel
    });

    let mirror_dir = Url::from_directory_path("/").unwrap();
    let args: Arguments = Arguments {
        working_area: working_area.clone(),
        pull_only: true,
        verbose_logging: true,
        repack: false,
        keep_multiple_backups: false, //testing this parameter
        push_options: vec![],
        check_ssl_certificates: false,
        transmitter: tx,
        maximum_redirects: 0,
        log_level: None,
    };

    //The sleep calls are required, because otherwise the backup folders will have the same timestamp, and this will cause a crash because lorry attempts to rername them to the sanme name
    mirror(repo_name, lorry, &mirror_dir, &args).await.unwrap();
    tokio::time::sleep(time::Duration::from_secs(1)).await;
    mirror(repo_name, lorry, &mirror_dir, &args).await.unwrap();
    tokio::time::sleep(time::Duration::from_secs(1)).await;
    mirror(repo_name, lorry, &mirror_dir, &args).await.unwrap();
    tokio::time::sleep(time::Duration::from_secs(1)).await;
    mirror(repo_name, lorry, &mirror_dir, &args).await.unwrap();
    tokio::time::sleep(time::Duration::from_secs(1)).await;
    mirror(repo_name, lorry, &mirror_dir, &args).await.unwrap();
    tokio::time::sleep(time::Duration::from_secs(1)).await;
    mirror(repo_name, lorry, &mirror_dir, &args).await.unwrap();
    tokio::time::sleep(time::Duration::from_secs(1)).await;
    mirror(repo_name, lorry, &mirror_dir, &args).await.unwrap();
    tokio::time::sleep(time::Duration::from_secs(1)).await;
    mirror(repo_name, lorry, &mirror_dir, &args).await.unwrap();
    //git-a and git-b directories should be set up

    let mirror_local_dirname = working_area.join(repo_name.replace('/', "_"));

    assert_eq!(
        2,
        mirror_local_dirname
            .read_dir()
            .unwrap()
            .collect::<Vec<_>>()
            .len(),
    ); // we should have just the A and B folders, and no backup folders
}

#[tokio::test]
async fn push_mirror_downstream() {
    //this should result in the repo being pushed to the downstream
    let upstream_dir = create_upstream(false);

    let first_commit_msg = "First commit";

    write_to_file(
        &upstream_dir.path().join("file"),
        first_commit_msg.as_bytes(),
    )
    .unwrap();

    // Add a git lfs file
    run_cmd(upstream_dir.path(), "git", &["lfs", "install", "--local"]);
    run_cmd(upstream_dir.path(), "git", &["lfs", "track", "*.big"]);
    write_to_file(
        &upstream_dir.path().join("lfs_file.big"),
        first_commit_msg.as_bytes(),
    )
    .unwrap();

    commit_changes(upstream_dir.path());
    run_cmd(upstream_dir.path(), "git", &["lfs", "ls-files", "--all"]);

    let mirror_dir: TempDir = prep_temp_folder("downstream_mirroring");

    let repo_name = "test";
    let lorry = &extract_lorry_specs(format!(
        "test:
    type: git
    url: file://{}",
        upstream_dir.path().to_str().unwrap()
    ))
    .unwrap()[repo_name];

    let working_area = mirror_dir.path().join("working_dir");
    fs::create_dir(&working_area).unwrap();

    let downstream = prep_temp_folder("my_downstream");
    let (tx, mut rx) = tokio::sync::mpsc::unbounded_channel::<(logging::LogLevel, String)>();
    let _logger_handle = tokio::task::spawn(async move {
        while let Some((_, _)) = rx.recv().await {} //drain the channel
    });

    let mirror_dir = Url::from_directory_path(downstream.path()).unwrap();
    let args: Arguments = Arguments {
        working_area: working_area.clone(),
        pull_only: false, //We are testing this param
        verbose_logging: true,
        repack: false,
        keep_multiple_backups: false,
        push_options: vec![],
        check_ssl_certificates: false,
        transmitter: tx,
        maximum_redirects: 0,
        log_level: None,
    };

    mirror(repo_name, lorry, &mirror_dir, &args).await.unwrap();
    //we should see a copy of the repo at `downstream`

    //pull from repo to check the result is equal to the upstream
    let clientside_repo = prep_temp_folder("pushing_client");

    let clientside_dir = clientside_repo.path().join("my_test");
    fs::create_dir(&clientside_dir).unwrap();
    assert!(run_cmd(
        clientside_repo.path(),
        "git",
        &[
            "clone",
            &format!(
                "file://{}",
                downstream.path().join("test.git").to_str().unwrap()
            ),
            "my_test"
        ],
    ));
    assert!(clientside_repo.path().read_dir().unwrap().count() > 0);
    assert_repos_equal!(&clientside_dir, upstream_dir.path());
}

#[tokio::test]
async fn no_push_mirror_downstream() {
    let repo_name = "test";
    let lorry_dir: TempDir = prep_temp_folder("lorry_internal");
    let mirror_dir: TempDir = prep_temp_folder("mirror");
    let upstream_dir = create_upstream(true);

    let developer_dir: TempDir = prep_temp_folder("dev");
    run_cmd(
        developer_dir.path(),
        "git",
        &["clone", upstream_dir.path().to_str().unwrap(), repo_name],
    );

    let developer_repo = developer_dir.path().join(repo_name);

    write_to_file(&developer_repo.join("file"), "First commit".as_bytes()).unwrap();
    commit_changes(&developer_repo);
    run_cmd(&developer_repo, "git", &["push"]);

    let lorry = &extract_lorry_specs(format!(
        "{repo_name}:
    type: git
    url: {}",
        upstream_dir.path().to_str().unwrap(),
    ))
    .unwrap()[repo_name];

    let (tx, mut rx) = tokio::sync::mpsc::unbounded_channel::<(logging::LogLevel, String)>();
    let _logger_handle = tokio::task::spawn(async move {
        while let Some((_, _)) = rx.recv().await {} //drain the channel
    });
    let mirror_git_dir = Url::from_directory_path(mirror_dir.path()).unwrap();
    let args: Arguments = Arguments {
        working_area: lorry_dir.path().to_path_buf(),
        pull_only: true, //testing that that stops mirroring
        verbose_logging: true,
        repack: false,
        keep_multiple_backups: false,
        push_options: vec![],
        check_ssl_certificates: false,
        transmitter: tx,
        maximum_redirects: 0,
        log_level: None,
    };
    mirror(repo_name, lorry, &mirror_git_dir, &args)
        .await
        .unwrap();
    //should not be anything in mirror-dir
    assert!(mirror_dir.path().read_dir().unwrap().count() == 0);

    mirror(repo_name, lorry, &mirror_git_dir, &args)
        .await
        .unwrap();
    assert!(mirror_dir.path().read_dir().unwrap().count() == 0);

    mirror(repo_name, lorry, &mirror_git_dir, &args)
        .await
        .unwrap();
    assert!(mirror_dir.path().read_dir().unwrap().count() == 0);

    mirror(repo_name, lorry, &mirror_git_dir, &args)
        .await
        .unwrap();
    assert!(mirror_dir.path().read_dir().unwrap().count() == 0);
}

#[tokio::test]
async fn migration() {
    let upstream_dir = create_upstream(false);
    let first_commit_msg = "First commit";

    write_to_file(
        &upstream_dir.path().join("file"),
        first_commit_msg.as_bytes(),
    )
    .unwrap();

    commit_changes(upstream_dir.path());

    let mirror_dir: TempDir = prep_temp_folder("mirror_that_will_migrate");

    let repo_name = "test";
    let lorry = &extract_lorry_specs(format!(
        "test:
    type: git
    url: {}",
        upstream_dir.path().to_str().unwrap()
    ))
    .unwrap()[repo_name];

    let working_area = mirror_dir.path().join("working_dir");
    fs::create_dir(&working_area).unwrap();

    //pretend we are some other mirroring system, pull the upstream down
    run_cmd(
        &working_area,
        "git",
        &["clone", upstream_dir.path().to_str().unwrap(), "my_test"],
    );

    let second_commit_msg = "Second commit";
    //make a change to the "upstream" and verify it is copied across
    write_to_file(
        &upstream_dir.path().join("file"),
        second_commit_msg.as_bytes(),
    )
    .unwrap();

    commit_changes(upstream_dir.path());

    assert!(run_cmd(working_area.join("my_test"), "git", &["pull"]));

    //we now move to using lorry, so we expect to see the repo migrated to have a git-a and git-b repo!

    let (tx, mut rx) = tokio::sync::mpsc::unbounded_channel::<(logging::LogLevel, String)>();
    let _logger_handle = tokio::task::spawn(async move {
        while let Some((_, _)) = rx.recv().await {} //drain the channel
    });

    let mirror_dir = Url::from_directory_path("/").unwrap();
    let args: Arguments = Arguments {
        working_area: working_area.clone(),
        pull_only: true,
        verbose_logging: true,
        repack: false,
        keep_multiple_backups: false,
        push_options: vec![],
        check_ssl_certificates: false,
        transmitter: tx,
        maximum_redirects: 0,
        log_level: None,
    };

    mirror(repo_name, lorry, &mirror_dir, &args).await.unwrap();

    assert!(working_area.join("test").join("git-a").exists());

    assert!(!working_area.join("test").join("git").exists());

    assert!(!working_area.join("test").join("git-b").exists());

    let mut update_count = String::new();
    fs::OpenOptions::new()
        .read(true)
        .open(
            working_area
                .join("test")
                .join("git-a")
                .join("lorry-update-count"),
        )
        .unwrap()
        .read_to_string(&mut update_count)
        .unwrap();
    assert_eq!(update_count, "1\n");

    //verify the repos are equal
    let clientside_repo = prep_temp_folder("migration_client");

    let clientside_dir = clientside_repo.path().join("my_test");
    assert!(run_cmd(
        &clientside_repo,
        "git",
        &[
            "clone",
            working_area.join("test").join("git-a").to_str().unwrap(),
            "my_test",
        ],
    ));

    println!(
        "{:?}",
        clientside_repo
            .path()
            .join("my_test")
            .read_dir()
            .unwrap()
            .collect::<Vec<_>>()
    );

    assert_repos_equal!(&clientside_dir, upstream_dir.path());
}

#[tokio::test]
async fn error_on_rewritten_history() {
    //create the upstream repo.
    //Mirror the upstream repo
    //create a long git history for upstream that is mirrored on the downstream
    //then, force push a commit that deletes some of that history
    //then, try to mirror. This should fail, and the mirror repo should be kept intact
    let lorry_dir: TempDir = prep_temp_folder("lorry_internal");
    let mirror_dir: TempDir = prep_temp_folder("mirror");

    let client_dir: TempDir = prep_temp_folder("client");
    let developer_dir: TempDir = prep_temp_folder("dev");
    let force_pusher_dir: TempDir = prep_temp_folder("force_pusher");
    let upstream_dir = create_upstream(true);
    run_cmd(
        developer_dir.path(),
        "git",
        &["clone", upstream_dir.path().to_str().unwrap(), "test"],
    );
    run_cmd(
        force_pusher_dir.path(),
        "git",
        &["clone", upstream_dir.path().to_str().unwrap(), "test"],
    );

    let developer_repo = developer_dir.path().join("test");
    let force_pusher_repo = force_pusher_dir.path().join("test");
    let client_repo = client_dir.path().join("test");

    write_to_file(&developer_repo.join("file"), "First commit".as_bytes()).unwrap();
    commit_changes(&developer_repo);
    run_cmd(&developer_repo, "git", &["push"]);

    let repo_name = "test";
    let lorry = &extract_lorry_specs(format!(
        "test:
    type: git
    url: {}",
        upstream_dir.path().to_str().unwrap(),
    ))
    .unwrap()[repo_name];

    let (tx, mut rx) = tokio::sync::mpsc::unbounded_channel::<(logging::LogLevel, String)>();
    let _logger_handle = tokio::task::spawn(async move {
        while let Some((_, _)) = rx.recv().await {} //drain the channel
    });
    let mirror_dir = Url::from_directory_path(mirror_dir.path()).unwrap();
    let args: Arguments = Arguments {
        working_area: lorry_dir.path().to_path_buf(),
        pull_only: false,
        verbose_logging: true,
        repack: false,
        keep_multiple_backups: false,
        push_options: vec![],
        check_ssl_certificates: false,
        transmitter: tx,
        maximum_redirects: 0,
        log_level: None,
    };

    mirror(repo_name, lorry, &mirror_dir, &args).await.unwrap();

    run_cmd(
        client_dir.path(),
        "git",
        &[
            "clone",
            mirror_dir
                .to_file_path()
                .unwrap()
                .join("test")
                .to_str()
                .unwrap(),
            "test",
        ],
    );

    assert_repos_equal!(&developer_repo, &client_repo);

    write_to_file(&developer_repo.join("file"), "Second commit".as_bytes()).unwrap();
    commit_changes(&developer_repo);
    run_cmd(&developer_repo, "git", &["push"]);
    mirror(repo_name, lorry, &mirror_dir, &args).await.unwrap();

    write_to_file(&developer_repo.join("file"), "Third commit".as_bytes()).unwrap();
    commit_changes(&developer_repo);
    run_cmd(&developer_repo, "git", &["push"]);
    mirror(repo_name, lorry, &mirror_dir, &args).await.unwrap();

    run_cmd(&client_repo, "git", &["pull"]);

    write_to_file(&developer_repo.join("file"), "Fourth commit".as_bytes()).unwrap();
    commit_changes(&developer_repo);
    run_cmd(&developer_repo, "git", &["push"]);
    mirror(repo_name, lorry, &mirror_dir, &args).await.unwrap();
    run_cmd(&client_repo, "git", &["pull"]);
    assert_repos_equal!(&developer_repo, &client_repo);

    write_to_file(&developer_repo.join("file"), "Fifth commit".as_bytes()).unwrap();
    commit_changes(&developer_repo);
    run_cmd(&developer_repo, "git", &["push"]);
    mirror(repo_name, lorry, &mirror_dir, &args).await.unwrap();

    write_to_file(&force_pusher_repo.join("file"), "Third commit".as_bytes()).unwrap();
    commit_changes(&developer_repo);
    run_cmd(&developer_repo, "git", &["push", "--force"]);
    mirror(repo_name, lorry, &mirror_dir, &args).await.unwrap();

    run_cmd(&client_repo, "git", &["pull"]);
    assert_repos_equal!(&developer_repo, &client_repo);
    assert_repos_not_equal!(&force_pusher_repo, &client_repo);
    assert_repos_not_equal!(&force_pusher_repo, &developer_repo);
    mirror(repo_name, lorry, &mirror_dir, &args).await.unwrap();
    run_cmd(&client_repo, "git", &["pull"]);
    assert_repos_equal!(&developer_repo, &client_repo);
    assert_repos_not_equal!(&force_pusher_repo, &client_repo);
    assert_repos_not_equal!(&force_pusher_repo, &developer_repo);
    mirror(repo_name, lorry, &mirror_dir, &args).await.unwrap();
    run_cmd(&client_repo, "git", &["pull"]);
    assert_repos_equal!(&developer_repo, &client_repo);
    assert_repos_not_equal!(&force_pusher_repo, &client_repo);
    assert_repos_not_equal!(&force_pusher_repo, &developer_repo);
}

#[tokio::test]
async fn raw_files() {
    let lorry_dir: TempDir = prep_temp_folder("lorry_internal");
    let mirror_dir: TempDir = prep_temp_folder("mirror");
    let client_dir: TempDir = prep_temp_folder("client");
    // if I don't create a repo here, then it fails. Is this the intended behaviour? is it expected that a git repo be created at the destination before creating a lorry pointing to it?
    //yes, yes it is. lorry-controller handles this (I think, not 100%. Seems ot happen in lorrycontroller/local.py|gitlab.py|...)
    let repo_name = "test";
    run_cmd(
        &mirror_dir,
        "git",
        &[
            "init",
            "--bare",
            "-b",
            workerlib::DEFAULT_BRANCH_NAME,
            repo_name,
        ],
    );

    let upstream_dir = prep_temp_folder("upstream");

    write_to_file(
        &upstream_dir.path().join("file"),
        "First raw file version".as_bytes(),
    )
    .unwrap();

    write_to_file(
        &upstream_dir.path().join("anotherfile"),
        "GFDFGAFH".as_bytes(),
    )
    .unwrap();

    write_to_file(&upstream_dir.path().join("atroot"), "VFASFSFSDF".as_bytes()).unwrap();

    let lorry = &extract_lorry_specs(format!(
        "test:
    type: raw-file
    urls:
    - destination: {}
      url: file://{}
    - destination: {}
      url: file://{}
    - destination: .
      url: file://{}
    ",
        "testingtesting123/aaa",
        upstream_dir.path().join("file").to_str().unwrap(),
        "aaa/thingmajic/andalongerpath",
        upstream_dir.path().join("anotherfile").to_str().unwrap(),
        upstream_dir.path().join("atroot").to_str().unwrap(),
    ))
    .unwrap()[repo_name];

    eprintln!("{:?}", lorry);

    let (tx, mut rx) = tokio::sync::mpsc::unbounded_channel::<(logging::LogLevel, String)>();
    let _logger_handle = tokio::task::spawn(async move {
        while let Some((_, _)) = rx.recv().await {} //drain the channel
    });
    let mirror_dir = Url::from_directory_path(mirror_dir.path()).unwrap();
    let args: Arguments = Arguments {
        working_area: lorry_dir.path().to_path_buf(),
        pull_only: false,
        verbose_logging: true,
        repack: false, //TODO test this flag at another point
        keep_multiple_backups: false,
        push_options: vec![],          //TODO test this flag at another point
        check_ssl_certificates: false, //TODO test this flag at another point
        transmitter: tx,
        maximum_redirects: 0,
        log_level: None,
    };

    //for i in 1..10 {
    //println!("iter:{}", i);
    mirror(repo_name, lorry, &mirror_dir, &args).await.unwrap();
    //}
    //test the internal repos. They should be OK,even if the downstream isn't

    run_cmd(
        client_dir.path(),
        "git",
        &[
            "clone",
            lorry_dir
                .path()
                .join(repo_name)
                .join("git-a")
                .to_str()
                .unwrap(),
        ],
    );

    let client_git_dir = client_dir.path().join("git-a");
    run_cmd(&client_git_dir, "git", &["lfs", "install"]);

    run_cmd(&client_git_dir, "git", &["fetch"]);
    run_cmd(&client_git_dir, "git", &["lfs", "pull"]);

    eprintln!(
        "contents: {:?}",
        get_all_relative_paths(&client_git_dir, true)
    );

    assert_eq!(
        fs::read(
            client_git_dir
                .join("testingtesting123")
                .join("aaa")
                .join("file")
        )
        .unwrap(),
        fs::read(upstream_dir.path().join("file")).unwrap()
    );

    assert_eq!(
        fs::read(
            client_git_dir
                .join("aaa")
                .join("thingmajic")
                .join("andalongerpath")
                .join("anotherfile")
        )
        .unwrap(),
        fs::read(upstream_dir.path().join("anotherfile")).unwrap()
    );

    assert_eq!(
        fs::read(client_git_dir.join("atroot")).unwrap(),
        fs::read(upstream_dir.path().join("atroot")).unwrap()
    );

    write_to_file(
        &upstream_dir.path().join("file3"),
        "Third raw file.".as_bytes(),
    )
    .unwrap();

    let lorry = &extract_lorry_specs(format!(
        "test:
    type: raw-file
    urls:
    - destination: {}
      url: file://{}
    - destination: {}
      url: file://{}
    - destination: {}
      url: file://{}
    - destination: .
      url: file://{}
    ",
        "testingtesting123/aaa", //TODO it seems it really does *NOT* like the location of the LFS server being local. idk, append a "localhost" to the front? But then I need a server...
        //How am I supposed to implement this test
        upstream_dir.path().join("file").to_str().unwrap(),
        "aaa/thingmajic/andalongerpath",
        upstream_dir.path().join("anotherfile").to_str().unwrap(),
        "f3",
        upstream_dir.path().join("file3").to_str().unwrap(),
        upstream_dir.path().join("atroot").to_str().unwrap(),
    ))
    .unwrap()[repo_name];

    //mirror twice because otherwise the updates will only be in git-b...
    mirror(repo_name, lorry, &mirror_dir, &args).await.unwrap();

    mirror(repo_name, lorry, &mirror_dir, &args).await.unwrap();

    run_cmd(&client_git_dir, "git", &["pull"]);

    assert_eq!(
        fs::read(
            client_git_dir
                .join("testingtesting123")
                .join("aaa")
                .join("file")
        )
        .unwrap(),
        fs::read(upstream_dir.path().join("file")).unwrap()
    );

    assert_eq!(
        fs::read(
            client_git_dir
                .join("aaa")
                .join("thingmajic")
                .join("andalongerpath")
                .join("anotherfile")
        )
        .unwrap(),
        fs::read(upstream_dir.path().join("anotherfile")).unwrap()
    );
    assert_eq!(
        fs::read(client_git_dir.join("f3").join("file3")).unwrap(),
        fs::read(upstream_dir.path().join("file3")).unwrap()
    );

    assert_eq!(
        fs::read(client_git_dir.join("atroot")).unwrap(),
        fs::read(upstream_dir.path().join("atroot")).unwrap()
    );

    let lorry = &extract_lorry_specs(format!(
        "test:
    type: raw-file
    urls:
    - destination: {}
      url: file://{}
    - destination: {}
      url: file://{}
    ",
        "testingtesting123/aaa", //TODO it seems it really does *NOT* like the location of the LFS server being local. idk, append a "localhost" to the front? But then I need a server...
        //How am I supposed to implement this test
        upstream_dir.path().join("file").to_str().unwrap(),
        "f3",
        upstream_dir.path().join("file3").to_str().unwrap(),
    ))
    .unwrap()[repo_name];
    //mirror twice because otherwise the updates will only be in git-b...
    mirror(repo_name, lorry, &mirror_dir, &args).await.unwrap();

    mirror(repo_name, lorry, &mirror_dir, &args).await.unwrap();

    run_cmd(&client_git_dir, "git", &["pull"]);

    assert_eq!(
        fs::read(
            client_git_dir
                .join("testingtesting123")
                .join("aaa")
                .join("file")
        )
        .unwrap(),
        fs::read(upstream_dir.path().join("file")).unwrap()
    );

    assert!(!client_git_dir
        .join("aaa")
        .join("thingmajic")
        .join("andalongerpath")
        .join("anotherfile")
        .exists());

    assert_eq!(
        fs::read(client_git_dir.join("f3").join("file3")).unwrap(),
        fs::read(upstream_dir.path().join("file3")).unwrap()
    );

    //TODO test last-modified header bit
}

#[tokio::test]
async fn update_count() {
    let lorry_dir: TempDir = prep_temp_folder("lorry_internal");
    let mirror_dir: TempDir = prep_temp_folder("mirror");

    let developer_dir: TempDir = prep_temp_folder("dev");
    let upstream_dir = create_upstream(true);

    let repo_name = "test";
    run_cmd(
        developer_dir.path(),
        "git",
        &["clone", upstream_dir.path().to_str().unwrap(), repo_name],
    );

    let developer_repo = developer_dir.path().join(repo_name);

    write_to_file(&developer_repo.join("file"), "First commit".as_bytes()).unwrap();
    commit_changes(&developer_repo);
    run_cmd(&developer_repo, "git", &["push"]);

    let lorry = &extract_lorry_specs(format!(
        "{repo_name}:
    type: git
    url: {}",
        upstream_dir.path().to_str().unwrap()
    ))
    .unwrap()[repo_name];
    let (tx, mut rx) = tokio::sync::mpsc::unbounded_channel::<(logging::LogLevel, String)>();
    let _logger_handle = tokio::task::spawn(async move {
        while let Some((_, _)) = rx.recv().await {} //drain the channel
    });
    let mirror_dir = Url::from_directory_path(mirror_dir.path()).unwrap();
    let args: Arguments = Arguments {
        working_area: lorry_dir.path().to_path_buf(),
        pull_only: true,
        verbose_logging: true,
        repack: false,
        keep_multiple_backups: false,
        push_options: vec![],
        check_ssl_certificates: false,
        transmitter: tx,
        maximum_redirects: 0,
        log_level: None,
    };

    mirror(repo_name, lorry, &mirror_dir, &args).await.unwrap();
    tokio::time::sleep(time::Duration::from_secs(1)).await;
    mirror(repo_name, lorry, &mirror_dir, &args).await.unwrap();
    tokio::time::sleep(time::Duration::from_secs(1)).await;
    mirror(repo_name, lorry, &mirror_dir, &args).await.unwrap();
    tokio::time::sleep(time::Duration::from_secs(1)).await;
    mirror(repo_name, lorry, &mirror_dir, &args).await.unwrap();

    let mut update_count = String::new();
    fs::OpenOptions::new()
        .read(true)
        .open(
            lorry_dir
                .path()
                .join(repo_name)
                .join("git-b")
                .join("lorry-update-count"),
        )
        .unwrap()
        .read_to_string(&mut update_count)
        .unwrap();
    assert_eq!(update_count, "4\n");

    //if mirrors fail, count shouldn't be updated

    upstream_dir.close().unwrap();

    mirror(repo_name, lorry, &mirror_dir, &args).await.err();
    tokio::time::sleep(time::Duration::from_secs(1)).await;
    mirror(repo_name, lorry, &mirror_dir, &args).await.err();
    tokio::time::sleep(time::Duration::from_secs(1)).await;
    mirror(repo_name, lorry, &mirror_dir, &args).await.err();
    tokio::time::sleep(time::Duration::from_secs(1)).await;
    mirror(repo_name, lorry, &mirror_dir, &args).await.err();

    let mut update_count = String::new();
    fs::OpenOptions::new()
        .read(true)
        .open(
            lorry_dir
                .path()
                .join(repo_name)
                .join("git-b")
                .join("lorry-update-count"),
        )
        .unwrap()
        .read_to_string(&mut update_count)
        .unwrap();
    assert_eq!(update_count, "4\n");
}

//TODO test cases to create:
/*

TODO Should we fail during a mirror to git-a, git-b should stay intact. When we continually mirror again and again, git-b will continue to stay intact, and it is git-a that is always deleted

    //test a/b switching in a context where things fail; i.e the backups should be OK
    //the case in which the a/b switching is supposed gto protect us is when the process is killed partway through the mirroring process, or the system crashes or something
    //TODO how could I induce such a failure? Is the upstream disappearing enough?
        //looking at the old tests - yes!


//TODO test the check_ssl_certificates option

//TODO test supplying SSL certs with the lorry

//TODO test supplying refspec option with the lorry
//Do these need testing? On some level that's just testing if `git` works
*/
