use std::{
    fs,
    path::{Path, PathBuf},
};

use thiserror::Error;

use crate::Arguments;

use super::{GitObjectsFolder, InternalGitDirectory, WorkspaceSetupError};
/// The active repo should be more up-to-date than the temporary one. So replace the temporary repo's contents with that of the active one.
/// This means that later operations on the temporary will be carried out of a *copy* of the most up-to-date version of the repo.
/// Will also take a backup of the temporary if the arguments requre it.
pub(crate) fn update_temporary_repo(
    temp_repo_path: &InternalGitDirectory,
    arguments: &Arguments,
    active_repo_timestamp: std::time::SystemTime,
    active_repo_path: &InternalGitDirectory,
    local_dir: &Path,
) -> Result<(), WorkspaceSetupError> {
    //get rid of the old temporary repo if necessary
    if temp_repo_path.exists() {
        if arguments.keep_multiple_backups {
            //we would like to keep the temporary repo as a backup
            let formatted_time = chrono::DateTime::<chrono::Local>::from(active_repo_timestamp);
            //get this to the Y-M-D-H:M:S format
            fs::rename(
                temp_repo_path,
                local_dir.join(format!("git-pre-update-{}", formatted_time.format("%F-%T"))),
            )
            .map_err(|e| WorkspaceSetupError::CouldNotTakeBackup {
                source: e,
                from: temp_repo_path.clone(),
                backup_dir: local_dir
                    .join(format!("git-pre-update-{}", formatted_time.format("%F-%T"))),
            })?;
        } else {
            //just delete the temporary repo to make space for the new one
            fs::remove_dir_all(temp_repo_path).map_err(|e| {
                WorkspaceSetupError::CouldNotDeleteTemporaryDir {
                    source: e,
                    dir: temp_repo_path.clone(),
                }
            })?;
        }
    }
    copy_git_dir(active_repo_path, temp_repo_path).map_err(|e| {
        WorkspaceSetupError::CouldNotCopyGitDir {
            source: e,
            old: active_repo_path.clone(),
            temp: temp_repo_path.clone(),
        }
    })?;
    Ok(())
}

//TODO does this error map too closely to the implementation?
#[derive(Error, Debug)]
pub enum CopyGitDirFailure {
    #[error("Failed to copy the git repo files over")]
    CopyFiles(CopyTreeFailure),
    #[error("Failed to hardlink the git repos")]
    Hardlink(HardlinkingFailure),
    #[error("Could not create the objects folder in the destination folder")]
    CreateObjectsFolder {
        src: std::io::Error,
        path: GitObjectsFolder,
    },
}

/// Copy the source directory to the dest, ignoring the `objects` directory and the update count file. Then, hardlinks the aforementioned `objects` directories together.
/// Assumes that the destination directory has already been deleted.
fn copy_git_dir(
    source_path: &InternalGitDirectory,
    destination_path: &InternalGitDirectory,
) -> Result<(), CopyGitDirFailure> {
    copy_tree_clean(source_path, destination_path).map_err(CopyGitDirFailure::CopyFiles)?;

    //link the objects up
    let source_objects = source_path.objects_folder();
    let destination_objects = destination_path.objects_folder();
    fs::create_dir_all(&destination_objects).map_err(|e| {
        CopyGitDirFailure::CreateObjectsFolder {
            src: e,
            path: destination_objects.clone(),
        }
    })?;
    hardlink_objects(&source_objects, &destination_objects).map_err(CopyGitDirFailure::Hardlink)?;

    Ok(())
}

#[derive(Error, Debug)]
pub enum HardlinkingFailure {
    #[error("Source folder does not exist!")]
    NoSource { path: GitObjectsFolder },

    #[error(transparent)]
    WalkDirError(#[from] walkdir::Error),

    #[error(transparent)]
    CreateRelativePathError(#[from] relative_path::FromPathError),

    #[error("Failed to create the relative path during copying the tree")]
    StripPrefixError(relative_path::StripPrefixError),

    #[error("Could not copy subdirectory across")]
    SubdirCreationError {
        source: std::io::Error,
        failed: PathBuf,
    },

    #[error("Hardlinking operation failed")]
    HardlinkError {
        source: std::io::Error,
        original: PathBuf,
        link: PathBuf,
    },
}

/// Creates a hard link between files in the source directory and files with the corresponding relative address in the desination directory.
fn hardlink_objects(
    src: &GitObjectsFolder,
    dest: &GitObjectsFolder,
) -> Result<(), HardlinkingFailure> {
    if !src.exists() {
        return Err(HardlinkingFailure::NoSource { path: src.clone() });
    }

    for entry in walkdir::WalkDir::new(src).follow_links(false) {
        let entry = entry?;
        let entry_path = entry.path();
        if entry_path == src.as_ref() {
            continue;
        } //we are at the root, no need to copy

        //TODO this is to get around the fact that https://github.com/udoprog/relative-path/issues/33 has not been implemented yet.
        //Once it is, we should be able to get relative_path through entry_path.relative_from(src) without the string intermediate
        let relative_path = &entry_path
            .to_str()
            .unwrap()
            .strip_prefix(src.0.to_str().unwrap())
            .expect(
                "Somehow could not get the relative addr of an item under the source directory",
            )[1..]; //drop the leading slash
        let destination_path = dest.get_file_inside(relative_path);
        if entry.file_type().is_dir() {
            //This is a directory. copy it across.
            fs::create_dir_all(&destination_path).map_err(|e| {
                HardlinkingFailure::SubdirCreationError {
                    source: e,
                    failed: destination_path.clone(),
                }
            })?;
        } else {
            //this is a file. create a link to it in the destination directory
            fs::hard_link(entry_path, &destination_path).map_err(|e| {
                HardlinkingFailure::HardlinkError {
                    source: e,
                    original: entry_path.to_path_buf(),
                    link: destination_path.clone(),
                }
            })?;
        }
    }
    Ok(())
}

#[derive(Error, Debug)]
pub enum CopyTreeFailure {
    #[error(transparent)]
    WalkDir(#[from] walkdir::Error),

    #[error("Failed to create relative paths")]
    CreateRelativePath(#[from] relative_path::FromPathError),

    #[error("Failed to create the relative path during copying the tree")]
    StripPrefix(relative_path::StripPrefixError),

    #[error("Could not copy file across")]
    Copy {
        source: std::io::Error,
        from: PathBuf,
        to: PathBuf,
    },
    #[error("Could not create destination directory")]
    DestCreation {
        source: std::io::Error,
        from: InternalGitDirectory,
        to: InternalGitDirectory,
    },
}

/// Copy the source over to the destination, ignoring the git objects folder (it will be hardlinked instead) and the update count file (it should be updated seperately for each repo)
fn copy_tree_clean(
    src: &InternalGitDirectory,
    dest: &InternalGitDirectory,
) -> Result<(), CopyTreeFailure> {
    if !dest.exists() {
        fs::create_dir_all(dest).map_err(|e| CopyTreeFailure::DestCreation {
            source: e,
            from: src.clone(),
            to: dest.clone(),
        })?;
    }

    for entry in walkdir::WalkDir::new(src)
        .follow_links(false)
        .into_iter()
        .filter_entry(|d| {
            //Refuse to copy the objects folder or count file. They are always on the top level dir
            d.depth() > 1
                || !(d.file_name() == "objects" || d.file_name() == super::COUNT_FILE_NAME)
        })
    {
        let entry = entry?;
        let entry_path = entry.path();
        if entry_path == src.as_ref() {
            continue;
        } //we are at the root, no need to copy

        //TODO this could use the relative_path crate
        let relative_path = &entry_path
            .to_str()
            .unwrap()
            .strip_prefix(src.0.to_str().unwrap())
            .expect("Could not get the relative addr of an item under the source directory")[1..]; //strip_prefix creates a leading slash, which we would liek to drop
        let destination_path = dest.get_file_inside(relative_path);
        if entry.file_type().is_dir() {
            fs::create_dir_all(&destination_path)
        } else {
            fs::copy(entry_path, &destination_path).map(|_| ())
        }
        .map_err(|e| CopyTreeFailure::Copy {
            source: e,
            from: entry_path.to_path_buf(),
            to: destination_path.clone(),
        })?;
    }

    Ok(())
}
