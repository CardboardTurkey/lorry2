//TODO perhaps change all uses of paths to the relative_path crate? It's supposed to be more cross-platform, at the very least
use std::path::{Path, PathBuf};
use tracing::Level;

///Handles the internal local repos and carries out A/B switching so that there is always a stable copy
mod a_b_switching;
/// Some common methods that are used in various places but are not tightly bound to any one caller conceptually, like debug.
mod common;
///Handles mirroring upstream git repos
mod git_mirror;

///Representation of `.lorry` specification files
pub mod lorry_specs;

///Handles mirroring random files from upstream that are not checked into some VCS.
mod raw_file_import;

/// The various errors that can be thrown during a mirroring operation
pub mod errors;

/// Combine cascading configuration files (TODO drop this functionality)
pub mod combine_configs;

use futures::TryFutureExt;
use logging::progress;
use lorry_specs::SingleLorry;
use thiserror::Error;
use url::Url;
use utils::command_wrapper::{CommandBuilder, CommandExecutionError};

use a_b_switching::InternalGitDirectory;
pub use lorry_specs::extract_lorry_specs;
pub use lorry_specs::LorrySpec;

pub const DEFAULT_BRANCH_NAME: &str = "main";
pub const DEFAULT_REF_NAME: &str = "refs/heads/main";

/// Options specifying how to carry out the mirroring operation
#[derive(Debug)]
pub struct Arguments {
    /// Directory for holding intermediate git repos and other internal data
    pub working_area: PathBuf,

    /// only mirror from upstreams, do not push to downstream mirror server
    pub pull_only: bool,

    /// Report debugging information to STDOUT
    pub verbose_logging: bool,

    /// Repack git repos when an import has been updated.
    pub repack: bool,

    /// Keep multiple timestamped backups
    pub keep_multiple_backups: bool,

    /// Options for to pass to `git push` when pushing to the downstream server.
    pub push_options: Vec<String>,

    /// Validate SSL/TLS server certifications. Some upstreams may not have set thiers up correctly so we should disable this if we trust them until they fix thier certificates.
    pub check_ssl_certificates: bool,

    /// The transmitter to which we send our logging to
    pub transmitter: logging::Sender,

    ///  The maximum number of redirections that should be followed by the client
    pub maximum_redirects: u32,
    /// Log Level
    pub log_level: Option<Level>,
}

impl Arguments {
    pub fn base() -> (Self, logging::Receiver) {
        let (tx, rx) = tokio::sync::mpsc::unbounded_channel::<(logging::LogLevel, String)>();

        (
            Self {
                working_area: PathBuf::from("workd"),
                pull_only: false,
                verbose_logging: false,
                repack: true,
                keep_multiple_backups: false,
                push_options: vec![],
                check_ssl_certificates: true,
                transmitter: tx,
                maximum_redirects: 0,
                log_level: Some(Level::INFO),
            },
            rx,
        )
    }
}

#[derive(
    Copy, Clone, PartialEq, Eq, PartialOrd, Ord, clap::ValueEnum, Debug, serde::Deserialize,
)]
pub enum WhenToOperate {
    #[serde(rename = "first")]
    First,

    #[serde(rename = "never")]
    Never,

    #[serde(rename = "always")]
    Always,
}

/// Top-level error type containing all possible errors during a mirroring operation.
#[derive(Error, Debug)]
#[non_exhaustive]
pub enum MirroringError {
    /// There was a problem trying to create an internal repository inside the working area.
    #[error(transparent)]
    FailedToEnsureRepo(#[from] a_b_switching::EnsureGitRepoError),

    /// The upstream was a git repo. There was an error pulling from the remote.
    #[error("Failed to mirror git repo")]
    GitMirrorFailed(git_mirror::GitMirrorError),

    /// The upstream was a set of raw files. Some error was encountered tryign to check these into the internal LFS repositories.
    #[error("Failed to mirror raw files")]
    RawFileMirrorFailed(raw_file_import::RawFileMirrorError),

    /// Failed to set up the A/B switched workspace for this run of lorry.
    #[error("Could not carry out the A/B switching management operations")]
    CouldNotPrepareRepos(a_b_switching::create_workspace::WorkspaceSetupError),

    /// There was an error when pushing to the downstream mirror git repo.
    #[error("Failed to push to remote")]
    DownstreamPushFailed(PushToMirrorError),

    /// lorry was told to repack the git repository after carrying out the mirror, but this operation failed. It was not pushed.
    #[error("Could not repack the repo; it is potentially damaged")]
    CouldNotRepack {
        source: CommandExecutionError,
        repo: InternalGitDirectory,
    },

    /// There was a problem creating a directory to house the working area of this lorry.
    #[error("Could create the dir corresponding to the working area for the given repository")]
    CouldNotCreateWorkingArea {
        source: std::io::Error,
        path: PathBuf,
    },

    /// The mirror failed. When this occurs, lorry tries to take a backup of the failed run's state, hwoever, that operation failed too. Contains the original error.
    #[error("Mirror failed. We tried to move the failed repo to a backup location but failed")]
    CouldNotMoveFailedRepoToBackup {
        source: std::io::Error,
        from: InternalGitDirectory,
        to: PathBuf,
        mirror_error: Box<MirroringError>,
    },

    /// There was an error updating the update count file of the internal working repo.
    #[error("tried to update the update count file, but failed")]
    CouldNotUpdateCountFile {
        source: std::io::Error,
        file_path: a_b_switching::CountFile,
    },

    /// While ths parser can handle this type of lorry specification, the mirroring function does not support it (yet).
    #[error("This lorry type (probably tarball) cannot be handled by the executable.")]
    LorryTypeNotImplemented,
}

/// Mirrors a single lorry specification.
///
/// This pulls the upstream down to the local working area and pushes to the downstream, if required.
/// In between runs, A/B switching is carried out in the working area to make sure that we do not push a malformed repo to the downstream.
/// i.e breaking upstream changes will be rejected and not mirrored to the downstream.
/// This is done by keeping two internal repositories, the most recently-updated one of which may not be modified.
/// This ensures that no failure during the mirroring operation will put the most up-to-date internal repository in an invalid state.
///
/// * `lorry_name` - The name given to the mirror. This is used to find the workspace the mirror is stored in in `lorry`'s working directory.
///
/// * `lorry_details` - The specifications of the mirror, such as its remote location, and what form it takes.
///
/// * `arguments` - Specifications for how to carry out the mirror, such as the location of the downstream or the logging level.
pub async fn mirror(
    lorry_name: &str,
    lorry_details: &LorrySpec,
    mirror_server_base_url: &Url,
    arguments: &Arguments,
) -> Result<(), MirroringError> {
    progress(
        format!("Getting {}", lorry_name),
        arguments.verbose_logging,
        &arguments.transmitter,
    )
    .await;

    let mut workspace = a_b_switching::prepare_repos(lorry_name, arguments)
        .await
        .map_err(MirroringError::CouldNotPrepareRepos)?;

    //TODO break the closure out
    workspace.run_and_swap(lorry_details, arguments).await?; //I think the lifetime error that arises if we try to pass in a closure is because the error can be passed out of the function...

    if !arguments.pull_only {
        //we should push to the mirror server
        workspace
            .with_active(lorry_details, mirror_server_base_url, arguments)
            .await
            .map_err(MirroringError::DownstreamPushFailed)?;
    }
    Ok(())
}

/// Error thrown if there is a problem updating the external downstream.
#[derive(Error, Debug)]
pub enum PushToMirrorError {
    /// The downstream mirror was specified as one on the local filesystem: however,  an error was encountered trying to create this repository.
    /// The result of the command used to create the repository are contained.
    #[error("Could not initialize mirror repo")]
    CouldNotInitializeRepo { source: CommandExecutionError },

    ///The `git push` to the downstream failed for some reason. The wrapped `CommandExecutionError` should contain the output of the push command,
    ///which can be examined for the originating error.
    #[error("Could not push to  mirror repo")]
    CouldNotPush { source: CommandExecutionError },

    ///Tried to access a repository on the local filesystem as the downstream, but the given path is not a valid one. The offending non-path is contained.
    #[error(
        "Downstream is specified as a path on the local filesystem, but the path is malformed."
    )]
    InvalidPath(String),

    #[error("Failed to construct path to downstream git repo")]
    ParseError(#[from] url::ParseError),
}

///Once the upstream has been pulled in successfully, we can call this function to push the results up to our downstream
///takes the path of the new active repo to push, as well as the details of the lorry (to find the remote URL as well as push options).
async fn push_to_mirror_server(
    lorry_details: &LorrySpec,
    lorry_name: &str,
    mirror_server_base_url: &Url,
    active_repo: &InternalGitDirectory,
    arguments: &Arguments,
) -> Result<(), PushToMirrorError> {
    let debug = |s: String| async move { logging::debug(&s, &arguments.transmitter).await };

    let refspecs: Vec<&str> = match lorry_details {
        LorrySpec::Git(s) => &s.refspecs,
        LorrySpec::RawFiles(m) => &m.refspecs,
        LorrySpec::Tarball(s) => &s.refspecs,
    }
    .as_ref()
    .map(|v| v.iter().map(AsRef::as_ref).collect())
    .unwrap_or(vec!["refs/heads/*:refs/heads/*", "refs/tags/*:refs/tags/*"]);
    let url_to_push_to = mirror_server_base_url.join(&format!("{lorry_name}.git"))?;
    if url_to_push_to.scheme() == "file" {
        let url_to_push_to = url_to_push_to
            .to_file_path()
            .map_err(|_| PushToMirrorError::InvalidPath(url_to_push_to.to_string()))?;
        //this repo is bieng pushed to the local filesystem
        //make sure it exists first
        if !url_to_push_to.exists() {
            progress(
                format!(
                    "Creating local repo {} so that we can push to it",
                    &url_to_push_to.to_string_lossy()
                ),
                arguments.verbose_logging,
                &arguments.transmitter,
            )
            .await;

            CommandBuilder::new("git")
                .args(&[
                    "init",
                    "--bare",
                    "-b",
                    crate::DEFAULT_BRANCH_NAME,
                    &url_to_push_to.to_string_lossy(),
                ])
                .execute(active_repo, debug)
                .await
                .map_err(|e| PushToMirrorError::CouldNotInitializeRepo { source: e })?;
        }
    }

    progress(
        format!("Pushing {} to mirror at{}", lorry_name, &url_to_push_to),
        arguments.verbose_logging,
        &arguments.transmitter,
    )
    .await;

    if let Err(err) = CommandBuilder::new("git")
        .args(&["lfs", "install", "--local"])
        .execute(active_repo, debug)
        .await
    {
        logging::debug(
            format!("Worker unable to push lfs objects: {err}"),
            &arguments.transmitter,
        )
        .await;
    }

    CommandBuilder::new("git")
        .arg("push")
        .args(
            &arguments
                .push_options
                .iter()
                .map(|s| (format!("--push-option={}", s)))
                .collect::<Vec<_>>(),
        )
        .arg(&url_to_push_to.to_string())
        .args(&refspecs)
        .execute(active_repo, debug)
        .await
        .map_err(|e| PushToMirrorError::CouldNotPush { source: e })?;
    Ok(())
}

/// Carry out the mirroring opration within a properly set-up workspace.
///
/// Takes in the less up-to-date internal git repo that we are allowed to modify,
/// and carries out the approprate operations to update the it, depending on the type of repository the upstream takes, indicated by the lorry spec passed.
/// Also carries out some post-processing (repacking) if required by arguments.
/// These can also result in errors, not just the core mirror and update operations!
async fn try_mirror(
    lorry_details: &LorrySpec,
    lorry_name: &str,
    temp_repo: &InternalGitDirectory,
    arguments: &Arguments,
    working_dir: &Path,
) -> Result<(), MirroringError> {
    let debug = |s: String| async move { logging::debug(&s, &arguments.transmitter).await };

    let needs_agressive: bool = match lorry_details {
        LorrySpec::Git(SingleLorry {
            url,
            check_ssl_certificates,
            refspecs,
        }) => git_mirror::fetch_git_repo(
            temp_repo,
            url,
            arguments,
            common::should_check_certificates(
                arguments.check_ssl_certificates,
                *check_ssl_certificates,
            ),
            refspecs.to_owned(),
        )
        .await
        .map_err(MirroringError::GitMirrorFailed)
        .map(|_| false),

        LorrySpec::RawFiles(raw_files) => {
            raw_file_import::mirror_raw_file_into_lfs(raw_files, working_dir, temp_repo, arguments)
                .await
                .map_err(MirroringError::RawFileMirrorFailed)
                .map(|_| false)
        }
        LorrySpec::Tarball(SingleLorry {
            url: _,
            check_ssl_certificates: _,
            refspecs: _,
        }) => Err(MirroringError::LorryTypeNotImplemented),
    }?;
    if arguments.repack {
        progress(
            format!("repacking {} git repo", lorry_name),
            arguments.verbose_logging,
            &arguments.transmitter,
        )
        .await;

        // We believe 128M is a good value, but it might be useful to make it configurable in the future.
        CommandBuilder::new("git")
            .args(&["config", "pack.windowMemory", "128M"])
            .execute(temp_repo, debug)
            .and_then(|_| async {
                let mut c = CommandBuilder::new("git").arg("gc");
                if needs_agressive {
                    c = c.args(&["--aggressive"]);
                }
                c.execute(temp_repo, debug).await
            })
            .await
            .map_err(|e| MirroringError::CouldNotRepack {
                source: e,
                repo: temp_repo.clone(),
            })?;
    }

    Ok(())
}

/// Returns a vector of all the results of the mirroring operations
pub async fn mirror_many(
    lorry_specification_files: Vec<std::collections::BTreeMap<String, LorrySpec>>,
    mirror_server_base_url: &Url,
    mirroring_args: &Arguments,
) -> Vec<(String, Result<(), MirroringError>)> {
    let lorry_results = lorry_specification_files.into_iter().flat_map(|lorries| {
        lorries
            .into_iter()
            .map(|(repo_name, lorry)| async move {
                let res = mirror(&repo_name, &lorry, mirror_server_base_url, mirroring_args).await;
                (repo_name, res)
            })
            .collect::<Vec<_>>()
    });

    futures::future::join_all(lorry_results).await
}
