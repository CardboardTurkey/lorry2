use std::{fs, path::PathBuf};

use logging::progress;
use relative_path::RelativePath;
use thiserror::Error;

use crate::{Arguments, InternalGitDirectory};
use utils::command_wrapper::{CommandBuilder, CommandExecutionError};

#[derive(Debug, Error)]
pub enum RemoveUnwantedFilesFailure {
    #[error("Attempted to delete old work tree {worktree} files from disk, but failed: {source}")]
    DeleteOldWorktree {
        source: std::io::Error,
        worktree: PathBuf,
    },

    #[error("Attempted to delete leftover work trees {worktree} from previous runs, but failed: source: {source}")]
    CleanUpOldWorktree {
        source: CommandExecutionError,
        worktree: PathBuf,
    },

    #[error("Attempted to prune work tree {worktree} created by run of raw file importer, but failed: {source}")]
    CleanUpNewWorktree {
        source: CommandExecutionError,
        worktree: PathBuf,
    },

    #[error("Attempted delete work tree {worktree} created by run of raw file importer, but failed: {source}")]
    DeleteNewWorktree {
        source: std::io::Error,
        worktree: PathBuf,
    },

    #[error("Could not create a fresh work tree {worktree} from the downstream repo: {source}")]
    CreateNewWorktree {
        source: CommandExecutionError,
        worktree: PathBuf,
    },

    #[error("Could not remove the file {file} from the git repository {repo}: {source}")]
    RemoveFromRepo {
        source: CommandExecutionError,
        file: String,
        repo: PathBuf,
    },

    #[error("Could not remove an unwanted file {file} from disk: {source}")]
    DeleteUnwantedFile {
        source: std::io::Error,
        file: PathBuf,
    },
}

/// Remove files from the repo that are no longer needed in the `lorry` config, and then commit the resulting worktree to the git repo `local-git-repo` at branch `raw_file_branch`
/// "no longer needed" is defined as those files in `old-files` that are not present in`desired-files`; i.e files that were present in the previous commit but not specified in the currently used `lorry` file
pub(crate) async fn remove_unwanted_files<T: AsRef<RelativePath>>(
    local_git_repo: &InternalGitDirectory,
    raw_file_branch: &str,
    old_files: Vec<T>,
    desired_files: Vec<T>,
    arguments: &Arguments,
) -> Result<(), RemoveUnwantedFilesFailure> {
    let debug = |s: String| async move { logging::debug(&s, &arguments.transmitter).await };

    progress(
        "committing worktree changes",
        arguments.verbose_logging,
        &arguments.transmitter,
    )
    .await;
    let git_dir_prefix = local_git_repo.parent();
    let worktree: PathBuf = git_dir_prefix.join("raw-file-worktree");
    if worktree.exists() {
        progress(
            "Deleting old worktree...",
            arguments.verbose_logging,
            &arguments.transmitter,
        )
        .await;
        fs::remove_dir_all(&worktree).map_err(|e| {
            RemoveUnwantedFilesFailure::DeleteOldWorktree {
                source: e,
                worktree: worktree.clone(),
            }
        })?;
    }

    CommandBuilder::new("git")
        .args(&["worktree", "prune"])
        .execute(local_git_repo, debug)
        .await
        .map_err(|e| RemoveUnwantedFilesFailure::CleanUpOldWorktree {
            source: e,
            worktree: worktree.clone(),
        })?;

    CommandBuilder::new("git")
        .args(&["worktree", "add"])
        .arg(&worktree)
        .args(&["--checkout", raw_file_branch])
        .execute(local_git_repo, debug)
        .await
        .map_err(|e| RemoveUnwantedFilesFailure::CreateNewWorktree {
            source: e,
            worktree: worktree.clone(),
        })?;
    let unexpected_files = old_files.into_iter().filter(|old_file| {
        !desired_files
            .iter()
            .any(|d| d.as_ref() == old_file.as_ref())
    });

    for file_path in unexpected_files {
        let file_path = file_path.as_ref();
        progress(
            format!("Found unexpected file: {}", file_path.as_str()),
            arguments.verbose_logging,
            &arguments.transmitter,
        )
        .await;

        CommandBuilder::new("git")
            .args(&["rm", "-f"])
            .arg(file_path.as_str())
            .execute(&worktree, debug)
            .await
            .map_err(|e| RemoveUnwantedFilesFailure::RemoveFromRepo {
                source: e,
                file: file_path.as_str().to_string(),
                repo: worktree.clone(),
            })?;

        let file_name = file_path
            .file_name()
            .expect("An unexpected file had no file name");
        if !desired_files
            .iter()
            .map(|p| p.as_ref().file_name())
            .any(|p| p == Some(file_name))
        {
            fs::remove_file(git_dir_prefix.join(file_name)).map_err(|e| {
                RemoveUnwantedFilesFailure::DeleteUnwantedFile {
                    source: e,
                    file: git_dir_prefix.join(file_name),
                }
            })?;
            progress(
                format!("Removed unwanted local file {}", file_name),
                arguments.verbose_logging,
                &arguments.transmitter,
            )
            .await;
        }
    }
    progress(
        "Committing deletions of unwanted files.",
        arguments.verbose_logging,
        &arguments.transmitter,
    )
    .await;
    match CommandBuilder::new("git")
        .args(&["commit", "-m", "Remove redundant file paths."])
        .execute(&worktree, debug)
        .await
    {
        Ok(_) => {}
        //TODO couldn't we just track the number of deletions we make in the loop above and then we can treat this as a real error instead of this uncertainty?
        Err(e) => {
            progress(
                "Couldn't commit deletions. Perhaps there weren't any?",
                arguments.verbose_logging,
                &arguments.transmitter,
            )
            .await;
            progress(
                format!("{:?}", e),
                arguments.verbose_logging,
                &arguments.transmitter,
            )
            .await;
        }
    }
    fs::remove_dir_all(&worktree).map_err(|e| RemoveUnwantedFilesFailure::DeleteNewWorktree {
        source: e,
        worktree: worktree.clone(),
    })?;

    CommandBuilder::new("git")
        .args(&["worktree", "prune"])
        .execute(local_git_repo, debug)
        .await
        .map_err(|e| RemoveUnwantedFilesFailure::CleanUpNewWorktree {
            source: e,
            worktree: worktree.clone(),
        })?;
    Ok(())
}
