use std::fs;
use std::path::{Path, PathBuf};

use std::sync::Arc;

use crate::common::{download_file, missing_or_empty};
use crate::lorry_specs::{MultipleRawFiles, RawFileURLMapping};
use crate::{Arguments, CommandExecutionError, InternalGitDirectory};
use futures::TryFutureExt;
use logging::progress;
use relative_path::RelativePath;
use relative_path::RelativePathBuf;
use thiserror::Error;
use utils::command_wrapper::CommandBuilder;

mod check_in;

mod remove_unwanted;

const COMMITTER_NAME: &str = "Lorry Raw File Importer";
const COMMITTER_EMAIL: &str = "lorry-raw-file-importer@lorry";

//TODO should I pass URLs instead of RawFileURLMappings? They aren't public so the caller gets less info.
#[derive(Error, Debug)]
pub enum RawFileMirrorError {
    /// Checking in raw files requires git LFS. When attempting to make sure that LFS is installed for the internal repo, an error occurred.
    /// The output of the verification command is contained.
    #[error("Could not install git LFS to the local repo: {0}")]
    CouldNotInstallLFS(CommandExecutionError),

    /// Could not extract a filename from the URL. This suggests that the URl does not point to a file, and thus a download could not be made.
    #[error("Supplied URL {0} does not seem to be a file")]
    UrlHasNoFilenameComponent(url::Url),

    /// Failed to configure git user credentials in preparation for making commits where the work-tree is cleared of unnecessary files.
    #[error("Could not configure work-tree: {0}")]
    CouldNotWriteConfig(CommandExecutionError),

    /// Tried to check if a given raw file was already downloaded, but there was an error trying to ascertain this. This implies the file exists on disk but we were unable to read its metadata
    #[error(
        "Could not read metadata at {url} into file {target} to verify if it was empty: {source}"
    )]
    CouldNotReadMetaData {
        source: std::io::Error,
        target: PathBuf,
        url: url::Url,
    },

    /// We tried to download the file at this URL, but failed. When trying to clean up the remnants of the failed download, another failure was encountered.
    #[error("Could not clean up the file {path} that we failed to download when working on {lorry:?}: {source}")]
    CouldNotCleanUpFailedDownload {
        source: std::io::Error,
        path: PathBuf,
        lorry: RawFileURLMapping,
    },

    /// An error occurred while trying to clean up files that were present in the repo but no longer necessary (i.e removed from the lorry spec).
    #[error("Was unable to carry out the operations to commit the changes made to the LFS repo: {source}")]
    CouldNotRemoveUnneededFiles {
        source: remove_unwanted::RemoveUnwantedFilesFailure,
    },

    /// An error occurred trying to download the file at the URL.
    #[error("Failed to download the lorry {lorry:?}: {source}")]
    DownloadFailed {
        source: crate::common::FileDownloadError,
        lorry: RawFileURLMapping,
    },

    /// An error occurred trying to commit the given file into the git LFS. The the `source` field should contain more information.
    #[error("failed to check the file {file} into git LFS at {repo}: {source}")]
    CheckInFailed {
        source: check_in::CheckInError,
        repo: InternalGitDirectory,
        file: PathBuf,
    },
}

/// Try to download each url contained in the lorry specs, and check if they have not already been checked into the LFS repo.
/// If so, check them into git-lfs and commit the new files into the repository in preparation for pushing to the downstream.
/// Also deletes files that were checked in but are no longer part of the specs file.
///
/// * `specs` - the lit of URL-path pairs contained in the .lorry file that we are expected to mirror.
///
/// * `working_dir` - the workspace directory containing the working repos
///
/// * `local_git_repo` - the internal repository that is being modified to become the next up-to-date version
///
/// * `arguments` - the arguments passed to lorry, used to pass around options about logging and the file downloads.
pub(crate) async fn mirror_raw_file_into_lfs<P: AsRef<Path>>(
    specs: &MultipleRawFiles,
    working_dir: P,
    local_git_repo: &InternalGitDirectory,
    arguments: &Arguments,
) -> Result<(), RawFileMirrorError> {
    let debug = |s: String| async move { logging::debug(&s, &arguments.transmitter).await };

    //fetch the upstream and figure out which files are present there

    //make sure LFS is installed for the local repo
    CommandBuilder::new("git")
        .args(&["lfs", "install", "--local"])
        .execute(local_git_repo, debug)
        .await
        .map_err(RawFileMirrorError::CouldNotInstallLFS)?;
    //go though each destination/url pair, and move the file at the URL to the destination if required, recording which ones were copied
    //TODO this is a nightmare that's basically just copied directly from the python version. Condense it.
    //list of all files in downstream from the beginning
    let old_files = CommandBuilder::new("git")
        .args(&["ls-tree", "-r", "HEAD", "--name-only"])
        .execute(local_git_repo, debug)
        .await
        .map(|(out, _err)| {
            out.trim()
                .split('\n')
                .map(RelativePathBuf::from)
                .collect::<Vec<_>>()
        })
        .unwrap_or(vec![]);

    //TODO the Rcs have become Arcs now, which are a bit expensive. Figure out a way around this!
    let mut new_files = Vec::<(&RelativePath, Arc<PathBuf>)>::new();
    let mut desired_files = vec![RelativePathBuf::from(".gitattributes")];
    let urls = &specs.urls;
    for pair in urls {
        let url = &pair.url;
        let base_filename = url
            .path_segments()
            .and_then(|spl| spl.last())
            .ok_or_else(|| RawFileMirrorError::UrlHasNoFilenameComponent(url.clone()))?;

        let file_destination = Arc::new(working_dir.as_ref().join(base_filename));

        let repo_subdir: &RelativePath = pair.destination.as_relative_path();
        let repo_destination: RelativePathBuf = repo_subdir.join_normalized(base_filename); //Given these are relative paths, I assume I don't need to do the equivalent of the os.path.relpath call
        progress(
            format!("Checking to see if we need to fetch {}", base_filename),
            arguments.verbose_logging,
            &arguments.transmitter,
        )
        .await;

        if missing_or_empty(&file_destination).map_err(|e| {
            RawFileMirrorError::CouldNotReadMetaData {
                source: e,
                target: file_destination.as_path().to_path_buf(),
                url: url.clone(),
            }
        })? {
            new_files.push((repo_subdir, Arc::clone(&file_destination)));
            progress(
                format!("Attempting to fetch {}", base_filename),
                arguments.verbose_logging,
                &arguments.transmitter,
            )
            .await;

            if let Err(e) = download_file(
                &file_destination,
                url,
                specs.check_ssl_certificates,
                arguments,
            )
            .await
            {
                //we could not fetch the file.
                progress(
                    format!("Could not fetch {}", base_filename),
                    arguments.verbose_logging,
                    &arguments.transmitter,
                )
                .await;

                if file_destination.exists() {
                    fs::remove_file(file_destination.as_ref()).map_err(|e| {
                        RawFileMirrorError::CouldNotCleanUpFailedDownload {
                            source: e,
                            path: file_destination.as_ref().to_path_buf(),
                            lorry: pair.clone(),
                        }
                    })?;
                }
                return Err(RawFileMirrorError::DownloadFailed {
                    source: e,
                    lorry: pair.clone(),
                });
            };
        } else if !old_files.contains(&repo_destination) {
            new_files.push((repo_subdir, Arc::clone(&file_destination)));
            progress(
                format!("Path changed for {}", base_filename),
                arguments.verbose_logging,
                &arguments.transmitter,
            )
            .await;
        } else {
            progress(
                format!("No need to import {}", base_filename),
                arguments.verbose_logging,
                &arguments.transmitter,
            )
            .await;
        }
        desired_files.push(repo_destination);
    }

    //Then run the raw file importer sequence which checks the file into LFS
    if new_files.is_empty() {
        progress(
            "No need to run the raw file importer.",
            arguments.verbose_logging,
            &arguments.transmitter,
        )
        .await;
    }

    //TODO we could move this up into the loop above instead an eliminate the need for (A)Rcs. However, this would change the oder of downloads, checking in, and debugs.
    for (sub_path, raw_file) in new_files {
        progress(
            format!("Checking in file {}", raw_file.to_string_lossy()),
            arguments.verbose_logging,
            &arguments.transmitter,
        )
        .await;
        check_in::check_file_into_lfs(&raw_file, sub_path, local_git_repo, &arguments.transmitter)
            .await
            .map_err(|e| RawFileMirrorError::CheckInFailed {
                source: e,
                repo: local_git_repo.to_owned(),
                file: raw_file.to_path_buf(),
            })?;
    }

    //create new work-tree
    CommandBuilder::new("git")
        .args(&["config", "user.name", COMMITTER_NAME])
        .execute(local_git_repo, debug)
        .and_then(|_| {
            CommandBuilder::new("git")
                .args(&["config", "user.email", COMMITTER_EMAIL])
                .execute(local_git_repo, debug)
        })
        .await
        .map_err(RawFileMirrorError::CouldNotWriteConfig)?;

    remove_unwanted::remove_unwanted_files(
        local_git_repo,
        crate::DEFAULT_BRANCH_NAME,
        old_files,
        desired_files,
        arguments,
    )
    .await
    .map_err(|e| RawFileMirrorError::CouldNotRemoveUnneededFiles { source: e })?;

    Ok(())
}
