use std::path::PathBuf;

use clap::Parser;
use serde::Deserialize;
use serde_with::{serde_as, DisplayFromStr};
use tracing::Level;

//TODO recent change means this can be changed quite a bit;
//shouldn't need unwraps, just use a default() Arguments as the accumulator!
// Once we do that, we can move this up from the lib to the bin, since `lorry`
//bin is the only place using it

/// Combines the fields of the given configuration fragments to get a full set of arguments to pass to the `mirror` function.
/// The most recent value for a given setting takes precedence. Should no value be provided, this function provides a sensible default.
/// Also returns, in order:
/// * A boolean indicating if to stop after the file parsing step of each lorry: this would be used to test if the files re valid without also trying to carry out the full mirror.
/// * Whether to use verbose debugging or not
/// * The logging target. TODO this should not be a string!
/// * The logging level limit - messages less important than this are discarded. //TODO but lorry doesn't need the full logging level struct!
pub fn combine_configs<T>(
    config_files: T,
) -> (
    crate::Arguments,
    bool,
    bool,
    Option<Level>,
    logging::Sender,
    logging::Receiver,
)
where
    T: IntoIterator<Item = PartialArguments>,
{
    let final_args = config_files
        .into_iter()
        .fold(PartialArguments::default(), |acc, cur| PartialArguments {
            working_area: cur.working_area.or(acc.working_area),
            pull_only: cur.pull_only.or(acc.pull_only),
            verbose: cur.verbose.or(acc.verbose),
            repack: cur.repack.or(acc.repack),
            keep_multiple_backups: cur.keep_multiple_backups.or(acc.keep_multiple_backups),
            push_options: cur.push_options.or(acc.push_options), //TODO should push-options be concatenated rather than substituted? old cliapp code didn't, but maybe that is more logical
            check_certs: cur.check_certs.or(acc.check_certs),
            test_parsing_only: cur.test_parsing_only.or(acc.test_parsing_only),
            log_level: cur.log_level.or(acc.log_level),
            maximum_redirects: cur.maximum_redirects.or(acc.maximum_redirects),
        });
    let (tx, rx) = tokio::sync::mpsc::unbounded_channel::<(logging::LogLevel, String)>();
    //these unwraps are OK because all the defaults are non-None
    (
        crate::Arguments {
            working_area: ensure_path_exists(&final_args.working_area.unwrap()),
            pull_only: final_args.pull_only.unwrap(),
            verbose_logging: final_args.verbose.unwrap(),
            repack: final_args.repack.unwrap(),
            keep_multiple_backups: final_args.keep_multiple_backups.unwrap(),
            push_options: final_args.push_options.unwrap(),
            check_ssl_certificates: final_args.check_certs.unwrap(),
            transmitter: tx.clone(),
            maximum_redirects: final_args.maximum_redirects.unwrap_or(0),
            log_level: final_args.log_level,
        },
        final_args.test_parsing_only.unwrap(),
        final_args.verbose.unwrap(),
        final_args.log_level,
        tx,
        rx,
    )
}
///creates the path if required and returns the absolute version of the path
fn ensure_path_exists(path: &std::path::PathBuf) -> std::path::PathBuf {
    std::fs::create_dir_all(path).unwrap();
    path.canonicalize().unwrap()
}
//TODO the partial arguments stuff can be moved to the binary, now that it's the on;y thing using this.
#[serde_as]
#[derive(Parser, Debug, Deserialize, Clone)]
#[command(author, version, about, long_about = None)]
#[serde(rename_all = "kebab-case")]
pub struct PartialArguments {
    ///Directory for holding intermediate git repos and other internal data (default: workd/)
    #[arg(long = "working-area", short = 'w')]
    pub working_area: Option<PathBuf>,

    ///only pull from upstreams, do not push to mirror server (default:disabled)
    #[arg(long = "pull-only")]
    pub pull_only: Option<bool>,

    ///report to STDOUT (default:disabled)
    #[arg(long = "verbose", short)]
    pub verbose: Option<bool>,

    ///repack git repos when an import has been updated (default:enabled)
    #[arg(long = "repack")]
    pub repack: Option<bool>,

    ///keep multiple timestamped backups (default:disabled)
    #[arg(long = "keep-multiple-backups")]
    pub keep_multiple_backups: Option<bool>,

    ///option for `git push` to pass to the downstream server
    #[arg(long = "push-option")]
    pub push_options: Option<Vec<String>>,

    ///validate SSL/TLS server certifications (default:enabled)
    #[arg(long = "check-certificates")]
    pub check_certs: Option<bool>,

    //TODO `test_parsing_only`, `log`, and `log_level` aren't used by minion, why are they here...
    ///validate `.lorry` files only, do not carry out mirroring operations. (default:disabled)
    #[arg(long = "test-parsing-only")]
    pub test_parsing_only: Option<bool>,

    /// Maximum number of HTTP redirections to follow when downloading raw-files
    #[arg(long = "maximum-redirects")]
    pub maximum_redirects: Option<u32>,

    #[serde_as(as = "Option<DisplayFromStr>")]
    #[arg(long = "log-level")]
    pub log_level: Option<Level>,
}

impl Default for PartialArguments {
    fn default() -> PartialArguments {
        PartialArguments {
            working_area: Some(PathBuf::from("workd")),
            pull_only: Some(false),
            verbose: Some(false),
            repack: Some(true),
            keep_multiple_backups: Some(false),
            push_options: Some(vec![]),
            check_certs: Some(true),
            test_parsing_only: Some(false),
            maximum_redirects: None,
            log_level: Some(Level::DEBUG),
        }
    }
}
