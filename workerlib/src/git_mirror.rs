use crate::{Arguments, InternalGitDirectory};
use futures::Future;
use std::path::Path;
use thiserror::Error;
use utils::command_wrapper::{CommandBuilder, CommandExecutionError};

#[derive(Error, Debug)]
pub enum GitMirrorError {
    /// The git fetch command to the updtream folder failed. The results of the command execution are contained.
    #[error("Could not fetch from upstream")]
    CouldNotFetch {
        source: CommandExecutionError,
        upstream: String,
    },
    #[error(transparent)]
    Command(#[from] CommandExecutionError),
}

/// Parse a vector of refspecs into matching refs in the local repo. Supports
/// globbing. If a refspec is of the form `+asdf:asdf` then only the local
/// refspec is used (i.e. what comes after the colon)
async fn parse_refspecs<F: Future<Output = ()>>(
    git_repo: impl AsRef<Path>,
    refspecs: &[String],
    debug: impl Fn(String) -> F + Copy,
) -> Result<Vec<String>, GitMirrorError> {
    let (stdout, _) = CommandBuilder::new("git")
        .args(&["for-each-ref", "--format=\"%(refname)\""])
        .args(
            &refspecs
                .iter()
                .map(|r| r.split(':').last().unwrap())
                .collect::<Vec<_>>(),
        )
        .execute(git_repo, debug)
        .await?;
    // Annoyingly the stdout gets wrapped in quotes so we have to remove them
    Ok(stdout.lines().map(|r| r.replace('"', "")).collect())
}

/// mirrors a git repo by pulling down the upstream.
pub(crate) async fn fetch_git_repo(
    git_repo: &InternalGitDirectory,
    upstream_url: &str,
    args: &Arguments,
    check_certificates: bool,
    refspecs: Option<Vec<String>>,
) -> Result<(), GitMirrorError> {
    let debug = |s: String| async move { logging::debug(&s, &args.transmitter).await };

    let refspecs = refspecs.unwrap_or(vec![
        "refs/heads/*:refs/heads/*".to_owned(),
        "refs/tags/*:refs/tags/*".to_owned(),
    ]);

    let mut c = CommandBuilder::new("git")
        .args(&[
            "-c",
            "gc.autodetach=false",
            "fetch",
            "--prune",
            upstream_url,
        ])
        .args(&refspecs);

    if !check_certificates {
        c = c.env(&[("GIT_SSL_NO_VERIFY", "true")]);
    }

    c.execute(git_repo, debug)
        .await
        .map_err(|e| GitMirrorError::CouldNotFetch {
            source: e,
            upstream: upstream_url.to_owned(),
        })?;

    if let Err(err) =
        fetch_lfs_objects(git_repo, debug, upstream_url, refspecs, check_certificates).await
    {
        logging::debug(
            format!("Failed to fetch LFS objects: {err}"),
            &args.transmitter,
        )
        .await;
    };
    Ok(())
}

async fn fetch_lfs_objects<F: Future<Output = ()>>(
    git_repo: &InternalGitDirectory,
    debug: impl Fn(String) -> F + Copy,
    upstream_url: &str,
    refspecs: Vec<String>,
    check_certificates: bool,
) -> Result<(), GitMirrorError> {
    CommandBuilder::new("git")
        .args(&["lfs", "install", "--local"])
        .execute(git_repo, debug)
        .await?;
    let mut c = CommandBuilder::new("git")
        .args(&[
            "-c",
            "gc.autodetach=false",
            "lfs",
            "fetch",
            "--prune",
            upstream_url,
        ])
        // git-lfs-fetch can't handle proper refspecs with wildcards so we have
        // to parse them first
        .args(&parse_refspecs(git_repo, &refspecs, debug).await?);
    if !check_certificates {
        c = c.env(&[("GIT_SSL_NO_VERIFY", "true")]);
    }
    c.execute(git_repo, debug).await?;
    Ok(())
}
