# Running Lorry controller et al. in Docker

There is a `Dockerfile` in the root of this repository which can be
used to create a container capable of running Lorry, the controller,
and the worker. This is a deliberately quite bare-bones container
so that it should not be too onerous to run on your systems.

In this directory there is an example compose file as well as
example configurations, though they will not work without personalisation.

The following is a step-by-step guide to running Lorry for your
project using the docker container and this compose set as an example.

# Prerequisites

1. You need a user on a Gitlab instance which will be the owner for your
   mirrored repositories and the configuration for Lorry
2. You will need a full personal access token to hand. If you wish the worker to
   push over SSH (rather than HTTP), you will also need an SSH key attached to
   that user.
3. You will need a group created on the Gitlab instance to which that user
   has full rights
4. You will need a repository on that (or another) Gitlab instance which
   that user is able to read (typically these are publically readable).

# Preparation

1. First set up the prerequisites above.
2. If using SSH, place a copy of the **private** SSH key into the file
   `lorry-key` in this directory.
3. Edit the configuration files `config/controller.conf` and `config/worker.conf`
   to match your configuration needs. Pay attention to any lines marked `TODO`.
4. Start the compose setup with the following (with `gitlab.com` replaced with
   the Gitlab host you are using):

   ```shell
   env LORRY_SSH_KEY="$(cat lorry-key)" LORRY_SSH_KNOWN_HOSTS=gitlab.com docker-compose up
   ```

   You can drop the SSH env vars if the worker is pushing over HTTP.

To check that your lorry controller is running and alive, try:

```shell
curl http://127.0.0.1:3000/1.0/health-check
```

If that returns without error then the controller is up and running.

# Configuration

The next step is to set up the configuration git repository which the Lorry
Controller will be working with.

Check out the repository you prepared as prerequisite 4, and create a file at the top level
of it called `lorry-controller.conf`

That file contains a list of stanzas, each configuration stanza describes a set of
lorries to be run. You can find more information in `README.md` in the root of the
repository. For now, create a single entry of the form:

```json
[
  {
    "type": "lorries",
    "interval": "PT1H",
    "timeout": "PT3H",
    "prefix": "mygroupname",
    "globs": ["lorry/*.lorry"]
  }
]
```

(Obviously substitute `mygroupname` for your group name on your Gitlab instance.)

Then make a directory called `lorries` and in there place a single file called
something like `lorry2.lorry` with the following content:

```yaml
lorry/lorry2:
  type: git
  url: https://gitlab.com/Codethinklabs/lorry/lorry2
```

Add those files, commit, and push them to the server. Next see if the lorry has
been loaded with:

```shell
curl http://127.0.0.1:3000/1.0/list-lorries
```

Assuming it has loaded the lorry, after about a minute, the worker ought to pick it
up and run the lorry process. Once that has happened you can check in your Gitlab
group and you should see a subgroup called `lorry` with a repo called `lorry2` inside
it which is a mirror of this repository.
