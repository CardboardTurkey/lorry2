# Lorry 2

[[_TOC_]]

Lorry 2 is a mirroring service primarily for git repositories, designed to allow for changes to upstream projects
without affecting downstream dependencies. This is managed by tracking an upstream repository and pushing any changes to
a separate repository held elsewhere, with manual intervention required to remove references or modify history.

This project is currently under heavy development, and as such is still not complete. Also note that this version will
not be a drop in replacement for the previous version of [Lorry], and as such will require some form of configuration
changes to upgrade to this version.

## Reimplementation in Rust

Lorry 2 is a full reimplementation of the original [Lorry] tool in Rust.

Some Lorry features which had little to no uptake by users have been dropped:

- Cascading configuration files
- Bundling or tarball-ing of mirrors
- Support for upstreams which are not `git` or `raw files`

Note: For the rest of this documentation, when referring to Lorry this refers to the version in this repository, and not
the previous implementation.

# Usage

Lorry is split into two applications: the controller and the worker. The controller is responsible for tracking the main
mirror configuration (called `confgit`) and distributing work among the various workers.

To configure Lorry completely, you require several pre-configured items and secrets:

* GitLab Instance - this can be the public GitLab.com, or a self-hosted private instance.
* Mirror Group (or subgroup) in GitLab - this will be where Lorry will store the mirrors.
* GitLab Access Token - this can be either a personal, group, or project access token with the `api` scope for the
  Mirror Group above.
* SSH Private Key or Gitlab Access Token - this will be used to push the git mirrors, and should either be configured
  for a user who will have write access to the Mirror Group, or an access token with the `write_repository` scope for
  the Mirror Group above.
* Some repositories to mirror! How about this one?

If you are running the binaries directly rather than using the docker container, then you will also need the following
installed:

* `git`
* `git-lfs`
* `curl`
* `libcurl`
* `libssl`

## Shared Configuration

The following options are common to all applications:

* `--config` No Default
  * Location of an optional configuration file. The config file can contain any other options available on the
    command line for each of the applications.
  * This option is not available in the config file.
* `--log-level` Default: `info`
  * The logging output level for the application. Available options
    are `debug`, `info`, `warning`, or `error`.
  * Config file: `log-level`

## Controller Configuration

The following options are unique to the controller. These can be provided either on the command line, or in a
configuration file. Note that any options passed on the command line will take precedence over any options in the
configuration file.

* `--statedb` **Required**
  * URI pointing to the backing database. Currently, this only supports a `sqlite` database. The URI should include
    the `sqlite://` protocol identifier in this case. Example value for a database at `/db/sqlite.db` would
    be `sqlite:///db/sqlite.db`. This option is required.
  * Config file: `statedb`
* `--port` Default: `3000`
  * The port for the controller to listen on. This covers both user requests and worker communication.
  * Config file: `port`
* `--confgit-url` **Required**
  * URL for the mirror configuration repository.
  * Config file: `confgit-url`
* `--confgit-branch` **Required**
  * Branch to use for the mirror configuration repository.
  * Config file: `confgit-url`
* `--confgit-update-period`
  * Interval between lorry checking `confgit` for updates. Express in ISO8601
    duration format. Defaults to 2 minutes.
  * Config file: `confgit-update-period`
* `--remove-ghost-period`
  * Interval between lorry removing ghost jobs. Express in ISO8601 duration
    format. Defaults to 2 minutes.
  * Config file: `remove-ghost-period`

* `--configuration-directory` Default: `.` (current directory)
  * The local directory to clone the mirror configuration repository in to (as specified by `confgit-url`).
  * Config file: `configuration-directory`

The following positional commands exist on the controller:

* `downstream`
  * The "downstream" location to mirror to. Currently only accepts `gitlab` as the option. This is a positional
    command, so would be called as `lorry-controller <options> gitlab`
  * Config file: `downstream`

Based on the setting of the `downstream` option, extra options are required. For the `gitlab` option:

* `--hostname` **Required**
  * The host name for the downstream host server.
  * Config file: `hostname`
* `--visibility` **Required**
  * The visibility setting to use when the controller creates a repository for minions to mirror to.
    Possible values are `public`, `internal` and `private`.
  * Config file: `visibility`
* `--gitlab-private-token` **Required**
  * A GitLab Personal Access Token (or Group Access Token) used for access to the GitLab API. This should be from the
    same GitLab instance as specified in `hostname` with the `api` scope. This token requires the ability to create
    repositories and groups on the target GitLab.
  * Config file: `gitlab-private-token`
* `--gitlab-insecure-http` Default: `false`
  * Allow connection to the GitLab API using http rather than https. Useful in self-hosted or containerized setups.
  * Config file: `gitlab-insecure-http`

### Configuration File Example

The following is a basic configuration file with all the options. This is written in TOML format

```toml
# Lorry Controller configuration

# Database storing all state and error logs
statedb = "sqlite://sqlite.db"

# Port for the controller to listen on
# port = 3000

# Enable more logging
# log-level = "info"

# Mirror configuration
confgit-url = "https://gitlab.com/mygroup/lorry2-mirror-config"
confgit-branch = "main"

# Directory to store the mirror configuration
# configuration-directory = "."

# Downstream mirror storage
downstream = "gitlab"

# GitLab specific downstream options
hostname = "gitlab.com"
visibility = "internal"
gitlab-private-token = "GITLAB-PRIVATE-TOKEN"
# Set to true if using over http
# gitlab-insecure-http = false
```

### User Endpoints

The following endpoints are available for users to interact with the controller.

#### Health Check

`GET /1.0/health-check`

Returns `200 OK` if there are no issues, or an error message if something is wrong.

#### List Jobs

`GET /1.0/list-jobs`

Lists of all the jobs that have been given to workers.

Example output:

```json
[
  {
    "id": 1,
    "path": "lorry-mirrors/github/octocat/hello-world",
    "exit_status": {
      "Finished": {
        "exit_code": 0,
        "disk_usage": 311296
      }
    },
    "host": "worker-0"
  }
]
```

#### List Lorries

`GET /1.0/list-lorries`

Lists all the lorries configured on the controller.

Example Output:

```json
[
  {
    "path": "lorry-mirrors/github/octocat/hello-world",
    "name": "lorry-mirrors/github/octocat/hello-world",
    "spec": {
      "type": "git",
      "url": "https://github.com/octocat/Hello-World.git",
      "check-certificates": true,
      "refspecs": null
    },
    "running_job": null,
    "last_run": 1696523383,
    "interval": "PT60S",
    "lorry_timeout": "PT60S",
    "last_run_results": {
      "Finished": {
        "exit_code": 0,
        "disk_usage": 311296
      }
    },
    "last_run_output": "...",
    "purge_before": 1696512967
  }
]
```

#### Purge

`POST /1.0/purge`

Sets the worker repositories to be purged before mirroring again. This has no effect against the downstream repository.

Example Output:

```text
Repo has been marked for deletion from local systems
```

#### Report Errors

`GET /1.0/errors`

Returns a json array of all errors from workers and the controller.

Example Output:

```json
[
  {
    "error_message": "Some Error Message",
    "endpoint": "",
    "timestamp": 1696513195,
    "for_lorry": "lorry-mirrors/github/octocat/hello-world"
  }
]
```

## Worker Configuration

The following options are unique to the worker. These can be provided either on the command line or in a configuration
file. Note that any options passed on the command line will take precedence over the configuration file.

* `--controller-address` Default: `http://0.0.0.0:3000`
  * URL of the controller for this worker. Used for requesting jobs.
  * Config file: `controller-address`
* `--controller-timeout` Default: `10`
  * Timeout for response from controller.
  * Config file: `controller-timeout`
* `--username`
  * Username to be used by minion when pushing to mirror via `http(s)`. Must be
    specified along side `private-token`. If neither are specified, minion will
    default to using `ssh`.
  * Config file: `username`
* `--private-token`
  * Private token to be used by minion when pushing to mirror via `http(s)`.
    Must be specified along side `username`. If neither are specified, minion
    will default to using `ssh`.
  * Config file: `private-token`
* `--host-name`
  * Host name for the git server that the minion pushes to. If not specified, minion will use same host name as for the controller.
  * Config file: `host-name`
* `--port`
  * Port for the git server that the minion pushes to. If not specified, minion will use same port as for the controller.
  * Config file: `port`
* `--sleep` Default: Random time between 30 and 60 seconds
  * Time to sleep between requests for jobs from the controller.
  * Config file: `sleep`
* `--working-area` Default: `workd`
  * Directory to perform mirroring operations inside.
  * Config file: `working-area`
* `--repack` Default: `true`
  * Whether to run `git gc` on mirrored repos before pushing to the downstream mirror.
  * Config file: `repack`
* `--keep-multiple-backups` Default: `false`
  * Store a timestamped backup of the repository folder after mirroring
  * Config file: `keep-multiple-backups`
* `--push-option` Optional
  * Additional push option arguments to pass to git during `git push` stage. See [git push documentation]
    and [GitLab push options][gitlab-push-options] for details on what options are available. This option can be
    specified multiple times on both command line and in config file.
  * Config file: `push-option`
* `--check-certificates` Default: `true`
  * Whether to validate upstream SSL/TLS certificates for mirrors.
  * Config file: `check-certificates`
* `--pull-only` Default: `false`
  * Disable push to downstream repository. Only really useful for debugging purposes.
  * Config file: `pull-only`
* `--verbose` Default: `false`
  * Print mirroring logs to stderr. Mirroring logs are only sent to the controller by default.
  * Config file: `verbose`

### Configuration File Example

The following is a basic configuration file with all available options. This is written in TOML format.

```toml
# Lorry Worker configuration

# Controller address for this worker
# controller-address = "http://0.0.0.0:3000"
# Timeout for response from the controller
# controller-timeout = 10

# Time to wait between requests for work to the controller
# sleep = 30

# Main working directory for mirrors
# working-area = "workd"

# Run garbage collection on the mirror before pushing downstream
# repack = true

# Keep a timestamped backup of each mirror attempt
# keep-multiple-backups = false

# Extra options to pass to git push
# push-option = "some-option"
# push-option = "another-option"

# Validate upstream SSL/TLS certificates
# check-certificates = true

# Disable pushing to downstream
# pull-only = false

# Print mirror logging to stderr
# verbose = false
```

## Mirror Configuration

The mirror configuration is where the link between upstream repositories and downstream mirrors are defined. There are
some basic requirements for this:

* Must be a git repository
* Must have a JSON file called `lorry-controller.conf` in the root of the repository.

The repository is specified in the `confgit-url` and `confgit-branch` settings for the controller, and is cloned or
updated when the **Read Configuration** endpoint is accessed.

### Lorry Controller Configuration

The main `lorry-controller.conf` file consists of a list of objects, containing the following required keys and values:

* `type` - String that should be set to `lorries`.
* `interval` - String in ISO8601 duration format. For example `PT3H` corresponds to a 3-hour duration. Specifies the
  interval for mirroring the various lorry configs in this group.
* `timeout` - String in ISO8601 duration format, see `interval`. If mirroring one of the lorry configs takes longer than
  the timeout, it will be cancelled.
* `prefix` - String specifying the downstream group prefix. This is prefixed to the individual lorry names in this group
  on the downstream repository.
* `globs` - Array of Strings specifying the file globs containing the individual lorry configurations. For example,
  the `folder/*.lorry` example given will look for all `.lorry` files in the `folder` directory (
  relative to the `lorry-controller.conf` file)

An example configuration file would look like this:

```json
[
  {
    "type": "lorries",
    "interval": "PT1M",
    "timeout": "PT1M",
    "prefix": "lorry-mirrors/github",
    "globs": [
      "github.lorry"
    ]
  }
]
```

This would mirror any repositories specified in a `github.lorry` file every minute.

The individual lorry mirror configurations are YAML files. These are in the form:

```yaml
mirror-name:
  type: mirror type
  # further mirror config
```

The mirror type currently can be either `git` or `raw-file`. This determines the extra mirror configuration required.

When the mirror type is `git`, the extra configuration options are:

* `url` **Required** - String of the git URL for the repository.
* `check-certificates` Default: `true` - Boolean if the SSL/TLS certificate for the specific repository should be
  checked. If the worker level `check-certificates` option is set to false, this will not turn the checking of
  certificates back on, it can only disable the checking of certificates for the current mirror.
* `refspecs` Optional - String in format of https://git-scm.com/book/en/v2/Git-Internals-The-Refspec to mirror only matched refspecs

When the mirror type is `raw-file`, the extra configuration options are:

* `urls` **Required** - List of URL mappings, with the following keys:
  * `url` **Required** - String of the file URL to download.
  * `destination` **Required** - String of the directory to store the downloaded file in.
* `check-certificates` Default: `true` - Boolean if the SSL/TLS certificate for the files should be checked. If the
  worker level `check-certificates` option is set to false, this will not turn the checking of certificates back on, it
  can only disable the checking of certificates for the current mirror.
* `refspecs` Optional - String in format of https://git-scm.com/book/en/v2/Git-Internals-The-Refspec to mirror only matched refspecs

With the above, an example lorry mirror configuration could look like the following:

```yaml
octocat/hello-world:
  type: git
  url: https://github.com/octocat/Hello-World.git

raw-files:
  type: raw-file
  urls:
    - destination: target-directory
      url: https://my-file-host.tld/directory/more-directory/file.tar
    - destination: another-target-directory
      url: https://my-file-host.tld/directory/another_file.tar
```

If this was used with the controller configuration above, then the two repositories created in your GitLab group would
be at:

* `lorry-mirrors/github/octocat/hello-world`
* `lorry-mirrors/github/raw-files`

## Known limitations

### GitLab LFS check

When receiving pushes to a repository which has LFS enabled (which is currently
all repositories which Lorry creates) GitLab attempts to perform an LFS pointer
check.

For particularly large repos (e.g. the linux kernel) the time taken for this can
be on the order of tens of seconds. GitLab applies the [Medium] timeout to this
operation which defaults to 30s. Hence there is potential for the push to fail
just because the LFS check took too long.

[Medium]:
    https://docs.gitlab.com/ee/administration/settings/gitaly_timeouts.html#available-call-timeouts

One option for circumnavigating this issue is to reconfigure the GitLab
instance's call timeouts. Codethink found that setting the Medium timeout to 90s
(which required raising the Default timeout which in turn required raising the
`max_request_duration_seconds`) was enough for mirroring the kernel on our
internal infrastructure.

If the repository you are mirroring does not use git LFS you also have the
option of disabling LFS just on that mirror. That should also disable the LFS
check.

### SHA-like tags

GitLab does not allow tags that look like a SHA-1 or SHA-256 checksums. GitHub,
on the other hand, does allow such tags and this can prevent lorry from
successfully mirroring GitHub projects. Check for `GitLab: You cannot create a
tag with a SHA-1 or SHA-256 tag name.` in the error message.

Note this requirement from GitLab does not appear to be documented, but you can
see the checks in the source code easily enough:
<https://gitlab.com/gitlab-org/gitlab/-/blob/master/spec/lib/gitlab/checks/tag_check_spec.rb#L88-130>.

# Development

## Dependencies

Requires Rust and Cargo. other dependencies are handled by `cargo`. To handle raw-file mirrors, you should also have Git
LFS installed on the machine running `lorry-worker` and on your upstream.

Currently, we also depend on `curl`, but have an issue to remove that dependency.

## DB setup instructions

To compile the distributed lorry system, you may need to rebuild the `sqlx-data.json` file, which contains information
about the database necessary for SQLX to type-check your code against. To do this, you will need to
run `cargo sqlx prepare --merged` before compilation. Note that you may need to pass `--workspace`,instead, as SQLX are
moving between the two flag names.

If you're moving from the old python version of lorry-controller, you'll need to first convert the sqlite database
backing lorry-controller from being managed by `yoyo` to being managed by` sqlx`. This is done by executing
the `patch_column_types.sql` and `convert_from_yoyo.sql` queries on the database:

```
sqlite3 lorries.db '.read patch_column_types.sql'
sqlite3 lorries.db '.read convert_from_yoyo.sql'
```

Contributions that add database migrations or create, modify, or remove queries will be required to regenerate
the `sqlx-data.json` file. This can be done by running

```bash
sqlx database create
sqlx migrate run --source distributed/migrations
```

to create a database with all the migrations, and then running `cargo sqlx prepare --merged` as above to generate the
JSON. The changes to this file should be tracked in Git.

# Migration from Lorry 1

The biggest difference between Lorry 1 and Lorry 2 are the configuration files, both for the workers and controller (or
webapp in Lorry 1) as well as the confgit repo. Thankfully as the controller configuration for Lorry 2 includes a
branch, this can be used to make migration easier.

## Confgit Migration

The first step will be to migrate the confgit configuration. For this, if your individual lorries are in yaml format,
then nothing is going to need to be changed. The biggest change is to the root `lorry-configuration.conf` where several
options have changed. You may also need to change/drop certain mirror files themselves.

1. Convert any JSON based lorry configs to YAML.
2. Convert the `lorry-configuration.conf` file:

* `type` stays the same (the value `lorries`)
* `interval` convert to ISO8601 Duration format
* `lorry-timeout` convert to ISO8601 Duration format and rename key `timeout`. This is now a required key.
* `prefix` stays the same
* `globs` stays the same
* any other keys should be discarded (but will be ignored if left)

3. Convert mirror files to be lorry2 compatible by running:

  * The `git` type is supported as the same usage
  * The `raw-file` type is supported as the same usage
  * The `tarball` type is no longer supported, so repos with `type: tarball` need to be dropped.
  * Mercurial upstream is no longer supported so you may need to drop mirrors with
    `type: hg` or find a `git` host of them.
  * Bazaar upstream is no longer supported so you may need to drop mirrors with
    `type: bzr` or find a `git` host of them.
  * Subversion upstream is no longer supported so you may need to drop mirrors with
    `type: svn` or find a `git` host of them.
  * CVS upstream is no longer supported so you may need to drop mirrors with
    `type: cvs` or find a `git` host of them.
  * ZIP upstream is no longer supported so you may need to drop mirrors with `type: zip`
  * GZIP upstream is no longer supported so you may need to drop mirrors with `type: gzip`

For `type: tarball`, `type: zip` and `type: gzip`, it might be possible
to use `type: raw-file` instead for these purpose. But bear in mind that
`type: tarball`, `type: zip` and `type: gzip` used to first expand the
compressed file and then commit the extracted content while `raw-file` just
simply pushes the compressed file.

To automate step 3 we internally use a script [`convert_to_lorry2.py`](./scripts/convert_to_lorry2.py).
This may be useful for your own conversion. Its usage is:

    python convert_to_lorry2.py mirror_config_repo

`mirror_config_repo` should be the root directory of the mirroring-config repo. This script will
automatically drop all repos with the unsupported types as listed above.

This can all be done in a separate branch to your main configuration, allowing for quick rollback in case of an issue.

## Controller Configuration

The controller migration will be taking some values from the webapp configuration. Using our example configuration
above, you will want to pull the following information from your webapp config:

* `confgit-url` Copy this over (should be the same repo as you edited in the previous migration step)
* `confgit-branch` Set this to your new branch with the migrated configuration
* `hostname` Take the hostname from the `downstream-http-url`, removing any scheme prefix (such as `https://`)
* `visibility` Use the value of `downstream-visibility`
* `gitlab-private-token` Copy from value of the same name (or create a new one specifically for lorry 2!)
* `configuration-directory` If set, copy this across as well, or change as needed.

Check over the other settings available for the controller as well, there are several which did not exist on Lorry 1 and
are mandatory, such as `statedb`.

## Worker Configuration

The worker migration will take some values from the original minion config. Note that your config may be split across
multiple files (which is not supported in Lorry 2). Using our example configuration from above, you will want to pull
the following values:

* `working-area` Copy this setting across or modify as needed for the new deployment
* `controller-address` Build this from the `webapp-host` and `webapp-port`
* `controller-timeout` Copy from `webapp-timeout`
* `working-area` Copy from `lorry-working-area`

Check the other available options as well, and then your worker config is ready. With these configurations ready, you
can now deploy Lorry 2!

[Lorry]: https://gitlab.com/CodethinkLabs/lorry/lorry

[git push documentation]: https://git-scm.com/docs/git-push#Documentation/git-push.txt---push-optionltoptiongt

[gitlab-push-options]: https://docs.gitlab.com/ee/user/project/push_options.html
