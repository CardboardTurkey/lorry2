use std::fmt::Display;

use serde::Deserialize;

//TODO not all of these log levels are used. So far we only used Debug and Info.
#[derive(Copy, Clone, PartialEq, Eq, PartialOrd, Ord, Debug, clap::ValueEnum, Deserialize)]
pub enum LogLevel {
    #[serde(rename = "debug")]
    Debug,
    #[serde(rename = "info")]
    Info,
    #[serde(rename = "warning")]
    Warning,
    #[serde(rename = "error")]
    Error,
    #[serde(rename = "critical")]
    Critial,
    #[serde(rename = "fatal")]
    Fatal,
}

impl Display for LogLevel {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "{}",
            match self {
                LogLevel::Debug => "DEBUG",
                LogLevel::Info => "INFO",
                LogLevel::Warning => "WARNING",
                LogLevel::Error => "ERROR",
                LogLevel::Critial => "CRITICAL",
                LogLevel::Fatal => "FATAL",
            }
        )
    }
}

pub type Sender = tokio::sync::mpsc::UnboundedSender<(LogLevel, String)>;
pub type Receiver = tokio::sync::mpsc::UnboundedReceiver<(LogLevel, String)>;

pub async fn debug<'a>(msg: impl Into<String>, tx: &Sender) {
    // it's OK to unwrap here, since this can only panic if the caller drops their receiver.
    // If that's the case, the caller should already have stopped the process calling this
    tx.send((LogLevel::Debug, msg.into())).unwrap();
}

pub async fn warn<'a>(msg: impl Into<String>, tx: &Sender) {
    // it's OK to unwrap here, since this can only panic if the caller drops their receiver.
    // If that's the case, the caller should already have stopped the process calling this
    tx.send((LogLevel::Warning, msg.into())).unwrap();
}

pub async fn progress<'a>(msg: impl Into<String>, verbose: bool, tx: &Sender) {
    let msg = msg.into();

    if verbose {
        println!("{}", &msg); //TODO use `tracing` instead of printing straight to stdio? It's supposed tobe printed co console rather than captured, but is the difference really critical?
    }
    debug(&msg, tx).await;
}
