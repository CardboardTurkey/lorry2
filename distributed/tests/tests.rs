use std::{
    collections::BTreeMap,
    io::Write,
    path::{Path, PathBuf},
    process::{Command, Stdio},
    sync::atomic::{AtomicU16, Ordering},
    time::Duration,
};

use axum::serve;
use distributed::{web::routes::list_lorries::LorryResult, web::ControllerSettings};
use tempfile::{Builder, TempDir};
use tokio::net::TcpListener;
use tokio::{select, time::sleep};
use workerlib::LorrySpec;

fn prep_temp_folder(name: &str) -> TempDir {
    Builder::new()
        .prefix(&format!("test-{name}-"))
        .tempdir_in(test_base_dir())
        .expect("Unable to create temporary directory for test")
}

fn test_base_dir() -> PathBuf {
    let mut base = PathBuf::from(env!("CARGO_TARGET_TMPDIR"));
    base.push("tests");
    base.push("networked");
    std::fs::create_dir_all(&base).expect("Unable to create test base directory");
    base
}

fn run_cmd<P: AsRef<Path>>(local: P, cmd: &str, args: &[&str]) -> bool {
    let mut child_process = Command::new(cmd);

    child_process
        .args(args)
        .env("GIT_CEILING_DIRECTORIES", local.as_ref().parent().unwrap());

    tracing::debug!("Running: {cmd} {:?}", args);
    let child_output = child_process
        .current_dir(local)
        .stdin(Stdio::null())
        .output()
        .expect("Unable to run subcommand");
    //if !child_output.status.success() {
    tracing::debug!("Status was: {:?}", child_output.status.code());
    tracing::debug!("Stdout was:\n{:?}", String::from_utf8(child_output.stdout));
    tracing::debug!("Stderr was:\n{:?}", String::from_utf8(child_output.stderr));
    //}
    child_output.status.success()
}

fn get_all_relative_paths<P: AsRef<Path>>(p: P, top_level: bool) -> Vec<String> {
    let p = p.as_ref();
    let folder_name = p.file_name().unwrap().to_os_string().into_string().unwrap();

    let files = p
        .read_dir()
        .unwrap()
        .map(|r| r.unwrap().path())
        .filter(|f| f.is_file())
        .map(|n| n.file_name().unwrap().to_os_string().into_string().unwrap());
    //.collect::<Vec<_>>();

    let dirs = p
        .read_dir()
        .unwrap()
        .map(|r| r.unwrap().path())
        .filter(|f| f.is_dir());

    let f = dirs.flat_map(|d| get_all_relative_paths(d, false));
    //.collect::<Vec<_>>();

    if !top_level {
        f.chain(files)
            .map(|s| {
                //append the folder name to the filenames given

                format!("{}/{}", folder_name, s)
            })
            .collect()
    } else {
        f.chain(files).collect()
    }
}

fn repos_equal(left: &Path, right: &Path) -> Result<(), String> {
    let temp_dir: TempDir = prep_temp_folder("comparisions");
    let clone_repo = |repo: &Path| {
        let cloned = temp_dir
            .path()
            .join(repo.file_name().unwrap().to_str().unwrap());
        std::fs::create_dir_all(&cloned).unwrap();
        run_cmd(&cloned, "git", &["clone", repo.to_str().unwrap(), "."]);
        cloned
    };

    let left = clone_repo(left);
    let right = clone_repo(right);

    //std::thread::sleep(std::time::Duration::from_secs(300));

    let get_file_list = |repo: &Path| {
        let file_list = get_all_relative_paths(repo, true);
        //filter out the .git folder, since we want to compare the final, output files
        let is_git_directory = |name: &str| -> bool { name.starts_with(".git/") };
        let mut file_list = file_list
            .into_iter()
            .filter(|s| !is_git_directory(s))
            .collect::<Vec<_>>();

        file_list.sort();
        file_list
    };

    let left_file_list = get_file_list(&left);
    let right_file_list = get_file_list(&right);

    if left_file_list != right_file_list {
        return Err(format!(
            "File lists not equal: {:?} vs {:?}",
            left_file_list, right_file_list
        ));
    }

    let file_pairs = left_file_list.iter().zip(right_file_list.iter());

    file_pairs
        .map(|(u, d)| (left.join(u), right.join(d)))
        .filter_map(|(upstream_file, downstream_file)| {
            if std::fs::read(&upstream_file)
                .unwrap_or_else(|_| panic!("couldnt read {}", &upstream_file.to_string_lossy()))
                != std::fs::read(&downstream_file).unwrap_or_else(|_| {
                    panic!("couldnt read {}", &downstream_file.to_string_lossy())
                })
            {
                Some(Err::<(), String>(format!(
                    "Files {:?} and {:?} not equal: {:?} vs {:?}",
                    left_file_list,
                    right_file_list,
                    std::fs::read(&upstream_file).unwrap_or_else(|_| panic!(
                        "couldnt read {}",
                        &upstream_file.to_string_lossy()
                    )),
                    std::fs::read(&downstream_file).unwrap_or_else(|_| panic!(
                        "couldnt read {}",
                        &downstream_file.to_string_lossy()
                    ))
                )))
            } else {
                None
            }
        })
        .next()
        .unwrap_or(Ok(()))
}

fn create_config(
    temp_dir: &TempDir,
    lorry_configs: &Vec<distributed::read_config::Config>,
) -> PathBuf {
    let confgit_dir = temp_dir.path().join("CONFGIT");

    std::fs::create_dir(&confgit_dir).unwrap();
    run_cmd(
        &confgit_dir,
        "git",
        &["init", "-b", workerlib::DEFAULT_BRANCH_NAME],
    );
    let mut f = std::fs::OpenOptions::new()
        .write(true)
        .create(true)
        .open(confgit_dir.join("lorry-controller.conf"))
        .unwrap();

    //TODO a test with multiple configs?
    write!(
        f,
        "{}",
        serde_yaml::to_string::<Vec<distributed::read_config::Config>>(lorry_configs).unwrap()
    )
    .unwrap();
    run_cmd(&confgit_dir, "git", &["add", "*"]);
    run_cmd(
        &confgit_dir,
        "git",
        &["commit", "-a", "-m", "Commited config."],
    );

    confgit_dir
}

async fn wait_for_mirror(
    controller_addr: &url::Url,
    mut client_handle: &mut tokio::task::JoinHandle<()>,
    mut server_handle: &mut tokio::task::JoinHandle<()>,
    job_path: &'static str,
) -> distributed::web::routes::list_lorries::LorryResult {
    let mut url = controller_addr
        .join(distributed::web::routes::list_lorries::PATH)
        .expect("Could not create the full request URL");
    url.set_port(Some(controller_addr.port().unwrap())).unwrap(); //TODO is this line necessary
    let url = url;

    let send_request = move |url: url::Url| async move {
        let client = reqwest::Client::new();

        let my_lorry: Vec<LorryResult> = loop {
            let request = client
                .get(url.clone())
                .header("content-type", "application/json");
            //get the response
            let response = request.send().await.unwrap();
            let r = response.status().is_success();
            assert!(r);
            let lorries = response
                .json::<Vec<distributed::web::routes::list_lorries::LorryResult>>()
                .await
                .unwrap();
            tracing::debug!("lorries returned: {:?}", lorries);
            let my_lorry = lorries
                .into_iter()
                .filter(|r| r.path == distributed::comms::LorryPath::new(job_path.to_string()))
                .collect::<Vec<_>>();
            match my_lorry.len() {
                0 => continue,
                1 => break my_lorry,
                d => panic!("Found more than one lorry - dunno how to handle this: {d:?}"),
            }
        };

        my_lorry.into_iter().next().unwrap()
    };

    let tester_handle = tokio::spawn({
        let initial_timestamp = select! {
            my_lorry = send_request(url.clone()) => my_lorry.last_run,
            _ = sleep(Duration::from_secs(10)) => panic!("Waiting for the controller to recognise the lorry has taken far too long"),
        };

        async move {
            loop {
                let my_lorry = send_request(url.clone()).await;
                if my_lorry.last_run_results.is_finished() && my_lorry.last_run > initial_timestamp
                {
                    return my_lorry;
                }
            }
        }
    });

    match tokio::time::timeout(std::time::Duration::from_secs(60), async move {
        tokio::select!(
            Ok(l) = tester_handle => {l}
            _ = &mut client_handle => panic!("Client shouldn't exit"),
            _ = &mut server_handle => panic!("Server shouldn't exit!"),
        )
    })
    .await
    {
        Ok(l) => {
            tracing::info!("Seems to have passed...");
            l
        }
        Err(_) => panic!("Took too long, assuming the test failed..."),
    }
}

async fn set_up_minion(
    temp_dir: &TempDir,
    controller_addr: &url::Url,
) -> (tokio::task::JoinHandle<()>, PathBuf, PathBuf) {
    let minion_config_dir = temp_dir.path().join("minion_config");

    let minion_working_area = temp_dir.path().join("minion_working_dir");

    let client_handle = tokio::spawn({
        let (tx, mut rx) = tokio::sync::mpsc::unbounded_channel::<(logging::LogLevel, String)>();
        let controller_addr = controller_addr.clone();
        let workd = minion_working_area.clone();
        async move {
            let client_args = distributed::minion::MinionArguments {
                controller_address: controller_addr,
                controller_timeout: std::time::Duration::from_secs(10),
                sleep: std::time::Duration::from_secs(5),
                mirroring_settings: workerlib::Arguments {
                    working_area: workd.clone(),
                    pull_only: false,
                    verbose_logging: false,
                    repack: false,
                    keep_multiple_backups: false,
                    push_options: vec![],
                    check_ssl_certificates: false,
                    transmitter: tx,
                    maximum_redirects: 0,
                    log_level: None,
                },
                username: None,
                private_token: None,
                host_name: None,
                port: None,
            };

            distributed::minion::work(client_args, &mut rx).await;
        }
    });

    (client_handle, minion_config_dir, minion_working_area)
}

fn create_lorry_files(
    temp_dir: &TempDir,
    lorry_files: &Vec<(String, BTreeMap<&str, LorrySpec>)>,
) -> PathBuf {
    let dot_lorry_files_container = temp_dir.path().join("lorry_file_container");

    std::fs::create_dir(&dot_lorry_files_container).unwrap();

    for (file_name, contents) in lorry_files {
        let mut f = std::fs::OpenOptions::new()
            .write(true)
            .create(true)
            .open(&dot_lorry_files_container.join(file_name))
            .unwrap();

        write!(f, "{}", serde_yaml::to_string(&contents).unwrap()).unwrap();
    }
    dot_lorry_files_container
}

fn get_controller_base_address() -> reqwest::Url {
    static PORT: AtomicU16 = AtomicU16::new(8080);
    let this_port = PORT.fetch_add(1, Ordering::AcqRel);

    format!("http://127.0.0.1:{this_port}").parse().unwrap()
}

// Extra thread needed so that server can read confgif in background
#[tokio::test(flavor = "multi_thread", worker_threads = 2)]
async fn one_mirror_one_job() {
    const TEST_LORRY_NAME: &str = "mrorr";
    const TEST_LORRY_PREFIX: &str = "upstream";
    const TEST_LORRY_PATH: &str = "upstream/mrorr";

    let controller_addr = get_controller_base_address();
    let temp_dir: TempDir = prep_temp_folder("1m1j");

    //Set up the controller area
    let controller_dir = temp_dir.path().join("controller");
    std::fs::create_dir(&controller_dir).unwrap();
    let db_url = format!("sqlite:{}/lorries.db", controller_dir.to_str().unwrap());

    //set up the remote upstream
    let upstream = temp_dir.path().join("upstream");
    {
        std::fs::create_dir(&upstream).unwrap();
        run_cmd(
            &upstream,
            "git",
            &["init", "-b", workerlib::DEFAULT_BRANCH_NAME],
        );
        let mut f = std::fs::OpenOptions::new()
            .write(true)
            .create(true)
            .open(upstream.join("tracked_file"))
            .unwrap();

        write!(f, "tracked data is here and it should be mirrored").unwrap();

        run_cmd(&upstream, "git", &["add", "*"]);
        run_cmd(&upstream, "git", &["commit", "-a", "-m", "Commited file."]);
    }

    // Set up the lorry-pointer upstream
    let dot_lorry_files_container = create_lorry_files(
        &temp_dir,
        &vec![(format!("{}.lorry", TEST_LORRY_NAME), {
            let lorry_contents = workerlib::LorrySpec::Git(workerlib::lorry_specs::SingleLorry {
                url: upstream.to_str().unwrap().to_string(),
                check_ssl_certificates: false,
                refspecs: None,
            });

            let mut b = std::collections::BTreeMap::new();
            b.insert(TEST_LORRY_NAME, lorry_contents);
            b
        })],
    );

    //Set up the CONFGIT repo
    let confgit_dir = create_config(
        &temp_dir,
        &vec![distributed::read_config::Config::Lorries(
            distributed::read_config::LorryConfig {
                interval: distributed::comms::Interval::new(30),
                prefix: TEST_LORRY_PREFIX.to_string(),
                globs: vec![dot_lorry_files_container
                    .join("*.lorry")
                    .to_str()
                    .unwrap()
                    .to_string()],
                timeout: distributed::comms::Interval::new(60),
            },
        )],
    );

    //set up the "remote" downstream
    let downstream = temp_dir.path().join("downstream");
    std::fs::create_dir_all(&downstream).unwrap();

    let controller_config_dir = controller_dir.join("config");
    let app_settings = ControllerSettings {
        downstream: async_convert::TryInto::try_into(
            distributed::downstream::DownstreamSetup::Local {
                base_dir: downstream.clone(),
            },
        )
        .await
        .unwrap(),
        configuration_directory: controller_config_dir,
        confgit_url: confgit_dir.to_str().unwrap().to_string(),
        confgit_branch: workerlib::DEFAULT_BRANCH_NAME.to_string(),
        confgit_update_period: Duration::from_secs(120),
        remove_ghost_period: Duration::from_secs(120),
        log_level: None,
    };

    let controller = distributed::web::app(&db_url, app_settings).await;
    let app = controller.router.into_make_service();
    let _guard = tracing::subscriber::set_default(controller.logger);
    let listener = TcpListener::bind(&controller_addr.socket_addrs(|| None).unwrap()[0])
        .await
        .unwrap();
    let server = serve(listener, app);
    let mut server_handle = tokio::spawn(async move {
        server.await.expect("Failed to run server..");
    });

    let (mut client_handle, _minion_config_dir, _minion_working_area) =
        set_up_minion(&temp_dir, &controller_addr).await;

    //Tests if the job has completed
    assert!(wait_for_mirror(
        &controller_addr,
        &mut client_handle,
        &mut server_handle,
        TEST_LORRY_PATH,
    )
    .await
    .last_run_results
    .is_success());

    //then, check that the mirror is in fact correct
    repos_equal(&downstream.join(TEST_LORRY_PATH), &upstream)
        .expect("Upstream not equal to downstream!");
    tracing::info!("Done!");
}

// Extra thread needed so that server can read confgif in background
#[tokio::test(flavor = "multi_thread", worker_threads = 2)]
async fn purge() {
    const TEST_LORRY_NAME: &str = "mrorr";
    const TEST_LORRY_PREFIX: &str = "prefix";
    const TEST_LORRY_PATH: &str = "prefix/mrorr";

    let controller_addr = get_controller_base_address();
    let temp_dir: TempDir = prep_temp_folder("purge");

    //Set up the controller area
    let controller_dir = temp_dir.path().join("controller");
    std::fs::create_dir(&controller_dir).unwrap();
    let db_url = format!("sqlite:{}/lorries.db", controller_dir.to_str().unwrap());

    //set up the remote upstream
    let upstream = temp_dir.path().join("upstream");
    {
        std::fs::create_dir(&upstream).unwrap();
        run_cmd(
            &upstream,
            "git",
            &["init", "-b", workerlib::DEFAULT_BRANCH_NAME],
        );
        let mut f = std::fs::OpenOptions::new()
            .write(true)
            .create(true)
            .open(upstream.join("tracked_file"))
            .unwrap();

        write!(f, "tracked data is here and it should be mirrored").unwrap();

        run_cmd(&upstream, "git", &["add", "*"]);
        run_cmd(&upstream, "git", &["commit", "-a", "-m", "Commited file."]);
    }

    // Set up the lorry-pointer upstream
    let dot_lorry_files_container = create_lorry_files(
        &temp_dir,
        &vec![(format!("{}.lorry", TEST_LORRY_NAME), {
            let lorry_contents = workerlib::LorrySpec::Git(workerlib::lorry_specs::SingleLorry {
                url: upstream.to_str().unwrap().to_string(),
                check_ssl_certificates: false,
                refspecs: None,
            });

            let mut b = std::collections::BTreeMap::new();
            b.insert(TEST_LORRY_NAME, lorry_contents);
            b
        })],
    );

    //Set up the CONFGIT repo
    let confgit_dir = create_config(
        &temp_dir,
        &vec![distributed::read_config::Config::Lorries(
            distributed::read_config::LorryConfig {
                interval: distributed::comms::Interval::new(10),
                prefix: TEST_LORRY_PREFIX.to_string(),
                globs: vec![dot_lorry_files_container
                    .join("*.lorry")
                    .to_str()
                    .unwrap()
                    .to_string()],
                timeout: distributed::comms::Interval::new(60),
            },
        )],
    );

    //set up the "remote" downstream
    let downstream = temp_dir.path().join("downstream");
    std::fs::create_dir_all(&downstream).unwrap();

    let controller_config_dir = controller_dir.join("config");
    let app_settings = ControllerSettings {
        downstream: async_convert::TryInto::try_into(
            distributed::downstream::DownstreamSetup::Local {
                base_dir: downstream.clone(),
            },
        )
        .await
        .unwrap(),
        configuration_directory: controller_config_dir,
        confgit_url: confgit_dir.to_str().unwrap().to_string(),
        confgit_branch: workerlib::DEFAULT_BRANCH_NAME.to_string(),
        confgit_update_period: Duration::from_secs(120),
        remove_ghost_period: Duration::from_secs(120),
        log_level: None,
    };

    let controller = distributed::web::app(&db_url, app_settings).await;
    let _guard = tracing::subscriber::set_default(controller.logger);

    let app = controller.router.into_make_service();
    let listener = TcpListener::bind(&controller_addr.socket_addrs(|| None).unwrap()[0])
        .await
        .unwrap();
    let server = serve(listener, app);
    let mut server_handle = tokio::spawn(async move {
        server.await.expect("Failed to run server..");
    });

    let (mut client_handle, _minion_config_dir, minion_working_area) =
        set_up_minion(&temp_dir, &controller_addr).await;

    //Tests if the job has completed; do this multiple times to allow the internal repos to populate
    for iter in 0..4 {
        tracing::info!("Makeing a good mirror");

        assert!(wait_for_mirror(
            &controller_addr,
            &mut client_handle,
            &mut server_handle,
            TEST_LORRY_PATH,
        )
        .await
        .last_run_results
        .is_success());
        //then, check that the mirror is in fact correct
        repos_equal(&downstream.join(TEST_LORRY_PATH), &upstream)
            .expect("Upstream not equal to downstream!");

        if iter >= 1 {
            //both internals should exist now; check that they have not been purged by the minion on suspicion of corruption
            //ok wow that's a comment that sounds like something else entirely
            assert!(minion_working_area
                .join(TEST_LORRY_PATH.replace('/', "_"))
                .join("git-a")
                .exists());
            assert!(minion_working_area
                .join(TEST_LORRY_PATH.replace('/', "_"))
                .join("git-b")
                .exists());
        }
    }
    tracing::info!("First mirrors succeeded!");

    //corrupt the internal repos

    let file_list = get_all_relative_paths(&minion_working_area, true);
    tracing::debug!("file list: {:?}", &file_list);
    let file_list = file_list
        .into_iter()
        .map(|s| minion_working_area.join(s))
        .filter(|f| f.is_file());
    file_list.for_each(|f| {
        tracing::debug!("Writing to {}", &f.display());

        let perms = f.as_path().metadata().unwrap().permissions();
        let readonly = perms.readonly();
        if readonly {
            tracing::debug!("File is readonly..");
            std::fs::set_permissions(f.as_path(), {
                let mut t = perms;
                t.set_readonly(false);
                t
            })
            .unwrap();
        }

        std::fs::OpenOptions::new()
            .write(true)
            .open(f)
            .unwrap()
            .write_all("CORRUPTED FILE".as_bytes())
            .unwrap();
    });

    //Pull a few times to corrupt the repos
    for _ in 0..4 {
        tracing::debug!("Making a corrupt mirror");
        let mirror_results = wait_for_mirror(
            &controller_addr,
            &mut client_handle,
            &mut server_handle,
            TEST_LORRY_PATH,
        )
        .await;
        tracing::debug!("output: {}", mirror_results.last_run_output.unwrap());
        assert!(!mirror_results.last_run_results.is_success());
        //then, check that the mirror is in fact correct
        repos_equal(&downstream.join(TEST_LORRY_PATH), &upstream).expect(
            "Downstream does not mirror upstream; lorry is failing to do it's basic function!",
        );

        repos_equal(
            &upstream,
            &minion_working_area
                .join(TEST_LORRY_PATH.replace('/', "_"))
                .join("git-a"),
        )
        .expect_err("Working area was corrected?");

        repos_equal(
            &upstream,
            &minion_working_area
                .join(TEST_LORRY_PATH.replace('/', "_"))
                .join("git-b"),
        )
        .expect_err("Working area was corrected?");

        repos_equal(
            &downstream.join(TEST_LORRY_PATH),
            &minion_working_area
                .join(TEST_LORRY_PATH.replace('/', "_"))
                .join("git-a"),
        )
        .expect_err("Downstream got corrupted!");

        repos_equal(
            &downstream.join(TEST_LORRY_PATH),
            &minion_working_area
                .join(TEST_LORRY_PATH.replace('/', "_"))
                .join("git-b"),
        )
        .expect_err("Downstream got corrupted!");
    }

    //notify the deletion access point that we would like to  get rid of this repo so we can repopulate it
    let mut url = controller_addr
        .join(distributed::web::routes::purge_repo::PATH)
        .expect("Could not create the full request URL");
    url.set_port(Some(controller_addr.port().unwrap())).unwrap(); //TODO is this line necessary
    let url = url;

    let client = reqwest::Client::new();
    let request = client
        .post(url)
        .header("content-type", "application/json")
        .json(TEST_LORRY_PATH);

    //get the response
    let response = request.send().await.unwrap();
    let r = response.status().is_success();
    assert!(r);
    //purge should be successfully initiated

    //The purge should be carried out during this mirror, cleaning up the mirroing process and allowing the process to run again
    let mirror_results = wait_for_mirror(
        &controller_addr,
        &mut client_handle,
        &mut server_handle,
        TEST_LORRY_PATH,
    )
    .await;
    //tracing::debug!("mirror_results:{:?}", mirror_results);
    assert!(mirror_results.last_run_results.is_success());

    repos_equal(&downstream.join(TEST_LORRY_PATH), &upstream)
        .expect("Downstream does not mirror upstream; lorry is failing to do it's basic function!");

    repos_equal(
        &upstream,
        &minion_working_area
            .join(TEST_LORRY_PATH.replace('/', "_"))
            .join("git-a"),
    )
    .expect("Working area was not purged?");

    //Now we have ascertained git-a was rebuilt correctly, we are almost sure the purge was carried out. But first, check thatgit-b was deleted...
    assert!(!minion_working_area
        .join(TEST_LORRY_PATH.replace('/', "_"))
        .join("git-b")
        .exists());

    //hurray the purge was carried out successfully, and we recovered from corrupted internals sucessfully
}

#[tokio::test]
async fn remove_ghost_jobs() {
    const TEST_LORRY_NAME: &str = "mrorr";
    const TEST_LORRY_PREFIX: &str = "upstream";
    const TEST_LORRY_PATH: &str = "prefix/mrorr";

    let controller_addr = get_controller_base_address();
    let temp_dir: TempDir = prep_temp_folder("purge");

    //Set up the controller area
    let controller_dir = temp_dir.path().join("controller");
    std::fs::create_dir(&controller_dir).unwrap();
    let db_url = format!("sqlite:{}/lorries.db", controller_dir.to_str().unwrap());

    //set up the remote upstream
    let upstream = temp_dir.path().join("upstream");
    {
        std::fs::create_dir(&upstream).unwrap();
        run_cmd(
            &upstream,
            "git",
            &["init", "-b", workerlib::DEFAULT_BRANCH_NAME],
        );
        let mut f = std::fs::OpenOptions::new()
            .write(true)
            .create(true)
            .open(upstream.join("tracked_file"))
            .unwrap();

        write!(f, "tracked data is here and it should be mirrored").unwrap();

        run_cmd(&upstream, "git", &["add", "*"]);
        run_cmd(&upstream, "git", &["commit", "-a", "-m", "Commited file."]);
    }

    // Set up the lorry-pointer upstream
    let dot_lorry_files_container = create_lorry_files(
        &temp_dir,
        &vec![(format!("{}.lorry", TEST_LORRY_NAME), {
            let lorry_contents = workerlib::LorrySpec::Git(workerlib::lorry_specs::SingleLorry {
                url: upstream.to_str().unwrap().to_string(),
                check_ssl_certificates: false,
                refspecs: None,
            });

            let mut b = std::collections::BTreeMap::new();
            b.insert(TEST_LORRY_PATH, lorry_contents);
            b
        })],
    );

    const MIRRORING_INTERVAL: i64 = 5;

    //Set up the CONFGIT repo
    let confgit_dir = create_config(
        &temp_dir,
        &vec![distributed::read_config::Config::Lorries(
            distributed::read_config::LorryConfig {
                interval: distributed::comms::Interval::new(MIRRORING_INTERVAL),
                prefix: TEST_LORRY_PREFIX.to_string(),
                globs: vec![dot_lorry_files_container
                    .join("*.lorry")
                    .to_str()
                    .unwrap()
                    .to_string()],
                timeout: distributed::comms::Interval::new(60),
            },
        )],
    );

    //set up the "remote" downstream - this is irrelevant but required for config. If the minion were real, it would mirror to here
    let downstream = temp_dir.path().join("downstream");
    std::fs::create_dir_all(&downstream).unwrap();

    //Set up the server
    let controller_config_dir = controller_dir.join("config");
    let app_settings = ControllerSettings {
        configuration_directory: controller_config_dir,
        confgit_url: confgit_dir.to_str().unwrap().to_string(),
        confgit_branch: workerlib::DEFAULT_BRANCH_NAME.to_string(),
        confgit_update_period: Duration::from_secs(120),
        remove_ghost_period: Duration::from_secs(5),
        downstream: distributed::downstream::Downstream::Local(
            distributed::hosts::local::LocalDownstream {
                base_dir: downstream,
            },
        ),
        log_level: None,
    };

    let controller = distributed::web::app(&db_url, app_settings).await;
    let app = controller.router.into_make_service();
    let _guard = tracing::subscriber::set_default(controller.logger);

    let listener = TcpListener::bind(&controller_addr.socket_addrs(|| None).unwrap()[0])
        .await
        .unwrap();
    let server = serve(listener, app);
    let mut server_handle = tokio::spawn(async move {
        server.await.expect("Failed to run server..");
    });

    //give the server time to update its config

    match tokio::time::timeout(std::time::Duration::from_secs(5), async {
        tokio::select!(
            _ = &mut server_handle => panic!("Server shouldn't exit!"),
        )
    })
    .await
    {
        Ok(_) => panic!("This shouldn't happen"),
        Err(_) => tracing::debug!("The server should have updated by now"),
    }

    //now we pretend to be a minion, and ask the controller for a job. We have no intention of working on it though...
    let mut get_job_url = controller_addr
        .join(<distributed::comms::JobRequest as distributed::comms::Communication>::PATH)
        .expect("Could not create the full request URL");
    get_job_url
        .set_port(Some(controller_addr.port().unwrap()))
        .unwrap(); //TODO is this line necessary
    let get_job_url = get_job_url;

    let client = reqwest::Client::new();
    let request = client
        .post(get_job_url.clone())
        .header("content-type", "application/json")
        .json(&distributed::comms::JobRequest {
            host: gethostname::gethostname()
                .to_str()
                .expect("My HostName is not a valid string.")
                .to_string(),
            pid: std::process::id(),
        });

    let response = request.send().await.unwrap();
    assert!(response.status().is_success());
    let assigned_job = response
        .json::<Option<distributed::comms::Job>>()
        .await
        .expect("This request should not fail!")
        .expect("Should get a job!");

    // wait some time, let the server run, for say,3 MIRRORING_INTERVALs. If we miss 3 mirroring cycles we certainly should be considered a ghost job!
    // Add 5 to account for ghost removal period.
    match tokio::time::timeout(
        std::time::Duration::from_secs(MIRRORING_INTERVAL as u64 * 3 + 5),
        async {
            tokio::select!(
                _ = &mut server_handle => panic!("Server shouldn't exit!"),
            )
        },
    )
    .await
    {
        Ok(_) => panic!("This shouldn't happen"),
        Err(_) => tracing::debug!("Time has passed..."),
    }

    // call list-jobs to check that the jobs is no longer (because lorry's "remove ghost jobs" task caught it)
    let list_jobs_url = controller_addr
        .join(distributed::web::routes::list_jobs::PATH)
        .unwrap();
    let request = client
        .get(list_jobs_url)
        .header("content-type", "application/json");

    let response = request.send().await.unwrap();

    //We treat the error type as a string, because we don't want to parse it anyway, and it would require implementing Deserialize on it.
    let body = response
        .json::<Vec<distributed::web::routes::list_jobs::JobResult>>()
        .await
        .expect("should not fail to parse!");

    assert!(body.len() == 1);

    assert!(body
        .iter()
        .any(|j| j.id == assigned_job.id && j.exit_status.is_finished())); //TODO and exit code is 127?

    // call get-jobs one last time to verify the job is now made available

    //Wait for mirroring interval to pass...
    match tokio::time::timeout(
        std::time::Duration::from_secs(MIRRORING_INTERVAL as u64),
        async {
            tokio::select!(
                _ = &mut server_handle => panic!("Server shouldn't exit!"),
            )
        },
    )
    .await
    {
        Ok(_) => panic!("This shouldn't happen"),
        Err(_) => tracing::debug!("Time has passed..."),
    }

    let request = client
        .post(get_job_url.clone())
        .header("content-type", "application/json")
        .json(&distributed::comms::JobRequest {
            host: gethostname::gethostname()
                .to_str()
                .expect("My HostName is not a valid string.")
                .to_string(),
            pid: std::process::id(),
        });

    let response = request.send().await.unwrap();
    assert!(response.status().is_success());
    assert!(response
        .json::<Option<distributed::comms::Job>>()
        .await
        .expect("This request should not fail!")
        .is_some());
}

#[tokio::test]
async fn error_logging() {
    const TEST_LORRY_NAME: &str = "mrorr";
    const TEST_LORRY_PREFIX: &str = "upstream";
    const TEST_LORRY_PATH: &str = "prefix/mrorr";

    let controller_addr = get_controller_base_address();
    let temp_dir: TempDir = prep_temp_folder("error_logging");

    //Set up the controller area
    let controller_dir = temp_dir.path().join("controller");
    std::fs::create_dir(&controller_dir).unwrap();
    let db_url = format!("sqlite:{}/lorries.db", controller_dir.to_str().unwrap());

    //set up the remote upstream
    let upstream = temp_dir.path().join("upstream");
    {
        std::fs::create_dir(&upstream).unwrap();
        run_cmd(
            &upstream,
            "git",
            &["init", "-b", workerlib::DEFAULT_BRANCH_NAME],
        );
        let mut f = std::fs::OpenOptions::new()
            .write(true)
            .create(true)
            .open(upstream.join("tracked_file"))
            .unwrap();

        write!(f, "tracked data is here and it should be mirrored").unwrap();

        run_cmd(&upstream, "git", &["add", "*"]);
        run_cmd(&upstream, "git", &["commit", "-a", "-m", "Commited file."]);
    }

    // Set up the lorry-pointer upstream
    let dot_lorry_files_container = create_lorry_files(
        &temp_dir,
        &vec![(format!("{}.lorry", TEST_LORRY_NAME), {
            let lorry_contents = workerlib::LorrySpec::Git(workerlib::lorry_specs::SingleLorry {
                url: upstream.to_str().unwrap().to_string(),
                check_ssl_certificates: false,
                refspecs: None,
            });

            let mut b = std::collections::BTreeMap::new();
            b.insert(TEST_LORRY_PATH, lorry_contents);
            b
        })],
    );

    const MIRRORING_INTERVAL: i64 = 5;

    //Set up the CONFGIT repo
    let confgit_dir = create_config(
        &temp_dir,
        &vec![distributed::read_config::Config::Lorries(
            distributed::read_config::LorryConfig {
                interval: distributed::comms::Interval::new(MIRRORING_INTERVAL),
                prefix: TEST_LORRY_PREFIX.to_string(),
                globs: vec![dot_lorry_files_container
                    .join("*.lorry")
                    .to_str()
                    .unwrap()
                    .to_string()],
                timeout: distributed::comms::Interval::new(60),
            },
        )],
    );

    //set up the "remote" downstream - this is isrrelevant but required for config. If the minion were real, it would mirror to here
    let downstream = temp_dir.path().join("downstream");
    std::fs::create_dir_all(&downstream).unwrap();

    //Set up the server
    let controller_config_dir = controller_dir.join("config");
    let app_settings = ControllerSettings {
        configuration_directory: controller_config_dir,
        confgit_url: confgit_dir.to_str().unwrap().to_string(),
        confgit_branch: workerlib::DEFAULT_BRANCH_NAME.to_string(),
        confgit_update_period: Duration::from_secs(120),
        remove_ghost_period: Duration::from_secs(120),
        downstream: distributed::downstream::Downstream::Local(
            distributed::hosts::local::LocalDownstream {
                base_dir: downstream,
            },
        ),
        log_level: None,
    };

    let controller = distributed::web::app(&db_url, app_settings).await;
    let app = controller.router.into_make_service();
    let _guard = tracing::subscriber::set_default(controller.logger);

    let listener = TcpListener::bind(&controller_addr.socket_addrs(|| None).unwrap()[0])
        .await
        .unwrap();
    let server = serve(listener, app);
    let mut server_handle = tokio::spawn(async move {
        server.await.expect("Failed to run server..");
    });

    //give the server time to update its config

    match tokio::time::timeout(std::time::Duration::from_secs(5), async {
        tokio::select!(
            _ = &mut server_handle => panic!("Server shouldn't exit!"),
        )
    })
    .await
    {
        Ok(_) => panic!("This shouldn't happen"),
        Err(_) => tracing::debug!("The server should have updated by now"),
    }

    //now we pretend to be a minion, and ask the controller for a job. We have no intention of working on it though...
    let mut get_job_url = controller_addr
        .join(<distributed::comms::JobRequest as distributed::comms::Communication>::PATH)
        .expect("Could not create the full request URL");
    get_job_url
        .set_port(Some(controller_addr.port().unwrap()))
        .unwrap();
    let get_job_url = get_job_url;

    let client = reqwest::Client::new();
    let request = client
        .post(get_job_url.clone())
        .header("content-type", "application/json")
        .json(&distributed::comms::JobRequest {
            host: gethostname::gethostname()
                .to_str()
                .expect("My HostName is not a valid string.")
                .to_string(),
            pid: std::process::id(),
        });

    let response = request.send().await.unwrap();
    assert!(response.status().is_success());
    let assigned_job = response
        .json::<Option<distributed::comms::Job>>()
        .await
        .expect("This request should not fail!")
        .expect("Should get a job!");

    //Now we respond with a request of a job update that says an error occurred.
    //The controller should log that as an error.

    let mut update_job_url = controller_addr
        .join(<distributed::comms::StatusUpdate as distributed::comms::Communication>::PATH)
        .expect("Could not create the full request URL");
    update_job_url
        .set_port(Some(controller_addr.port().unwrap()))
        .unwrap();
    let update_job_url = update_job_url;
    const ERR_MSG: &str = "Oh no an error occurred!";
    let request = client
        .post(update_job_url.clone())
        .header("content-type", "application/json")
        .json(&distributed::comms::StatusUpdate {
            job_id: assigned_job.id,
            exit_status: distributed::comms::JobExitStatus::Finished(
                distributed::comms::JobFinishedResults {
                    exit_code: 127,
                    disk_usage: distributed::comms::DiskUsage::new(3),
                },
            ),
            stdout: ERR_MSG.to_string(),
        });

    let response = request.send().await.unwrap();
    assert!(response.status().is_success()); // Remember; the controller won't send errors to minions.
                                             //Now check that the  error has been logged to the DB

    //wait for it to propagate first; I've run into issues with the DB not getting populated yet
    match tokio::time::timeout(std::time::Duration::from_secs(5), async {
        tokio::select!(
            _ = &mut server_handle => panic!("Server shouldn't exit!"),
        )
    })
    .await
    {
        Ok(_) => panic!("This shouldn't happen"),
        Err(_) => tracing::debug!("The server should have updated by now"),
    }

    let mut get_errors_url = controller_addr
        .join(distributed::web::routes::report_errors::PATH)
        .expect("Could not create the full request URL");

    get_errors_url
        .set_port(Some(controller_addr.port().unwrap()))
        .unwrap();
    let get_errors_url = get_errors_url;

    let request = client
        .get(get_errors_url.clone())
        .header("content-type", "application/json");

    let response = request.send().await.unwrap();
    assert!(response.status().is_success());

    let errors = response
        .json::<Vec<distributed::web::routes::report_errors::ErrorLog>>()
        .await
        .expect("Should not fail to deserialize");
    dbg!(&assigned_job);
    dbg!(&errors);

    eprintln!("{}", assigned_job.path.clone());
    //Check that the error we generated appears in the log
    assert!(errors.iter().any(|e| {
        eprintln!("{}", &e.for_lorry.clone().unwrap());
        e.for_lorry == Some(assigned_job.path.clone())
            && e.endpoint
                == <distributed::comms::StatusUpdate as distributed::comms::Communication>::PATH
    }));
}

//TODO tests to make:
// Test multiple .lorry files being pointed to in a single Config object
// Test multiple Config objects in one vec
// Test the disk usage logging - I noticed some bugs with it, not 100% that my path fixes also fixed *it*
