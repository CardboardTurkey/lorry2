-- Drop the "stupid" table, it seems to exist purely as an implementation detail of old-lorry
-- I think the idea was that you would write to it while you held open a DB connection
-- It doesn't seem to be relevant though since the new system seems to read and write perfectly fine

DROP TABLE stupid;

-- The max_jobs table is a bit strange, I don't see a scenario in which one would want to 
-- artificially throttle the number of jobs you can have running - just restrict the number of
-- minion instances! I suppose this could povide some protection against malicious requests
-- but that should be handled by your network setup, and the ghost job cleaning.

DROP TABLE max_jobs;