-- Assert that several fields in the DB will always be initialized. 
-- This is mostly for the convenience sqlx, which would like to know what type columns are,
-- And for the programmer so they can have a more concrete idea of what it is they're fetching from the DB.
CREATE TABLE temp AS
SELECT *
FROM lorries;

DROP TABLE lorries;

CREATE TABLE lorries(
    path TEXT PRIMARY KEY NOT NULL, 
    text TEXT NOT NULL, 
    from_trovehost TEXT NOT NULL, 
    from_path TEXT NOT NULL, 
    running_job INT, 
    last_run INT NOT NULL, 
    interval INT NOT NULL, 
    lorry_timeout INT NOT NULL, 
    disk_usage INT, 
    last_run_exit INT, 
    last_run_error TEXT
);

INSERT INTO lorries (path, text, from_trovehost, from_path, running_job, last_run, interval, lorry_timeout, disk_usage, last_run_exit, last_run_error)
SELECT path, text, from_trovehost, from_path, running_job, last_run, interval, lorry_timeout, disk_usage, last_run_exit, last_run_error FROM temp;

DROP TABLE temp;