use std::{path::PathBuf, sync::Arc, sync::Mutex, time::Duration};

use axum::{
    routing::{get, post},
    Router,
};

use prometheus_client::{metrics::gauge::Gauge, registry::Registry};

use tokio::{select, spawn};
use tracing::{debug, error, info, Level};

use sqlx::migrate::MigrateDatabase;
use sqlx::Sqlite;

pub mod routes;

use crate::comms::Communication;
use crate::downstream::Downstream;
use crate::state_db::StateDatabase;
use crate::{
    comms, create_logger, give_job, read_config, remove_ghost_jobs, update_lorry_status,
    ControllerComponents, ControllerState,
};

/// Settings for the creating the lorry controller server.
#[derive(Clone, Debug)]
pub struct ControllerSettings {
    /// Representation of some connection to the host of the downstream git mirrors. This could be git forge instance, a local filesystem, or something else.
    /// This is responsible for creating the downstream git mirrors for worker instances to mirror to.
    pub downstream: Downstream, //TODO as it stands, we open a session (say, of a gitlab connection) and hold it while the program runs, rather than only opening and closing when needed. Is this the best practice to follow?

    /// When the server updates its configuration, it does so by mirroring the config repository into this folder.
    pub configuration_directory: PathBuf,

    /// Git URL/path pointing to a git repository containing the server configuration. When instructed to update configuration, the server will
    /// git pull from the git repository located here.
    pub confgit_url: String, //TODO make it a URL type? While I don't think anyone is using a local filesystem config in production, it *is* useful for testing to allow for local repos.

    /// Inspect the branch with this name when pulling in `confgit_url` for configuration files.
    pub confgit_branch: String,

    pub confgit_update_period: Duration,

    pub remove_ghost_period: Duration,

    pub log_level: Option<Level>,
}

//TODO should DB setup be here? perhaps passed in? This breaking up just really exists for testing puposes anyway...
/// Constructs the components of a fully-functioning controller system.
/// * `db_url` - connect to a database at this location as the backing store. Currently, this can only handle SQLite database files.
/// * `app_settings` - a struct containing the parameters of the server. Note this is not a configuration of the mirrors;
/// rather, it is the data necessary to fetch the mirroring configuration later.
pub async fn app(db_url: &str, app_settings: ControllerSettings) -> ControllerComponents {
    if !Sqlite::database_exists(db_url).await.unwrap() {
        Sqlite::create_database(db_url).await.unwrap();
    }

    let pool = StateDatabase::connect(db_url)
        .await
        .expect("Could not connect to the database");

    let pool = Arc::new(pool);

    info!("finished connecting to DB");
    let (writer_handle, tracer_lifetime) = create_logger(pool.clone(), app_settings.log_level);

    let periodics = spawn({
        let pool = pool.clone();
        let app_settings = app_settings.clone();
        async move {
            select! {
                _ = read_config::periodic(pool.clone(), &app_settings) => {
                    debug!("Periodic read of config stopped for some reason, restarting now!");
                }
                _ = remove_ghost_jobs::periodic(pool, &app_settings) => {
                    debug!("Periodic removal of ghost jobs stopped for some reason, restarting now!");
                }
            }
        }
    });
    spawn(async move {
        if periodics.await.is_err() {
            // Thread panicked or was cancelled
            error!("Lorry is no longer auto-reading confgit or auto-removing ghost jobs");
        }
    });

    let total_lorries: Gauge = Gauge::default();
    let total_jobs: Gauge = Gauge::default();

    let mut metrics_registry: Registry = Registry::default();

    metrics_registry.register(
        "lorry2_total_jobs",
        "The total amount of jobs",
        total_jobs.clone(),
    );

    metrics_registry.register(
        "lorry2_total_lorries",
        "The total amount of lorries",
        total_lorries.clone(),
    );

    let state = ControllerState {
        db: pool,
        app_settings,
        metrics_registry: Arc::new(Mutex::new(metrics_registry)),
        total_lorries,
        total_jobs,
    };

    //TODO should the POST endpoints perhaps be pub rather than pub(crate)? That way, their argument list would be made public in the docs
    let router = Router::new()
        .route(
            comms::StatusUpdate::PATH,
            post(update_lorry_status::update_lorry_status),
        )
        .route(comms::JobRequest::PATH, post(give_job::give_job_to_worker))
        .route(
            routes::list_lorries::PATH,
            get(routes::list_lorries::list_all_lorries),
        )
        .route(
            routes::list_jobs::PATH,
            get(routes::list_jobs::list_all_jobs),
        )
        .route(
            routes::purge_repo::PATH,
            post(routes::purge_repo::purge_repo),
        )
        .route(
            routes::report_errors::PATH,
            get(routes::report_errors::report_errors),
        )
        .route(
            routes::health_check::PATH,
            get(routes::health_check::health_check),
        )
        .route(routes::metrics::PATH, get(routes::metrics::metrics))
        .with_state(state);

    ControllerComponents {
        router,
        writer: writer_handle,
        logger: tracer_lifetime,
    }

    //TODO shouldn't I change the 1.0's to 2.0's, given this is, well, Lorry 2? Or even drop them all together?
}
