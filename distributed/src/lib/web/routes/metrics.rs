use axum::{extract::State, http::StatusCode, response::IntoResponse};

use prometheus_client::encoding::text::encode;

use crate::ControllerState;

pub const PATH: &str = "/1.0/metrics";

/// Check that all the components of the controller are working properly
#[tracing::instrument(skip_all)]
pub(crate) async fn metrics(State(state): State<ControllerState>) -> Result<String, MetricsError> {
    let mut body = String::new();

    set_total_jobs(&state).await?;
    set_total_lorries(&state).await?;

    encode(&mut body, &state.metrics_registry.lock().unwrap())?;
    Ok(body)
}

async fn set_total_jobs(state: &ControllerState) -> Result<(), MetricsError> {
    let ln = state.db.get_individual_job_records().await?.len();
    state.total_jobs.set(ln.try_into()?);
    Ok(())
}

async fn set_total_lorries(state: &ControllerState) -> Result<(), MetricsError> {
    let ln = state.db.get_all_lorries_info().await?.len();
    state.total_lorries.set(ln.try_into()?);
    Ok(())
}

#[derive(thiserror::Error, Debug)]
pub(crate) enum MetricsError {
    #[error("Unable to encode metrics: {0}")]
    EncodeFailed(#[from] std::fmt::Error),
    #[error("Error occured while getting a value from database: {0}")]
    DBError(#[from] sqlx::Error),
    #[error("Error converting value to integer: {0}")]
    ConversionFailed(#[from] std::num::TryFromIntError),
}

impl IntoResponse for MetricsError {
    fn into_response(self) -> axum::response::Response {
        tracing::error!(endpoint =%PATH, "Metrics failed: {self}");
        (StatusCode::INTERNAL_SERVER_ERROR, self).into_response()
    }
}
