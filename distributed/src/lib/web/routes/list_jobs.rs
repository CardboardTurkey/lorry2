use axum::{extract::State, http::StatusCode, Json};

use crate::{comms, state_db, ControllerSettings, ControllerState};

/// Path to the endpoint to display all mirroing job's history
pub const PATH: &str = "/1.0/list-jobs";

/// Respond with a list of all the jobs that have ever been run in this controller's history,
#[tracing::instrument(skip_all)]
pub(crate) async fn list_all_jobs(
    State(state): State<ControllerState>,
) -> Result<Json<Vec<JobResult>>, ListAllJobsAsJsonError> {
    //TODO: perhaps pre-sort to put running jobs up top and then sort by date issued?
    get_all_jobs(&state.db, state.app_settings)
        .await
        .map_err(ListAllJobsAsJsonError::DBError)
        .map(Json::from)
}
#[derive(thiserror::Error, Debug)]
pub(crate) enum ListAllJobsAsJsonError {
    #[error("Error occurred during database operation")]
    DBError(#[from] sqlx::Error),
}

impl axum::response::IntoResponse for ListAllJobsAsJsonError {
    fn into_response(self) -> axum::response::Response {
        let (status, err_msg) = match self {
            ListAllJobsAsJsonError::DBError(e) => (
                StatusCode::INTERNAL_SERVER_ERROR,
                format!("Database operation getting the jobs failed: {:?}", e),
            ),
        };

        tracing::error!(endpoint =%PATH, "Error at listing jobs: {}", err_msg);

        (status, err_msg).into_response()
    }
}

// Place holder function that could be used to list jobs with html
#[allow(unused)]
pub(crate) async fn list_all_jobs_pretty(
    State((db, app_settings)): State<(state_db::StateDatabase, ControllerSettings)>,
) {
    todo!()
}

#[derive(serde::Serialize, serde::Deserialize, Debug)]
pub struct JobResult {
    pub id: comms::JobId,
    pub path: String,
    pub exit_status: comms::JobExitStatus,
    pub host: String,
}

#[tracing::instrument(skip_all)]
async fn get_all_jobs(
    db: &state_db::StateDatabase,
    _app_settings: ControllerSettings,
) -> Result<Vec<JobResult>, sqlx::Error> {
    //let now = db.get_current_time().await;
    Ok(db
        .get_individual_job_records()
        .await?
        .into_iter()
        .collect::<Vec<_>>())
}
