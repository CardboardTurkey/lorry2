pub mod health_check;

/// Give a list of all jobs that have been or are running, as well as status information on them.
pub mod list_jobs;

/// Give a list of all lorries, as well as status information, such as the last run results, or the mirroring interval.
pub mod list_lorries;

/// Endpoint to expose Prometheus metrics
pub mod metrics;

/// Tell minions that check in to delete old copies of a mirror, because it is believed that corruption or other unrecoverable error occurred.
pub mod purge_repo;

/// Public endpoint to view errors that have occurred
pub mod report_errors;
