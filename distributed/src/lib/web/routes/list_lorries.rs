use axum::{extract::State, http::StatusCode, Json};

use crate::{comms, state_db, ControllerSettings, ControllerState};

/// Path to the endpoint to list mirror status
pub const PATH: &str = "/1.0/list-lorries";

/// Return a list of all the mirror configurations.
#[tracing::instrument(skip_all)]
pub(crate) async fn list_all_lorries(
    State(state): State<ControllerState>,
) -> Result<Json<Vec<LorryResult>>, ListAllLorriesAsJsonError> {
    get_all_lorries(&state.db, state.app_settings)
        .await
        .map_err(ListAllLorriesAsJsonError::DBError)
        .map(Json::from)
}
#[derive(thiserror::Error, Debug)]
pub(crate) enum ListAllLorriesAsJsonError {
    #[error("Error occurred during database operation")]
    DBError(#[from] sqlx::Error),
}

impl axum::response::IntoResponse for ListAllLorriesAsJsonError {
    fn into_response(self) -> axum::response::Response {
        let (status, err_msg) = match self {
            ListAllLorriesAsJsonError::DBError(e) => (
                StatusCode::INTERNAL_SERVER_ERROR,
                format!("Database operation getting the jobs failed: {:?}", e),
            ),
        };

        //TODO is it worth logging this error to the database?
        //Firstly, it's not critical to operation.
        //Secondly, the only possible error is a DB error, so odds are the write will fail too.
        tracing::error!(endpoint = %PATH, "Error at listing all lorries: {}", err_msg);

        (status, err_msg).into_response()
    }
}

// Place holder function that could be used to list lorries with html
#[allow(unused)]
pub(crate) async fn list_all_lorries_pretty(
    State((db, app_settings)): State<(state_db::StateDatabase, ControllerSettings)>,
) {
    todo!()
}

//TODO why am I copying around a struct nigh-identical to LorryEntry... largely becuase I haven't committed to which fields should or should not be shown so I guess I'll just show all of them!
#[derive(serde::Serialize, serde::Deserialize, Debug)]
pub struct LorryResult {
    pub path: comms::LorryPath,
    pub name: String,
    pub spec: workerlib::LorrySpec,
    pub running_job: Option<i64>,
    pub last_run: comms::TimeStamp,
    pub interval: comms::Interval,
    pub lorry_timeout: comms::Interval,
    pub last_run_results: comms::JobExitStatus,
    pub last_run_output: Option<String>, //TODO this is rather tied to last_run results, try to fold them together. We can't just add a Srting field to `comms::JobExitStatus` because that would mean the minion has to send its log inside the output field; but the logs are *partial* so it's a little counter to the intention of what this type means
    pub purge_before: comms::TimeStamp,
}

#[tracing::instrument(skip_all)]
async fn get_all_lorries(
    db: &state_db::StateDatabase,
    _app_settings: ControllerSettings,
) -> Result<Vec<LorryResult>, sqlx::Error> {
    //let now = db.get_current_time().await;
    Ok(db
        .get_all_lorries_info()
        .await?
        .into_iter()
        .map(|l| LorryResult {
            path: l.path,
            name: l.lorry_name,
            spec: l.lorry_spec,
            running_job: l.running_job,
            last_run: l.last_run,
            interval: l.interval,
            lorry_timeout: l.lorry_timeout,
            last_run_results: l.last_run_results,
            last_run_output: l.last_run_error,
            purge_before: l.purge_before,
        })
        .collect::<Vec<_>>())
}
