use crate::downstream;

use super::comms::Communication;
use super::state_db;
use axum::{extract::State, Json};
use workerlib::lorry_specs::SingleLorry;

use super::comms;
use crate::ControllerState;

/// Minion calls this endpoint when it has no job. If there is a valid job, the controller will send the
/// details about the job over so that the minion will start running the mirror.
#[tracing::instrument(skip_all)]
pub(crate) async fn give_job_to_worker(
    State(state): State<ControllerState>,
    Json(job_request): Json<comms::JobRequest>,
) -> Json<<comms::JobRequest as Communication>::Response> {
    get_job(&state.db, &state.app_settings, &job_request)
        .await
        .map_err(|e| e.trace_error(&job_request))
        .ok()
        .flatten()
        .into()
}

async fn get_job(
    db: &state_db::StateDatabase,
    app_settings: &super::ControllerSettings,
    job_request: &comms::JobRequest,
) -> Result<<comms::JobRequest as Communication>::Response, JobGivingError> {
    if db
        .get_running_queue()
        .await
        .map_err(JobGivingError::DBOperationErrorBeforeFindingALorry)?
    {
        let lorry_infos = db
            .get_all_lorries_info()
            .await
            .map_err(JobGivingError::DBOperationErrorBeforeFindingALorry)?;
        let now = db.get_current_time().await;
        for lorry_row in lorry_infos {
            if ready_to_run(&lorry_row, now) {
                let path = lorry_row.path.clone();
                match issue_job(db, lorry_row, app_settings, job_request, now).await {
                    Ok(j) => return Ok(Some(j)),
                    Err(e) => {
                        tracing::warn!(
                            lorry = %path,
                            endpoint =%<comms::JobRequest as comms::Communication>::PATH,
                            "Failed to issue job {}. Error was: {}",
                            &path,
                            e
                        );
                        // If we failed to issue this job, then we should set it as if it was run now. Otherwise,
                        // it will be legal to issue, and thus we'll likely try and fail to issue it next time
                        // a minion asks for a job, at which point we'll likely keep hitting the same errors
                        db.set_last_run_time(&path,now)
                        .await
                        .map_err(|e|{
                            tracing::warn!(
                                lorry = %path,
                                endpoint =%<comms::JobRequest as comms::Communication>::PATH,
                                "Ran into an error trying to updating the last run time of job {} to prevent it from being constantly issued: {e}",path);
                            JobGivingError::DBOperationErrorDuringIssue(e, path)})?;
                        return Err(e);
                    }
                }
            }
        }
    }

    tracing::info!("No job to give to worker");

    Ok(None)
}

async fn issue_job(
    db: &state_db::StateDatabase,
    lorry_row: state_db::LorryEntry,
    app_settings: &crate::ControllerSettings,
    job_request: &comms::JobRequest,
    now: comms::TimeStamp,
) -> Result<comms::Job, JobGivingError> {
    let metadata = get_repo_metadata(&lorry_row)
        .await
        .map_err(|e| JobGivingError::FailedToGetUpstreamMetadata(e, lorry_row.path.clone()))?;

    let path = lorry_row.path;
    app_settings
        .downstream
        .prepare_repo(&path, metadata)
        .await
        .map_err(|e| JobGivingError::PreparingDownstreamError(e, path.clone()))?;
    let new_job_id = db
        .get_next_job_id()
        .await
        .map_err(|e| JobGivingError::DBOperationErrorDuringIssue(e, path.clone()))?;
    db.set_running_job(&path, Some(new_job_id))
        .await
        .map_err(|e| JobGivingError::DBOperationErrorDuringIssue(e, path.clone()))?;
    db.add_new_job(new_job_id, &job_request.host, job_request.pid, &path, now)
        .await
        .map_err(|e| JobGivingError::DBOperationErrorDuringIssue(e, path.clone()))?;
    tracing::info!(
        "Giving job {} to lorry {} to worker {}:{}",
        new_job_id,
        path,
        &job_request.host,
        job_request.pid
    );
    Ok(comms::Job {
        lorry_name: lorry_row.lorry_name,
        lorry_spec: lorry_row.lorry_spec,
        id: new_job_id,
        path,
        purge_cutoff: lorry_row.purge_before,
        insecure: app_settings.downstream.insecure(),
        mirror_server_base_url: app_settings.downstream.mirror_base_url(),
    })
}

//TODO This as well as a bunch of other functions needs a rename to be more descriptive
async fn get_repo_metadata(
    lorry_row: &state_db::LorryEntry,
) -> Result<downstream::RepoMetadata, Box<dyn std::error::Error + Send>> {
    let (host_name, metadata) = get_single_repo_metadata(&lorry_row.lorry_spec)
        .await
        .map(|(h, m)| (h.unwrap_or("".to_string()), m))?;

    let metadata = if !host_name.is_empty() {
        downstream::RepoMetadata {
            head: metadata.head,
            description: metadata
                .description
                .map(|d| format!("{}: {}", host_name, d)),
        }
    } else {
        metadata
    };

    Ok(metadata)
}

//TODO this fn is a mess of repeadly matching the same thing, clean it up...
//TODO This is a source of a frustrating amount of Option<>s,
async fn get_single_repo_metadata(
    lorry_spec: &workerlib::LorrySpec,
) -> Result<(Option<String>, downstream::RepoMetadata), Box<dyn std::error::Error + Send>> {
    //TODO original asserts that  lorry_spec.from_trovehost was not set yet.

    let url = match lorry_spec {
        workerlib::LorrySpec::Git(SingleLorry {
            url,
            check_ssl_certificates: _,
            refspecs: _,
        }) => Some(url),
        workerlib::LorrySpec::RawFiles(_) => None,
        workerlib::LorrySpec::Tarball(SingleLorry {
            url,
            check_ssl_certificates: _,
            refspecs: _,
        }) => Some(url),
    }; //TODO python code has .strip(): is that necessary or just defensive?

    let (host_name, repo_path) = url
        .and_then(|url| {
            match lorry_spec {
                workerlib::LorrySpec::Git(_) => {
                    //test URL is a valid Git URL. This is of the form:
                    //user@host:path, host:path.  Path must not start with '//' as that indicates a real URL.
                    fancy_regex::Regex::new(r"^(?:[^:@/]+@)?([^:@/]+):(?!//)") //TODO precompile this so it isn't recompiled every request
                        .unwrap()
                        .captures(url).expect("Don't this this is supposed to throw an *error*; fails ot match are Ok(None)")
                    .map(
                       |matches|(
                            matches.get(1).unwrap().as_str().to_string(), //TODO this probably needs some validation
                            matches.get(0).unwrap().as_str().to_string(),
                        ))
                    },
                _ => None,
            }
            .or_else(|| {
                reqwest::Url::parse(url).ok().map(|url_obj| {
                    (
                        url_obj.host().unwrap().to_string(),
                        url_obj.path().trim_matches('/').to_string(),
                    )
                })
            })
        })
        .unzip();

    let metadata = match lorry_spec {
        workerlib::LorrySpec::Git(_) => match url {
            None => downstream::RepoMetadata {
                head: None,
                description: None,
            },
            Some(url) => downstream::RepoMetadata {
                head: utils::command_wrapper::CommandBuilder::new("git")
                    .args(&["ls-remote", "--symref", "--"])
                    .arg(url)
                    .arg("HEAD")
                    .execute(std::env::current_dir().unwrap(), |s| async move {
                        tracing::debug!(s)
                    })
                    .await
                    .ok()
                    .and_then(|(o, _)| {
                        fancy_regex::Regex::new(r"^ref: refs/heads/([^\s]+)\tHEAD\n")
                            .expect("Static regex is maformed!") //TODO precompile these regii rather than recompile every request
                            .captures(&o)
                            .expect("Don't see how this can error on an non-malformed expression")
                            .and_then(|m| m.get(1))
                            .map(|m| m.as_str().to_string())
                    }),
                description: repo_path,
            },
        },

        workerlib::LorrySpec::RawFiles(_) => downstream::RepoMetadata {
            head: Some(workerlib::DEFAULT_BRANCH_NAME.to_string()),
            description: repo_path,
        },
        workerlib::LorrySpec::Tarball(_) => downstream::RepoMetadata {
            head: Some(workerlib::DEFAULT_BRANCH_NAME.to_string()),
            description: repo_path,
        },
    };
    Ok((host_name, metadata))
}

fn ready_to_run(lorry_row: &state_db::LorryEntry, now: super::comms::TimeStamp) -> bool {
    let due = lorry_row.last_run + lorry_row.interval;
    tracing::debug!(
        "Lorry {} time comparision: ({} + {})  {} < {} ",
        lorry_row.path,
        lorry_row.last_run,
        lorry_row.interval,
        due,
        now
    );
    due <= now && lorry_row.running_job.is_none()
}

#[derive(thiserror::Error, Debug)]
pub(crate) enum JobGivingError {
    //TODO include actual DB request that failed?
    #[error("Error occurred during some database operation")]
    DBOperationErrorBeforeFindingALorry(sqlx::Error),

    #[error("Error occurred during some database operation while issuing a job")]
    DBOperationErrorDuringIssue(sqlx::Error, comms::LorryPath),

    #[error("Error occurred when trying to get metadata about the repo from upstream")]
    FailedToGetUpstreamMetadata(Box<dyn std::error::Error + Send>, comms::LorryPath),

    #[error("Failed to prepare downstream repo")]
    PreparingDownstreamError(Box<dyn std::error::Error + Send>, comms::LorryPath),
}

impl JobGivingError {
    fn trace_error(self, request: &comms::JobRequest) {
        let (path, err_msg) = match &self {
            JobGivingError::FailedToGetUpstreamMetadata(e, path) => (
                Some(path),
                format!(
                    "Failed to get metadata from the upstream repo for lorry {}: {:?}",
                    path, e
                ),
            ),
            JobGivingError::PreparingDownstreamError(e, path) => (
                Some(path),
                format!(
                    "Failed to get prepare the downstream repo for lorry {}: {:?}",
                    path, e
                ),
            ),
            JobGivingError::DBOperationErrorBeforeFindingALorry(e) => {
                (None, format!("Internal database error: {:?}", e))
            }

            JobGivingError::DBOperationErrorDuringIssue(e, path) => (
                Some(path),
                format!("Internal database error while issuing {}: {:?}", path, e),
            ),
        };

        tracing::error!(
            lorry = path.map(tracing::field::display),
            endpoint =%<comms::JobRequest as comms::Communication>::PATH,
            "Error at giving job to worker {}:{} : {}",
            request.host,
            request.pid,
            err_msg,
        );
    }
}
