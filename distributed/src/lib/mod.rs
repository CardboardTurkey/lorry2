use std::{collections::BTreeMap, sync::Arc, sync::Mutex};

use axum::Router;

use prometheus_client::{metrics::gauge::Gauge, registry::Registry};
use state_db::StateDatabase;

use tracing::Level;
use tracing_subscriber::{filter::LevelFilter, prelude::*, Layer};

/// Operations for the backing database
mod state_db;

/// Send mirror specifications to minion instances to carry out.
mod give_job;

/// Receive updates on how a job is going, including receiving logs from the minion program denoting mirror status.
mod update_lorry_status;

//TODO make these pub(crate) instead and figure out which types specifically need to be made public
/// Common types used for communications between controller and minion.
pub mod comms;

/// Link to downstream mirror hosts. Used to manage the hosts, for example, creating empty repositories to mirror to.
pub mod downstream;

/// Specific implementations of downstream operations for different downstream hosts.
pub mod hosts;

/// Program that requests work from the controller application and carries out mirroring tasks appropriately.
pub mod minion;

/// Update the controller's configuration by reading from the external configuration repository.
pub mod read_config; //TODO it's only pub so we can test, find a way to make this non-pub again

/// Mark jobs that have been running for too long as not running, to allow them to be scheduled again.
pub mod remove_ghost_jobs;

pub mod web;

use crate::web::routes::report_errors::ErrorLog;
use crate::web::ControllerSettings;

use comms::TimeStamp;

#[derive(Clone)]
/// Global controller state object used across the web application
pub struct ControllerState {
    db: Arc<StateDatabase>,
    app_settings: ControllerSettings,
    metrics_registry: Arc<Mutex<Registry>>,
    total_lorries: Gauge,
    total_jobs: Gauge,
}

/// Concatenation of the major sub-components of the controller.
pub struct ControllerComponents {
    /// The server that listens on endpoints to manage minion instances.
    pub router: Router,
    /// A tracing subscriber that sends errors and warnings to `writer` to be written to the database.
    pub logger: Box<dyn tracing::Subscriber + Send + Sync>,
    /// A task that runs in parallel with `router` that accepts error messages from `logger_lifetime`
    /// and logs them to the database.
    pub writer: tokio::task::JoinHandle<()>,
}

//TODO place this in some common functions module
/// This is used so we can return sqlx::Error s wrapped nicely as a JSON value in our errors.
pub(crate) fn dump_debug<S>(val: impl std::fmt::Debug, s: S) -> Result<S::Ok, S::Error>
where
    S: serde::Serializer,
{
    s.serialize_str(&format!("{:?}", val))
}

//TODO perhaps more of the error logging functionality should be moved to report_errors?
fn create_logger(
    db: Arc<state_db::StateDatabase>,
    level: Option<Level>,
) -> (
    tokio::task::JoinHandle<()>,
    Box<dyn tracing::Subscriber + Send + Sync>,
) {
    let (tx, mut rx) = tokio::sync::mpsc::unbounded_channel::<ErrorLog>();

    let subscriber = tracing_subscriber::registry()
        .with(
            Logger::new(tx).with_filter(tracing_subscriber::filter::filter_fn(|m| {
                m.level() == &tracing::Level::WARN || m.level() == &tracing::Level::ERROR
            })),
        )
        .with(
            tracing_subscriber::fmt::layer()
                .with_span_events(
                    tracing_subscriber::fmt::format::FmtSpan::NEW
                        | tracing_subscriber::fmt::format::FmtSpan::CLOSE,
                )
                .with_file(true)
                .with_line_number(true)
                .with_filter(LevelFilter::from_level(level.unwrap_or(Level::INFO))),
        );

    (
        tokio::task::spawn(async move {
            while let Some(e) = rx.recv().await {
                db.record_error(&e.error_message, &e.endpoint, e.for_lorry, e.timestamp)
                    .await
                    .ok();
            }

            eprintln!("Lorry-controller DB writer task died!")
        }),
        Box::new(subscriber),
    )
}

struct Logger {
    tx: tokio::sync::mpsc::UnboundedSender<ErrorLog>,
}

impl Logger {
    pub fn new(tx: tokio::sync::mpsc::UnboundedSender<ErrorLog>) -> Self {
        Self { tx }
    }
}

struct ConcatVisitor<'a> {
    concatted: BTreeMap<&'a str, String>,
}

impl ConcatVisitor<'_> {
    pub fn new() -> Self {
        ConcatVisitor {
            concatted: BTreeMap::new(),
        }
    }
}

impl tracing::field::Visit for ConcatVisitor<'_> {
    fn record_debug(&mut self, field: &tracing::field::Field, value: &dyn std::fmt::Debug) {
        //if two fields have the same name, this can overwrite the first.
        //however, I don't think tracing events allow that to occur in the first place, so this should be OK
        self.concatted.insert(field.name(), format!("{:?}", value));
    }
}

impl<S> tracing_subscriber::Layer<S> for Logger
where
    S: tracing::Subscriber,
{
    fn on_event(
        &self,
        event: &tracing::Event<'_>,
        _ctx: tracing_subscriber::layer::Context<'_, S>,
    ) {
        let mut concat_visitor = ConcatVisitor::new();
        event.record(&mut concat_visitor);

        //TODO what about other fields? We aren't recording them, if the caller happens to give them...
        //TODO also use CTX to get the span this occurred in
        //TODO turns out we can't actually do that since tracing doesn't store that into; the subscriber has to implement keeping track of that!
        let lorry_we_errored_on = concat_visitor.concatted.remove("lorry");

        let endpoint_we_errored_on = concat_visitor.concatted.remove("endpoint");

        let message = concat_visitor
            .concatted
            .remove("message")
            .expect("tracing log can't not have a message");

        let message = ErrorLog {
            error_message: message,
            endpoint: endpoint_we_errored_on.unwrap_or("".to_string()),
            timestamp: now_in_secs(),
            for_lorry: lorry_we_errored_on.map(comms::LorryPath::new),
        };
        self.tx.send(message).expect("Logger receiver shouldn't be dropped; it's in a loop it can only exit if this transmitter is dropped!")
    }
}

/// Returns the current time in seconds
///
/// Will panic if you've got access to the TARDIS.
fn now_in_secs() -> TimeStamp {
    TimeStamp::new(
        std::time::SystemTime::now()
            .duration_since(std::time::UNIX_EPOCH)
            .expect("This should not be run on pre-1970 times")
            .as_secs()
            .try_into()
            .expect("timestamp too big, consider upping the size of internal timestamps!"),
    )
}
