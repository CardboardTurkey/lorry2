use crate::comms::Communication;

use super::comms;
use super::state_db;
use axum::{extract::State, Json};

/// The minion calls this endpoint to update the controller about logging that has happened so far. The controller responds
/// by updating the minion if the job has been cancelled in the mean time since the last check-in.
#[tracing::instrument(skip_all)]
pub(crate) async fn update_lorry_status(
    State(state): State<super::ControllerState>,
    Json(status_msg): Json<comms::StatusUpdate>,
) -> Json<<comms::StatusUpdate as comms::Communication>::Response> {
    match update_status_body(&status_msg, &state.db).await {
        Err(e) => {
            e.trace_error().await;

            super::comms::JobInfo { kill: false }.into() //by default, we'd rather not tell the minion to stop the jobs it's running on
        }
        Ok(v) => v.into(),
    }
}

async fn update_status_body(
    status_msg: &comms::StatusUpdate,
    db: &state_db::StateDatabase,
) -> Result<<comms::StatusUpdate as comms::Communication>::Response, UpdateLorryStatusError> {
    let path = db.find_lorry_running_job(&status_msg.job_id).await?;

    if !status_msg.stdout.is_empty() {
        db.append_to_job_output(status_msg.job_id, &status_msg.stdout)
            .await
            .map_err(|e| UpdateLorryStatusError::DBOperationError(e, path.clone()))?;
    }

    let now = db.get_current_time().await;
    db.set_job_updated(status_msg.job_id, now)
        .await
        .map_err(|e| UpdateLorryStatusError::DBOperationError(e, path.clone()))?;

    let lorry_info = db
        .get_lorry_info(&path)
        .await
        .map_err(|e| UpdateLorryStatusError::DBOperationError(e, path.clone()))?;

    match status_msg.exit_status {
        comms::JobExitStatus::Finished(exit_status) => {
            tracing::info!("Job ended! (but not neccessarily successfully)");
            let job_output = if !status_msg.exit_status.is_success() {
                let d = db
                    .get_job_output(status_msg.job_id)
                    .await
                    .map_err(|e| UpdateLorryStatusError::DBOperationError(e, path.clone()))?;
                tracing::warn!(
                    endpoint =%comms::StatusUpdate::PATH,
                    lorry = %path,
                    "Job failed:{}",
                    &d
                );
                d
            } else {
                //TODO added this for dbug. Should we keep this? Don't know if we want to keep a pile of logs for lorries that have passed OK
                let d = db
                    .get_job_output(status_msg.job_id)
                    .await
                    .map_err(|e| UpdateLorryStatusError::DBOperationError(e, path.clone()))?;
                tracing::info!("It was successful!");
                d
            };

            db.set_running_job(&path, None)
                .await
                .map_err(|e| UpdateLorryStatusError::DBOperationError(e, path.clone()))?;

            db.set_job_exit(status_msg.job_id, exit_status, now)
                .await
                .map_err(|e| UpdateLorryStatusError::DBOperationError(e, path.clone()))?;

            db.set_lorry_last_run_status(
                &path,
                &exit_status,
                &job_output,
                exit_status.disk_usage,
                now,
            )
            .await
            .map_err(|e| UpdateLorryStatusError::DBOperationError(e, path.clone()))?;
        }
        comms::JobExitStatus::Running => {
            if time_to_die(db, status_msg.job_id, &lorry_info)
                .await
                .map_err(|e| UpdateLorryStatusError::DBOperationError(e, path.clone()))?
            {
                tracing::warn!(
                    lorry = %path,
                    endpoint =%comms::StatusUpdate::PATH,
                    "Job {} has been running for too long, marking it to be killed.",
                    status_msg.job_id
                );

                db.set_kill_job(status_msg.job_id, true)
                    .await
                    .map_err(|e| UpdateLorryStatusError::DBOperationError(e, path.clone()))?;
            }
        }
    };

    let obj: comms::JobInfo = db
        .get_job_info(status_msg.job_id)
        .await
        .map_err(|e| UpdateLorryStatusError::DBOperationError(e, path.clone()))?
        .into();
    tracing::debug!("Returning job = {:?}", obj);
    Ok(obj)
}

async fn time_to_die(
    db: &state_db::StateDatabase,
    job_id: comms::JobId,
    lorry_info: &state_db::LorryEntry,
) -> Result<bool, sqlx::Error> {
    let (started, _ended) = db.get_job_start_and_end_times(job_id).await?;
    let now = db.get_current_time().await;
    let age = now - started;
    Ok(age >= lorry_info.lorry_timeout)
}

#[derive(thiserror::Error, Debug, serde::Serialize)] //TODO why is this serialize?
pub(crate) enum UpdateLorryStatusError {
    //TODO include actual request that failed?
    #[error("Error occurred during some database operation")]
    DBOperationError(
        #[serde(serialize_with = "super::dump_debug")] sqlx::Error,
        comms::LorryPath,
    ),
    #[error("There was an error trying to find the path (??? TODO what is that)")]
    CouldNotFigureWhichInstanceYouAre(#[from] state_db::FindLorryFromJobError),
}

impl UpdateLorryStatusError {
    async fn trace_error(self) {
        let (path, err_msg) = match self {
            UpdateLorryStatusError::DBOperationError(e, path) => (Some(path), format!("{}", e)),
            //TODO we instead impl IntoResponse on FindLorryRunningJobError and just call that instead?
            UpdateLorryStatusError::CouldNotFigureWhichInstanceYouAre(e) => match e {
                state_db::FindLorryFromJobError::MultipleLorriesRunningJob(runners) => {
                    (
                        None,
                        format!(
                            "There are multiple lorries claiming this job: {:?}",
                            runners
                        ),
                    )
                    //TODO is it appropriate to leak values like this? if anything, I want to *LOG* these results, not send them to the lorry!
                }
                state_db::FindLorryFromJobError::DBOperationError(e) => (
                    None,
                    format!(
                        "DB operation failed when trying to find lorry this job is running: {:?}",
                        e
                    ),
                ),
                state_db::FindLorryFromJobError::NoLorryRunningThisJob => {
                    (None, "No lorry assigned to this job".to_string())
                }
            },
        };

        //TODO I'd prefer to pass the id through the event and have the logging task call the DB rather than making this function async due to the db call , but doing that introduces the wonders of making a type work as a tracable value....
        tracing::error!(
            endpoint =%comms::StatusUpdate::PATH,
            lorry = path.map(|p| tracing::field::display(p.to_string())),
            "Error occured with updating job: {}",
            err_msg
        )
    }
}
