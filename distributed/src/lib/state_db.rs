use std::ops::Deref;

use sqlx::{sqlite::SqliteQueryResult, SqlitePool};

use crate::comms;

// FIXME: these abstractions leak
use crate::web::routes::{list_jobs, report_errors};

//TODO make DB not neccesairly be SQLite; we'd like to be able to use other DBs too!
//TODO is it correct to keep the DB connection open at all times? Should we instead open and close and with each request? Not quite sure what the python code is doing

#[derive(Debug, Clone)]
pub(crate) struct StateDatabase(SqlitePool);

impl AsRef<SqlitePool> for StateDatabase {
    fn as_ref(&self) -> &SqlitePool {
        &self.0
    }
}

impl Deref for StateDatabase {
    type Target = SqlitePool;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

//TODO tear this one up into "queries" and "updates" submodules? Or some other organisation, anything over this single clump of code
impl StateDatabase {
    //TODO so much duplicated code here, figure out how to condense it
    #[tracing::instrument(skip_all)]
    pub(crate) async fn connect(db_url: &str) -> Result<StateDatabase, sqlx::Error> {
        let me = SqlitePool::connect(db_url)
            .await
            .map_err(|e| {
                tracing::error!("DB setup failed:{}", e);
                e
            })
            .map(StateDatabase)?;

        sqlx::migrate!().run(&me.0).await.map_err(|e| {
            tracing::error!("DB migrations failed:{}", e);
            e
        })?;

        Ok(me)
    }

    #[tracing::instrument(skip_all)]
    pub async fn get_all_lorries_info(&self) -> Result<Vec<LorryEntry>, sqlx::Error> {
        sqlx::query!("SELECT * FROM lorries ORDER BY (last_run + interval)")
            //TODO this code is repeated with find_lorry_running_job; maybe they can be squashed together?
            .map(|l| {
                let (name, spec): (String, workerlib::LorrySpec) =
                    workerlib::extract_lorry_specs(l.text)
                        .unwrap()
                        .into_iter()
                        .next()
                        .unwrap(); // Lorry entry should only ever hold ONE lorry spec
                LorriesRow {
                    path: comms::LorryPath::new(l.path),
                    name,
                    spec,
                    running_job: l.running_job,
                    last_run: comms::TimeStamp::new(l.last_run),
                    interval: comms::Interval::new(l.interval),
                    lorry_timeout: comms::Interval::new(l.lorry_timeout),
                    disk_usage: l.disk_usage.map(comms::DiskUsage::new),
                    last_run_exit: l.last_run_exit,
                    last_run_error: l.last_run_error,
                    purge_from_before: comms::TimeStamp::new(l.purge_from_before), //TODO: we could use DATETIME in the DB and read this as https://docs.rs/sqlx/latest/sqlx/types/chrono/struct.DateTime.html maybe?
                }
                .into()
            })
            .fetch_all(&self.0)
            .await
            .map_err(|e| {
                tracing::error!("DB operation failed:{}", e);
                e
            })
    }

    #[tracing::instrument(skip_all)]
    pub(crate) async fn append_to_job_output(
        &self,
        job_id: comms::JobId,
        stderr: &str,
    ) -> Result<SqliteQueryResult, sqlx::Error> {
        sqlx::query!(
            "UPDATE jobs SET output= (output || ?) WHERE job_id = ?",
            stderr,
            job_id
        )
        .execute(&self.0)
        .await
        .map_err(|e| {
            tracing::error!("DB transaction failed:{}", e);
            e
        })
    }

    #[tracing::instrument(skip_all)]
    pub(crate) async fn get_current_time(&self) -> comms::TimeStamp {
        //TODO consider removing this table, I don't see a purpose for it, like max_jobs; when would I *ever* want to set this? I want the real time!
        let r = sqlx::query!("SELECT * FROM time").fetch_one(&self.0).await;
        match r {
            // Unwrap is OK; if row.now was uninitialized, then something went wrong in the first place
            // TODO on the other hand, don't we want the controller to be resilient?
            Ok(row) => comms::TimeStamp::new(row.now.unwrap()),
            Err(_) => comms::TimeStamp::new(chrono::Utc::now().timestamp()),
        }
    }

    #[tracing::instrument(skip_all)]
    pub(crate) async fn set_job_updated(
        &self,
        job_id: comms::JobId,
        now: comms::TimeStamp,
    ) -> Result<SqliteQueryResult, sqlx::Error> {
        sqlx::query!("UPDATE jobs SET updated=? where job_id IS ?", now, job_id,)
            .execute(&self.0)
            .await
            .map_err(|e| {
                tracing::error!("DB transaction failed:{}", e);
                e
            })
    }

    #[tracing::instrument(skip_all)]
    pub(crate) async fn find_lorry_running_job(
        &self,
        job_id: &comms::JobId,
    ) -> Result<comms::LorryPath, FindLorryFromJobError> {
        let mut paths = sqlx::query!("SELECT path FROM lorries WHERE running_job is ?", job_id)
            .fetch_all(&self.0)
            .await
            .map_err(|e| {
                tracing::error!("DB transaction failed:{}", e);
                e
            })?;

        match paths.len() {
            0 => Err(FindLorryFromJobError::NoLorryRunningThisJob),
            1 => Ok(comms::LorryPath::new(
                paths
                    .pop()
                    .expect("Have already verified we have an element")
                    .path,
            )),
            _ => {
                let runners = paths.into_iter().map(|r| r.path).collect::<Vec<_>>();

                tracing::error!(
                    "{} lorries are running job {job_id}:, {:?}",
                    runners.len(),
                    runners
                );
                Err(FindLorryFromJobError::MultipleLorriesRunningJob(runners))
            }
        }
    }

    #[tracing::instrument(skip_all)]
    pub(crate) async fn get_lorry_info(
        &self,
        path: &comms::LorryPath,
    ) -> Result<LorryEntry, sqlx::Error> {
        let l = sqlx::query!("SELECT * FROM lorries WHERE path is ?", path)
            .fetch_one(&self.0)
            .await?;

        // These unwraps should be OK. if the data was uninitilaized in the DB, then it is a program error, not a user error

        let (name, spec): (String, workerlib::LorrySpec) = workerlib::extract_lorry_specs(l.text)
            .unwrap()
            .into_iter()
            .next()
            .unwrap(); // Lorry entry should only ever hold ONE lorry spec
        let ret = LorriesRow {
            path: comms::LorryPath::new(l.path),
            name,
            spec,
            running_job: l.running_job,
            last_run: comms::TimeStamp::new(l.last_run),
            interval: comms::Interval::new(l.interval),
            lorry_timeout: comms::Interval::new(l.lorry_timeout),
            disk_usage: l.disk_usage.map(comms::DiskUsage::new),
            last_run_exit: l.last_run_exit,
            last_run_error: l.last_run_error,
            purge_from_before: comms::TimeStamp::new(l.purge_from_before),
        };
        Ok(ret.into())
    }

    #[tracing::instrument(skip_all)]
    pub(crate) async fn get_job_info(&self, job_id: comms::JobId) -> Result<JobRow, sqlx::Error> {
        let r = sqlx::query!("SELECT * FROM jobs WHERE job_id = ?", job_id)
            .fetch_one(&self.0)
            .await
            .map_err(|e| {
                tracing::error!("DB transaction failed:{}", e);
                e
            })?;
        // These unwraps should be OK; if the values are unset in the DB then we are initializing wrong
        //TODO I'm sure we can remove this near-identity code
        let ret = JobRow {
            job_id: comms::JobId::new(r.job_id),
            host: r.host,
            pid: r.pid,
            started: comms::TimeStamp::new(r.started),
            ended: r.ended.map(comms::TimeStamp::new),
            updated: comms::TimeStamp::new(r.updated),
            kill: match r.kill {
                1 => true,
                0 => false,
                _ => panic!("bad value for kill placed in DB"),
            },
            path: comms::LorryPath::new(r.path),
            exit: r.exit,
            disk_usage: r.disk_usage,
            //output: "dummy value do not keep".to_string(), // TODO getting output is going to be a bit more complex than this, so we're passing some dummy values r.output.unwrap(),
        };
        Ok(ret)
    }

    #[tracing::instrument(skip_all)]
    pub(crate) async fn get_job_start_and_end_times(
        &self,
        job_id: comms::JobId,
    ) -> Result<(comms::TimeStamp, Option<comms::TimeStamp>), sqlx::Error> {
        let r = sqlx::query!("SELECT started, ended FROM jobs WHERE job_id IS ?", job_id)
            .fetch_one(&self.0)
            .await
            .map_err(|e| {
                tracing::error!("DB transaction failed:{}", e);
                e
            })?;
        Ok((
            comms::TimeStamp::new(r.started),
            r.ended.map(comms::TimeStamp::new),
        ))
    }

    //TODO is kill_status *ever* passed false? may as well drop it then, to make clear thisfn is for making the job to be killed
    #[tracing::instrument(skip_all)]
    pub(crate) async fn set_kill_job(
        &self,
        job_id: comms::JobId,
        kill_status: bool,
    ) -> Result<SqliteQueryResult, sqlx::Error> {
        //TODO logging

        let kill_status_as_int: i32 = kill_status.into();

        sqlx::query!(
            "UPDATE jobs SET kill=? WHERE job_id=?",
            kill_status_as_int,
            job_id,
        )
        .execute(&self.0)
        .await
        .map_err(|e| {
            tracing::error!("DB operation failed:{}", e);
            e
        })
    }

    #[tracing::instrument(skip_all)]
    pub(crate) async fn get_job_output(&self, job_id: comms::JobId) -> Result<String, sqlx::Error> {
        //we're not doing the linked-list model in the end and instead allowing the DB to handle the concatanation, which should be a little less slow than doing it on the application end
        let last_output = sqlx::query!("SELECT output FROM jobs WHERE job_id IS ?", job_id)
            .fetch_one(&self.0)
            .await
            .map_err(|e| {
                tracing::error!("DB transaction failed:{}", e);
                e
            })?;

        Ok(last_output.output)
    }

    //TODO wouldn't ot make more sense to place the output in the jobs table and instead have lorry rows forign key link into jobs?
    // We already place the output there so it would only reduce the duplication of data
    #[tracing::instrument(skip_all)]
    pub(crate) async fn set_lorry_last_run_status(
        &self,
        path: &comms::LorryPath,
        exit_status: &comms::JobFinishedResults,
        job_output: &str,
        disk_usage: comms::DiskUsage,

        last_run_time: comms::TimeStamp,
    ) -> Result<SqliteQueryResult, sqlx::Error> {
        sqlx::query!(
            "UPDATE lorries SET last_run=?, last_run_exit=?, last_run_error=? ,disk_usage=? WHERE path=?",
            last_run_time,
            exit_status.exit_code,
            job_output,
            disk_usage,
            path
        )
        .execute(&self.0)
        .await
        .map_err(|e| {
            tracing::error!("DB operation failed:{}", e);
            e
        })
    }

    /// Tell the DB that the job with id `job_id` has exited, with the exit code and disk usage info in the `exit_status` struct
    #[tracing::instrument(skip_all)]
    pub(crate) async fn set_job_exit(
        &self,
        job_id: comms::JobId,
        exit_status: comms::JobFinishedResults,
        exit_time: comms::TimeStamp,
    ) -> Result<SqliteQueryResult, sqlx::Error> {
        sqlx::query!(
            "UPDATE jobs SET exit=?, ended=?, disk_usage=? WHERE job_id IS ?",
            exit_status.exit_code,
            exit_time,
            exit_status.disk_usage,
            job_id
        )
        .execute(&self.0)
        .await
        .map_err(|e| {
            tracing::error!("DB operation failed:{}", e);
            e
        })
    }

    //TODO maybe these args could be combined into one LorryRow or something?
    #[tracing::instrument(skip_all)]
    pub(crate) async fn add_to_lorries(
        &self,
        path: &comms::LorryPath,
        text: &std::collections::BTreeMap<&comms::LorryPath, workerlib::LorrySpec>,
        interval: comms::Interval,
        timeout: comms::Interval,
    ) -> Result<SqliteQueryResult, sqlx::Error> {
        let text = serde_yaml::to_string(text).unwrap(); //TODO pass up an error instead
                                                         //TODO

        let now = self.get_current_time().await;
        match self.get_lorry_info(path).await {
            Err(_)=>sqlx::query!(
                "INSERT INTO lorries    (path, text, last_run, interval, lorry_timeout, running_job, purge_from_before) 
                VALUES             (?,    ?,   ?,?,       ?,      ?,                   ?)",
                                    path, text,0,interval,timeout,Option::<i64>::None, now
            ),
            Ok(_)=>
                sqlx::query!("UPDATE lorries SET text=?, interval=?, lorry_timeout=? WHERE path IS ?",text, interval, timeout, path),
        }.execute(&self.0)
        .await
        .map_err(|e| {
            tracing::error!("DB operation failed:{}", e);
            e
        })
    }

    //Set the running job ID for the lorry at `path`. If None, this means the job it was running has finished and it has no job.
    //TODO maybe this should be two seperate functions, one for clearing the field and one for se4tting it to a real value?
    #[tracing::instrument(skip_all)]
    pub(crate) async fn set_running_job(
        &self,
        path: &comms::LorryPath,
        job_id: Option<comms::JobId>,
    ) -> Result<SqliteQueryResult, sqlx::Error> {
        //TODO debug

        //TODO if job_id is None, then we're.. clearing the entry for the lorry at the given path
        //I don't think that is easy to reason about, so this function needs to be reworked a bit

        sqlx::query!(
            "UPDATE lorries SET running_job=? WHERE path=?",
            job_id,
            path
        )
        .execute(&self.0)
        .await
        .map_err(|e| {
            tracing::error!("DB operation failed:{}", e);
            e
        })
    }

    #[tracing::instrument(skip_all)]
    pub(crate) async fn get_running_jobs(&self) -> Result<Vec<super::comms::JobId>, sqlx::Error> {
        sqlx::query!("SELECT running_job FROM lorries WHERE running_job IS NOT NULL")
            .map(|r| super::comms::JobId::new(r.running_job.unwrap())) //the SQL query verifies that running_job is not-none so this unwrap is OK
            .fetch_all(&self.0)
            .await
            .map_err(|e| {
                tracing::error!("DB transaction failed:{}", e);
                e
            })
    }

    #[tracing::instrument(skip_all)]
    pub(crate) async fn get_running_queue(&self) -> Result<bool, sqlx::Error> {
        Ok(sqlx::query!("SELECT running FROM running_queue")
            .fetch_one(&self.0)
            .await?
            .running.unwrap() //TODO set running field to NOT NULL? We init it from the beginning anyways
            != 0)
    }

    #[tracing::instrument(skip_all)]
    pub(crate) async fn get_next_job_id(&self) -> Result<comms::JobId, sqlx::Error> {
        let r = sqlx::query!("SELECT job_id FROM next_job_id",)
            .fetch_one(&self.0)
            .await
            .map_err(|e| {
                tracing::error!("DB operation failed:{}", e);
                e
            })?
            .job_id
            .expect("job_id field is not in next_job_id db!"); //TODO this should be made explicitly NOT NULL

        let n = r + 1;
        sqlx::query!("UPDATE next_job_id SET job_id=?", n)
            .execute(&self.0)
            .await
            .map_err(|e| {
                tracing::error!("DB operation failed:{}", e);
                e
            })?;

        Ok(comms::JobId::new(r))
    }

    #[tracing::instrument(skip_all)]
    pub(crate) async fn add_new_job(
        &self,
        job_id: comms::JobId,
        host: &str,
        pid: u32,
        path: &comms::LorryPath,
        started: comms::TimeStamp,
    ) -> Result<SqliteQueryResult, sqlx::Error> {
        sqlx::query!("INSERT INTO jobs (job_id, host, pid, path, started,updated, kill,output) VALUES (?, ?, ?, ?, ?, ?, ?,?)", job_id, host, pid, path, started, started, 0,"")
            .execute(&self.0)
            .await
            .map_err(|e| {
                tracing::error!("DB operation failed:{}", e);
                e
            })
    }

    #[tracing::instrument(skip_all)]
    pub(crate) async fn get_lorries_paths(
        &self,
    ) -> Result<Vec<super::comms::LorryPath>, sqlx::Error> {
        sqlx::query!("SELECT path FROM lorries ORDER BY (last_run + interval)")
            .map(|r| super::comms::LorryPath::new(r.path))
            .fetch_all(&self.0)
            .await
            .map_err(|e| {
                tracing::error!("DB operation failed:{}", e);
                e
            })
    }

    #[tracing::instrument(skip_all)]
    pub(crate) async fn remove_lorry(
        &self,
        path: &super::comms::LorryPath,
    ) -> Result<SqliteQueryResult, sqlx::Error> {
        sqlx::query!("DELETE FROM lorries WHERE path IS ?", path)
            .execute(&self.0)
            .await
            .map_err(|e| {
                tracing::error!("DB operation failed:{}", e);
                e
            }) //TODO this map_err is everywhere, it's a bit boilerplatey now, no?
    }

    #[tracing::instrument(skip_all)]
    pub(crate) async fn set_repo_to_purge(&self) -> Result<SqliteQueryResult, sqlx::Error> {
        let now = self.get_current_time().await;

        sqlx::query!("UPDATE lorries SET purge_from_before = ?", now)
            .execute(&self.0)
            .await
            .map_err(|e| {
                tracing::error!("DB operation failed:{}", e);
                e
            })
    }

    #[tracing::instrument(skip_all)]
    pub(crate) async fn get_individual_job_records(
        &self,
    ) -> Result<Vec<list_jobs::JobResult>, sqlx::Error> {
        sqlx::query!("SELECT job_id, path, exit,host ,disk_usage FROM jobs")
            .map(|r| list_jobs::JobResult {
                id: comms::JobId::new(r.job_id),
                path: r.path,
                exit_status: r.exit.map_or(comms::JobExitStatus::Running, |c| {
                    comms::JobExitStatus::Finished(comms::JobFinishedResults {
                        exit_code: c,
                        disk_usage: comms::DiskUsage::new(r.disk_usage.unwrap_or(0)),
                    })
                }),
                host: r.host,
            })
            .fetch_all(&self.0)
            .await
    }

    pub(crate) async fn set_last_run_time(
        &self,
        path: &comms::LorryPath,
        now: comms::TimeStamp,
    ) -> Result<(), sqlx::Error> {
        sqlx::query!("UPDATE lorries SET last_run = ? WHERE path = ?", now, path)
            .execute(&self.0)
            .await
            .map_err(|e| {
                tracing::error!("DB operation failed:{}", e);
                e
            })
            .map(|_| ())
    }

    // UNDER ***NO*** CIRCUMSTANCE SHOULD YOU PUT A `tracing::error!` or `tracing::warn!` HERE
    // BECUASE THE SYSTEM WILL ATTEMPT TO LOG THOSE TO DB, USING THIS VERY FUNCTION
    // WHICH CAN RESULT IN AN INFINITE LOOP AS IT ERRORS AGAIN TRYING LOG ITS OWN ERROR
    #[tracing::instrument(skip_all)]
    pub(crate) async fn record_error(
        &self,
        error: &str,
        endpoint: &str,
        for_lorry: Option<comms::LorryPath>,
        timestamp: super::comms::TimeStamp,
    ) -> Result<SqliteQueryResult, sqlx::Error> {
        sqlx::query!(
            "INSERT INTO errors (error, time_occurred, in_endpoint, for_lorry) VALUES (?, ?, ?, ?)",
            error,
            timestamp,
            endpoint,
            for_lorry
        )
        .execute(&self.0)
        .await
        .map_err(|e| {
            tracing::debug!("DB operation failed:{}", e);
            e
        })
    }

    //TODO perhaps a param like cutoff date? Or will that be handled by the cleanup endpoint
    #[tracing::instrument(skip_all)]
    pub(crate) async fn fetch_errors(&self) -> Result<Vec<report_errors::ErrorLog>, sqlx::Error> {
        sqlx::query!("SELECT * FROM errors")
            .map(|r| report_errors::ErrorLog {
                error_message: r.error,
                endpoint: r.in_endpoint.unwrap_or("".to_string()),
                timestamp: super::comms::TimeStamp::new(r.time_occurred),
                for_lorry: r.for_lorry.map(super::comms::LorryPath::new),
            })
            .fetch_all(&self.0)
            .await
            .map_err(|e| {
                tracing::warn!("DB operation failed:{}", e);
                e
            })
    }

    #[tracing::instrument(skip_all)]
    pub(crate) async fn test_connection(&self) -> Result<(), sqlx::Error> {
        self.0.acquire().await?;
        Ok(())
    }
}

// Several of these fields are unused but maybe be useful further down the line
#[derive(sqlx::FromRow)]
pub(crate) struct JobRow {
    job_id: comms::JobId,
    #[allow(unused)]
    host: String,
    #[allow(unused)]
    pid: i64,
    #[allow(unused)]
    started: comms::TimeStamp,
    #[allow(unused)]
    ended: Option<comms::TimeStamp>,
    updated: comms::TimeStamp,
    kill: bool,
    path: comms::LorryPath,
    #[allow(unused)]
    exit: Option<i64>,
    #[allow(unused)]
    disk_usage: Option<i64>,
    //output: todo!()
}
impl JobRow {
    pub fn path(&self) -> &comms::LorryPath {
        &self.path
    }

    pub fn job_id(&self) -> comms::JobId {
        self.job_id
    }

    pub fn updated(&self) -> comms::TimeStamp {
        self.updated
    }
}
impl From<JobRow> for comms::JobInfo {
    fn from(value: JobRow) -> Self {
        Self { kill: value.kill }
    }
}

#[derive(sqlx::FromRow, Debug)] //TODO remove FromRow?
struct LorriesRow {
    pub path: comms::LorryPath,
    pub name: String,
    pub spec: workerlib::LorrySpec,
    pub running_job: Option<i64>, //TODO give this its own type?
    pub last_run: comms::TimeStamp,
    pub interval: comms::Interval,
    pub lorry_timeout: comms::Interval,
    pub last_run_exit: Option<i64>,
    pub disk_usage: Option<comms::DiskUsage>,
    pub last_run_error: Option<String>, //TODO should probably get its own type
    pub purge_from_before: comms::TimeStamp,
}

#[derive(Debug)]
pub(crate) struct LorryEntry {
    pub path: comms::LorryPath,
    pub lorry_name: String,
    pub lorry_spec: workerlib::LorrySpec,
    pub running_job: Option<i64>, //TODO give this its own type?
    pub last_run: comms::TimeStamp,
    pub interval: comms::Interval,
    pub lorry_timeout: comms::Interval, //TODO the two interval fields *seem* to only be initiliased to the comms::Interval fields in Host. But I'm not sure if they written to at some point. If not, let's just merge them, it'll make things nicer I think
    pub last_run_results: comms::JobExitStatus,
    pub last_run_error: Option<String>, //TODO should probably get its own type
    pub purge_before: comms::TimeStamp,
}

impl From<LorriesRow> for LorryEntry {
    fn from(value: LorriesRow) -> Self {
        LorryEntry {
            path: value.path,
            lorry_name: value.name,
            lorry_spec: value.spec,
            running_job: value.running_job,
            last_run: value.last_run,
            interval: value.interval,
            lorry_timeout: value.lorry_timeout,
            last_run_results: if let Some(e) = value.last_run_exit {
                comms::JobExitStatus::Finished(comms::JobFinishedResults{ exit_code:e, disk_usage: value.disk_usage.expect("If this panics then our DB operatiomns have failed to update these together, which they always should be!") })
            } else {
                comms::JobExitStatus::Running //or, technically, has not been run yet
            },
            last_run_error: value.last_run_error,
            purge_before: value.purge_from_before,
        }
    }
}

//TODO should these be errors? I feel the DB schema + code is supposed to prevent any of these cases from ever happening
// perhaps they should be panics instead?
#[derive(thiserror::Error, Debug, serde::Serialize)]
pub(crate) enum FindLorryFromJobError {
    #[error("There are multiple lorries that claim this job")]
    MultipleLorriesRunningJob(Vec<String>),
    #[error("DB backend access failed")]
    DBOperationError(
        #[from]
        #[serde(serialize_with = "super::dump_debug")]
        sqlx::Error,
    ),
    #[error("No lorry is linked to this job")]
    NoLorryRunningThisJob,
}
