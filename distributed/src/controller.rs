use std::{net::SocketAddr, path::PathBuf, str::FromStr, time::Duration};

use axum::serve;
use clap::{arg, Parser};
use serde::Deserialize;
use serde_with::{serde_as, DisplayFromStr};
use tokio::net::TcpListener;
use tracing::Level;

use distributed::downstream::PartialDownstreamSetup;
// FIXME: This abstraction leaks
use distributed::web;

/// The default period to wait before lorry re-reads the confgit
const DEFAULT_PERIOD: Duration = Duration::from_secs(120);

#[serde_as]
#[derive(Parser, Debug, Deserialize)]
#[command(author, version, about, long_about = None)]
#[serde(rename_all = "kebab-case")]
struct PartialControllerArguments {
    /// Path to the state database. If there is no database at this path, one will be created.
    #[arg(long = "statedb")]
    #[serde(rename = "statedb")]
    state_db: Option<String>, //TODO this could be a URL or a path, can we enforce this?

    /// Local directory populated with configuration files. This will be populated when the /read_config
    /// endpoint is called. Initially, this is not read, and the endpoint must be called for controller
    /// to read in the configuration. This includes information about what we want to mirror.
    #[arg(long = "configuration-directory")]
    configuration_directory: Option<PathBuf>,

    /// Get the configuration git repo from this git URL; it will be mirrored to `configuration-directory`.
    /// This can be a local git repository or a remote; so long as it is a git repo.
    #[arg(long = "confgit-url")]
    confgit_url: Option<String>, //TODO use a URL?

    /// The period between lorry's attempt to re-read the confgit
    ///
    /// Defaults to 2 minutes.
    ///
    /// Note, lorry reads the confgit immediately at startup, regardless of the
    /// value set here.
    #[arg(long = "confgit-update-period")]
    confgit_update_period: Option<iso8601::Duration>,

    /// The period between lorry's removal of ghost jobs
    ///
    /// Defaults to 2 minutes.
    #[arg(long = "remove-ghost-period")]
    remove_ghost_period: Option<iso8601::Duration>,

    /// Name of the branch on `confgit-url` that contains the configuration. The base config file should
    /// be called `lorry-controller.conf`, and in the root of the work-tree.
    #[arg(long = "confgit-branch")]
    confgit_branch: Option<String>,

    #[command(subcommand)]
    #[serde(flatten)]
    downstream: Option<distributed::downstream::PartialDownstreamSetup>,

    /// Controller listens on this port number (default:3000)
    #[arg(long = "port")]
    port: Option<u16>,

    #[serde_as(as = "Option<DisplayFromStr>")]
    #[arg(long = "log-level")]
    log_level: Option<Level>,
}

struct ControllerConfig {
    state_db: String,

    configuration_directory: PathBuf,

    confgit_url: String,

    confgit_branch: String,

    confgit_update_period: Duration,

    remove_ghost_period: Duration,

    downstream: distributed::downstream::DownstreamSetup,

    port: u16,

    log_level: Option<Level>,
}

impl From<ControllerCommandLineArguments> for ControllerConfig {
    fn from(value: ControllerCommandLineArguments) -> Self {
        let args = value.args;

        let read_file = value.config_file.and_then(|p| {
            let contents = match std::fs::read_to_string(&p) {
                Ok(content) => content,
                Err(e) => {
                    eprintln!("Could not read config file {}:\n {}", p.display(), e);
                    return None;
                }
            };

            match toml::from_str::<PartialControllerArguments>(&contents) {
                Ok(config) => Some(config),
                Err(err) => {
                    eprintln!("The config file {} was invalid: {err}", p.display());
                    None
                }
            }
        });
        let args = if let Some(read_file) = read_file {
            // Merge the downstream setup objects
            let d = match (args.downstream, read_file.downstream) {
                (None, None) => None,
                (None, Some(v)) => Some(v),
                (Some(v), None) => Some(v),
                (
                    Some(PartialDownstreamSetup::Local {
                        base_dir: base_dir_a,
                    }),
                    Some(PartialDownstreamSetup::Local {
                        base_dir: base_dir_b,
                    }),
                ) => Some(PartialDownstreamSetup::Local {
                    base_dir: base_dir_a.or(base_dir_b),
                }),
                (
                    Some(PartialDownstreamSetup::Gitlab {
                        hostname: host_url,
                        downstream_visibility,
                        private_token,
                        insecure,
                    }),
                    Some(PartialDownstreamSetup::Local { base_dir: _ }),
                ) => Some(PartialDownstreamSetup::Gitlab {
                    hostname: host_url,
                    downstream_visibility,
                    private_token,
                    insecure,
                }),
                (
                    Some(PartialDownstreamSetup::Local { base_dir }),
                    Some(PartialDownstreamSetup::Gitlab {
                        hostname: _,
                        downstream_visibility: _,
                        private_token: _,
                        insecure: _,
                    }),
                ) => Some(PartialDownstreamSetup::Local { base_dir }),

                (
                    Some(PartialDownstreamSetup::Gitlab {
                        hostname: host_url_a,
                        downstream_visibility: downstream_visibility_a,
                        private_token: private_token_a,
                        insecure: insecure_a,
                    }),
                    Some(PartialDownstreamSetup::Gitlab {
                        hostname: host_url_b,
                        downstream_visibility: downstream_visibility_b,
                        private_token: private_token_b,
                        insecure: insecure_b,
                    }),
                ) => Some(PartialDownstreamSetup::Gitlab {
                    hostname: host_url_a.or(host_url_b),
                    downstream_visibility: downstream_visibility_a.or(downstream_visibility_b),
                    private_token: private_token_a.or(private_token_b),
                    insecure: insecure_a.or(insecure_b),
                }),
            };

            PartialControllerArguments {
                state_db: args.state_db.or(read_file.state_db),
                configuration_directory: args
                    .configuration_directory
                    .or(read_file.configuration_directory),
                confgit_url: args.confgit_url.or(read_file.confgit_url),
                confgit_branch: args.confgit_branch.or(read_file.confgit_branch),
                downstream: d,
                port: args.port.or(read_file.port),
                confgit_update_period: args
                    .confgit_update_period
                    .or(read_file.confgit_update_period),
                remove_ghost_period: args.remove_ghost_period.or(read_file.remove_ghost_period),
                log_level: read_file.log_level.or(read_file.log_level),
            }
        } else {
            args
        };

        ControllerConfig {
            state_db: args.state_db.expect("Must specify statedb"),
            configuration_directory: args
                .configuration_directory
                .unwrap_or(PathBuf::from_str(".").expect("this is a valid path")),
            confgit_url: args.confgit_url.expect("Must specify confgit-url"),
            confgit_branch: args.confgit_branch.expect("Must specify confgit-branch"),
            confgit_update_period: args
                .confgit_update_period
                .map(Duration::from)
                .unwrap_or_else(|| DEFAULT_PERIOD.to_owned()),
            remove_ghost_period: args
                .remove_ghost_period
                .map(Duration::from)
                .unwrap_or_else(|| DEFAULT_PERIOD.to_owned()),
            downstream: args
                .downstream
                .expect("Must specify a downstream")
                .try_into()
                .expect("Did not fully specify downstream"),
            port: args.port.unwrap_or(3000),
            log_level: args.log_level,
        }
    }
}

#[derive(Parser)]
struct ControllerCommandLineArguments {
    #[command(flatten)]
    args: PartialControllerArguments,

    #[arg(long = "config")]
    config_file: Option<PathBuf>,
}

#[tokio::main]
async fn main() {
    let args: ControllerConfig = ControllerCommandLineArguments::parse().into();

    let db_url = args.state_db; //TODO allow DB to be configurable: i.e could be SQLite, could be Postgres
                                //TODO fetch this from the environment vars or .env file rather than just the CLI.

    let settings = web::ControllerSettings {
        downstream: async_convert::TryInto::<distributed::downstream::Downstream>::try_into(
            args.downstream,
        )
        .await
        .expect("Could not connect to downstream"),
        configuration_directory: args.configuration_directory,
        confgit_url: args.confgit_url,
        confgit_update_period: args.confgit_update_period,
        remove_ghost_period: args.remove_ghost_period,
        confgit_branch: args.confgit_branch,
        log_level: args.log_level,
    };
    let addr = SocketAddr::from(([0, 0, 0, 0], args.port));
    let app = web::app(&db_url, settings).await;
    tracing::subscriber::set_global_default(app.logger).unwrap();
    let listener = TcpListener::bind(addr).await.unwrap();
    let server_future = serve(listener, app.router);
    let db_writer_handler = app.writer;
    server_future.await.expect("Failed to run server..");
    db_writer_handler.await.expect("Failed to run logger");
}

#[cfg(test)]
mod test {
    use crate::PartialControllerArguments;

    use tracing::Level;

    #[test]
    fn config_parse() {
        let config = r#"
            statedb = "/db/go/here"
            configuration-directory = "/config/go/here"
            confgit-url = "/come/git/your/config"
            confgit-update-period = "PT30H"
            remove-ghost-period = "PT20S"
            confgit-branch = "hi/im/a/branch"
            port = 20
            log-level = "debug"
        "#;

        let config = toml::from_str::<PartialControllerArguments>(config).unwrap();

        assert_eq!(config.state_db, Some("/db/go/here".to_owned()));
        assert_eq!(
            config.configuration_directory,
            Some("/config/go/here".into())
        );
        assert_eq!(config.confgit_url, Some("/come/git/your/config".to_owned()));
        assert_eq!(config.confgit_update_period, Some("PT30H".parse().unwrap()));
        assert_eq!(config.remove_ghost_period, Some("PT20S".parse().unwrap()));
        assert_eq!(config.confgit_branch, Some("hi/im/a/branch".to_owned()));
        assert_eq!(config.port, Some(20));
        assert_eq!(config.log_level, Some(Level::DEBUG));
    }
}
