use std::{path::PathBuf, process::ExitCode};

use clap::Parser;
use distributed::minion::MinionArguments;
use serde::Deserialize;
use thiserror::Error;
use tracing::error;

#[derive(Parser, Debug, Deserialize, Clone)]
#[command(author, version, about, long_about = None)]
#[serde(rename_all = "kebab-case")]
pub struct PartialMinionCommandLineArgs {
    #[command(flatten)]
    #[serde(flatten)]
    settings: workerlib::combine_configs::PartialArguments,

    /// The URL the controller is listening on
    #[arg(long = "controller-address")]
    controller_address: Option<reqwest::Url>, //Default:0.0.0.0:3000

    /// Username for auth when pushing to git server
    #[arg(long = "username")]
    username: Option<String>,

    /// Token for minion to use when pushing to git server
    #[arg(long = "private-token")]
    private_token: Option<String>,

    /// Downstream host name
    #[arg(long = "host-name")]
    host_name: Option<String>,

    /// Downstream port
    #[arg(long = "port")]
    port: Option<u16>,

    /// The timout to set for requests to the controller (default: 10s)
    #[arg(long = "controller-timeout")]
    timeout: Option<u64>, //default 10

    /// How long to wait between requests to the controller upon receiving a message that there are no jobs.
    /// Excluding this value, or setting it to 0, gives this a random value between 30 and 60 seconds.
    #[arg(long = "sleep")]
    sleep: Option<u64>,
}

//TODO Could use a rewrite on the doc comments here, given they are printed to the user when they ask for --help
#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
struct MinionCommandLineArgs {
    #[command(flatten)]
    settings: PartialMinionCommandLineArgs,

    /// Can specify further configuration in this file. Options here correspond to the options that can be used on the command line
    #[arg(long = "config")]
    lorry_config_file: Option<PathBuf>,
}

#[derive(Error, Debug)]
enum MinionError {
    #[error("You must provide *both* a username and private-token in the minion config to mirror via https.
NOTE you don't need to provide any auth for ssh.")]
    InvalidAuth,
}

impl TryFrom<MinionCommandLineArgs> for (MinionArguments, logging::Receiver) {
    type Error = MinionError;
    fn try_from(
        value: MinionCommandLineArgs,
    ) -> Result<(MinionArguments, logging::Receiver), Self::Error> {
        let config_fragments = value
            .lorry_config_file
            .map(|p| {
                let contents = match std::fs::read_to_string(&p) {
                    Ok(content) => content,
                    Err(e) => {
                        panic!("Could not read config file {}:\n {}", p.display(), e);
                    }
                };
                match toml::from_str::<PartialMinionCommandLineArgs>(&contents) {
                    Ok(config) => config,
                    Err(err) => panic!("The config file {} was invalid: {err}", p.display()),
                }
            })
            .into_iter()
            .chain(std::iter::once(value.settings));
        let (acc, rx) = MinionArguments::base();
        let conf = config_fragments.fold(acc, |acc, cur| MinionArguments {
            mirroring_settings: workerlib::Arguments {
                working_area: cur
                    .settings
                    .working_area
                    .unwrap_or(acc.mirroring_settings.working_area),
                pull_only: cur
                    .settings
                    .pull_only
                    .unwrap_or(acc.mirroring_settings.pull_only),
                verbose_logging: cur
                    .settings
                    .verbose
                    .unwrap_or(acc.mirroring_settings.verbose_logging),
                repack: cur.settings.repack.unwrap_or(acc.mirroring_settings.repack),
                keep_multiple_backups: cur
                    .settings
                    .keep_multiple_backups
                    .unwrap_or(acc.mirroring_settings.keep_multiple_backups),
                push_options: cur
                    .settings
                    .push_options
                    .unwrap_or(acc.mirroring_settings.push_options),
                check_ssl_certificates: cur
                    .settings
                    .check_certs
                    .unwrap_or(acc.mirroring_settings.check_ssl_certificates),
                transmitter: acc.mirroring_settings.transmitter,
                maximum_redirects: cur
                    .settings
                    .maximum_redirects
                    .unwrap_or(acc.mirroring_settings.maximum_redirects),
                log_level: acc
                    .mirroring_settings
                    .log_level
                    .or(acc.mirroring_settings.log_level),
            },
            controller_address: cur.controller_address.unwrap_or(acc.controller_address),
            username: cur.username.or(acc.username),
            private_token: cur.private_token.or(acc.private_token),
            host_name: cur.host_name.or(acc.host_name),
            port: cur.port.or(acc.port),
            controller_timeout: cur
                .timeout
                .map(std::time::Duration::from_secs)
                .unwrap_or(acc.controller_timeout),
            sleep: cur
                .sleep
                .map(std::time::Duration::from_secs)
                .unwrap_or(acc.sleep),
        });

        match (&conf.username, &conf.private_token) {
            (Some(_), Some(_)) => (),
            (None, None) => (),
            _ => return Err(MinionError::InvalidAuth),
        };

        Ok((conf, rx))
    }
}

#[tokio::main]
pub async fn main() -> ExitCode {
    let (args, mut rx) = match MinionCommandLineArgs::parse().try_into() {
        Ok(out) => out,
        Err(e) => {
            error!("Minion failed: {e}");
            return ExitCode::FAILURE;
        }
    };
    tracing_subscriber::fmt()
        .with_file(true)
        .with_line_number(true)
        .with_max_level(args.mirroring_settings.log_level)
        .compact()
        .try_init()
        .ok();

    distributed::minion::work(args, &mut rx).await;
    error!("Minion seems to have crashed");
    ExitCode::FAILURE
}

#[cfg(test)]
mod test {
    use crate::PartialMinionCommandLineArgs;

    use tracing::Level;

    #[test]
    fn config_parse() {
        let config = r#"
            # Oh look a comment!
            username = "alan_partridge"
            private-token = "aha!"
            host-name = "example.com"
            port = 21
            controller-address = "http://lorry-controller:3000"
            working-area = "/work-area"
            timeout = 10
            sleep = 10

            pull-only = true
            verbose = true
            repack = true
            keep-multiple-backups = true
            push-options = ["--ooo-an-option"]
            check-certs = true
            test-parsing-only = true
            maximum-redirects = 1
            log-level = "info"
        "#;

        let config = toml::from_str::<PartialMinionCommandLineArgs>(config).unwrap();
        assert_eq!(config.username, Some("alan_partridge".to_owned()));
        assert_eq!(config.private_token, Some("aha!".to_owned()));
        assert_eq!(config.host_name, Some("example.com".to_owned()));
        assert_eq!(config.port, Some(21));
        assert_eq!(
            config.controller_address,
            Some("http://lorry-controller:3000".try_into().unwrap())
        );
        assert_eq!(config.settings.working_area, Some("/work-area".into()));
        assert_eq!(config.timeout, Some(10));
        assert_eq!(config.sleep, Some(10));

        assert_eq!(config.settings.pull_only, Some(true));
        assert_eq!(config.settings.verbose, Some(true));
        assert_eq!(config.settings.repack, Some(true));
        assert_eq!(config.settings.keep_multiple_backups, Some(true));
        assert_eq!(
            config.settings.push_options,
            Some(vec!["--ooo-an-option".to_owned()])
        );
        assert_eq!(config.settings.check_certs, Some(true));
        assert_eq!(config.settings.test_parsing_only, Some(true));
        assert_eq!(config.settings.log_level, Some(Level::INFO));
    }
}
