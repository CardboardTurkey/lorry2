use std::fmt::Display;

use fancy_regex::Regex;
use std::sync::OnceLock;

fn get_expressions() -> &'static Vec<Regex> {
    static EXPRESSIONS: OnceLock<Vec<Regex>> = OnceLock::new();
    EXPRESSIONS.get_or_init(|| vec![Regex::new(r##"/glpat-\w+"##).unwrap()])
}

/// redact removes any known sensitive strings from the input and replaces them
/// with LORRY_REDACTED
pub fn redact(input: &impl Display) -> String {
    let mut copy = input.to_string();
    for expr in get_expressions() {
        copy = expr.replace_all(&copy, "LORRY_REDACTED").to_string();
    }
    copy
}
