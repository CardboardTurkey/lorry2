///Command builder that supplies deaults to reduce verbosity of git calls
pub mod command_wrapper;

/// Helper utility that can redact sensitive strings common in Lorry
pub mod redact;
