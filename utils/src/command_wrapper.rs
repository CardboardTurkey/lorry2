use std::{ffi::OsStr, future::Future, path::Path};

use thiserror::Error;
use tokio::process::Command;

#[derive(Error, Debug)]
pub enum CommandExecutionError {
    /// Program failed to open a subprocess.
    #[error("Unable to run subcommand {command}")]
    IO {
        command: String,
        source: std::io::Error,
    },

    /// The subprocess ran to completion but returned a failure code.
    #[error("Command {command:?} failed with STDERR: {stderr}")]
    CommandError {
        command: Command,
        status: std::process::ExitStatus,
        stderr: String,
    },
}

///This is a wrapper around the std::Command struct that just allows us to write commands less verbosely by supplying the defaults implicitly (i.e setting up the STDOUT/ERR)
/// as well as debug information and handing out and err in a more convenient tuple.
pub struct CommandBuilder {
    internal: Command,
}

impl CommandBuilder {
    ///Creates a new Command builder that will call the given program
    pub fn new<A: AsRef<OsStr>>(cmd: A) -> CommandBuilder {
        let mut cmd = Command::new(cmd);
        cmd.kill_on_drop(true);
        CommandBuilder { internal: cmd }
    }

    ///Appends an argument to the command
    pub fn arg<A: AsRef<OsStr>>(mut self, arg: A) -> CommandBuilder {
        self.internal.arg(arg);
        self
    }

    ///Appends asequence of arguments to the command
    pub fn args<A: AsRef<OsStr>>(mut self, args: &[A]) -> CommandBuilder {
        self.internal.args(args);
        self
    }

    ///Sets enviroment variables for the process the command will execute in. Each pair in the argument array is the name and value to which to sdet each enviroment variable.
    pub fn env<A: AsRef<OsStr>>(mut self, env: &[(A, A)]) -> CommandBuilder {
        for (key, val) in env {
            self.internal.env(key, val);
        }
        self
    }

    ///Executes the built process in the given directory. Takes no STDIN and runs to completion. If the command succeeds, returns the programs STDOUT and STDERR.
    /// Also logs that the command was run and the results to debug.
    pub async fn execute<P: AsRef<Path>, C, F>(
        mut self,
        cwd: P,
        debug: C,
    ) -> Result<(String, String), CommandExecutionError>
    where
        C: Fn(String) -> F,
        F: Future<Output = ()>,
    {
        self.internal.current_dir(cwd);

        self.internal.stdin(std::process::Stdio::null());

        let child_process_output =
            self.internal
                .output()
                .await
                .map_err(|e| CommandExecutionError::IO {
                    command: format!("{:?}", self.internal),
                    source: e,
                })?;
        let succeeded = child_process_output.status.success();
        let out = String::from_utf8_lossy(&child_process_output.stdout);
        let err = String::from_utf8_lossy(&child_process_output.stderr);

        debug(format!(
            "Command:{:?} \n Exit code:{:?} \n Stdout: {}, Stderr: {} \n",
            &self.internal,
            child_process_output.status.code(),
            out,
            err,
        ))
        .await;
        if !succeeded {
            debug(format!(
                "Failed to run {:?}, Status was: {:?}, Stdout was:\n{:?},Stderr was:\n{:?}",
                &self.internal,
                child_process_output.status.code(),
                out,
                err
            ))
            .await;
            Err(CommandExecutionError::CommandError {
                status: child_process_output.status,
                stderr: err.to_string(),
                command: self.internal,
            })
        } else {
            Ok((out.to_string(), err.to_string()))
        }
    }
}
